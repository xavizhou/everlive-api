<?php

class Book_Model extends CI_Model {

  function create_new($bookData) {
    $data['name'] = $bookData['name'];
    $data['intro'] = $bookData['intro'];
    $data['url'] = $bookData['url'];
    $data['author'] = ($bookData['author'])?$bookData['author']:'未知';
    $data['active'] = ($bookData['active'])?ACTIVE_T:ACTIVE_F;
    $data['create_date'] = date('Y-m-d H:i:s');

    if ( $this->db->insert(TBL_BOOK,$data) ) {
      return $this->db->insert_id();
    } else {
      return false;
    }
  }

  function modify($bookData) {
    $data['name'] = $bookData['name'];
    $data['intro'] = $bookData['intro'];
    $data['url'] = $bookData['url'];
    $data['author'] = ($bookData['author'])?$bookData['author']:'未知';
    $data['active'] = ($bookData['active'])?ACTIVE_T:ACTIVE_F;
    $this->db->where('id', $bookData['id']);
    return $this->db->update(TBL_BOOK, $data); 
  }
  

  function get_books($num_posts = 10 ,$offset = 0) {

    $this->db->from(TBL_BOOK);
    $this->db->limit( $num_posts ,$offset);
    $this->db->order_by('create_date','desc');

    $posts = $this->db->get()->result_array();

    if( is_array($posts) && count($posts) > 0 ) {
      return $posts;
    }
    return false;
  }
  
  
    function remove($id){
        $this->db->delete(TBL_BOOK, array('id' => $id)); 
    }
    function unactive($id){
        $data = array(
               'active' => ACTIVE_F
            );
        $this->db->where('id', $id);
        $this->db->update(TBL_BOOK,$data); 
    }
    function active($id){
        $data = array(
               'active' => ACTIVE_T
            );
        $this->db->where('id', $id);
        $this->db->update(TBL_BOOK,$data); 
    }
    
}
