<?php
define('TBL_XMAS', 'xmas');
define('TBL_XCOU', 'xmas_coupon');
define('TBL_XMSG', 'xmas_message');
define('TBL_XHON', 'xmas_hongbao');

define('XMAS_STATUS_CREA', 1);//create hongbao,ready for draw
define('XMAS_STATUS_DEAL', 10);//dealing connect with wechat
define('XMAS_STATUS_DRAW', 2);//drawed

class Xmas_Model extends CI_Model {
    function create($xmasData) {
        if(!$xmasData['user_id']  || !$xmasData['tree_id']):
            return false;
        endif;
        $data['user_id'] = $xmasData['user_id'];
        $data['tree_id']=$xmasData['tree_id'];
        if ($this -> db -> insert(TBL_XMAS, $data)) {
                return $this -> db -> insert_id();
        } else {
                return false;
        }
    }
    function update($xmasData,$user_id){
        if(!$user_id):
            return false;
        endif;
        $data['coupon_id'] = $xmasData['coupon_id'];
        $data['code'] = $xmasData['code'];
        $data['password']=$xmasData['password'];
        $this->db->where('user_id', $user_id);
        return $this -> db -> update(TBL_XMAS, $data);
    }
    
    function get_xmas_by_user($user_id){
        if(!$user_id):
            return false;
        endif;
        $this->db->select('tree_id,coupon_id,code,password');
        $this->db->from(TBL_XMAS);
        $this->db->where('user_id',$user_id);
        return $this->db->get()->row_array();
    }
    
    function get_message($tree_id){
        if(!$tree_id):
            return false;
        endif;
        $this->db->select('message');
        $this->db->from(TBL_XMSG);
        $this->db->where('tree_id',$tree_id);
        return $this->db->get()->result_array();
    }
    
    function get_coupon($user_id){
        if(!$user_id):
            return false;
        endif;
        
        $this->db->from(TBL_XCOU);
        $this->db->select('id,code,password,date');
        $this->db->where('user_id', $user_id);
        $r = $this->db->get()->row_array();
      
        //if exist coupon record
        if($r):
            return array('status'=>1,'r'=>$r);
        endif;
        
        $data = array(
            'user_id'=>$user_id
        );
        $this->db->where('user_id',0);
        $this->db->limit(1);
        $this->db->update(TBL_XCOU,$data);
        $row = $this->db->affected_rows();
        
        if(!$row){
            return false;
        }
        
        $this->db->from(TBL_XCOU);
        $this->db->select('id,code,password,date');
        $this->db->where('user_id', $user_id);
        $r = $this->db->get()->row_array();
      
        //if exist coupon record
        if($r):
            return array('status'=>0,'r'=>$r);
        endif;
    }
        
    function get_xmas_num($user_id){
        if(!$user_id):
            return false;
        endif;
        $sql = "SELECT user_id
                FROM `".$this->db->dbprefix(TBL_UFOR)."`
                WHERE cid
                IN (
                    SELECT tree_id
                    FROM  `".$this->db->dbprefix(TBL_XMAS)."` 
                    WHERE user_id = '$user_id'
                )";
        $r = $this->db->query($sql)->result_array();
        if($r):
            return $r;
        endif;
        return false;
    }
    
    function get_cron(){
        $sql = "SELECT xmas_id,C.user_id,D.open_id, num
        FROM 
        ( SELECT COUNT( B.user_id ) AS num, A.id as xmas_id, A.user_id
            FROM `".$this->db->dbprefix(TBL_XMAS)."` AS A
            LEFT JOIN `".$this->db->dbprefix(TBL_UFOR)."` AS B ON A.tree_id = B.cid
            WHERE A.notice = 0
            GROUP BY B.cid
        ) C
        LEFT JOIN `".$this->db->dbprefix(TBL_USES)."` AS D on C.user_id = D.user_id
        WHERE num > 5";
        $r = $this->db->query($sql)->result_array();
        
        if($r):
            return $r;
        endif;
        return false;
    }

    
    function get_lastest_hongbao() {
        $this->db->from(TBL_XHON);
        $this->db->select('name,money');
        $this->db->where('status',XMAS_STATUS_DRAW);
        $this->db->where('money >= 2',NULL);
        $this->db->order_by('draw_time','desc');
        $this->db->limit(3);
        $r1 = $this->db->get()->result_array();
        if(!$r1):
            $r1 = array();
        endif;
        $this->db->from(TBL_XHON);
        $this->db->select('name,money');
        $this->db->where('status',XMAS_STATUS_DRAW);
        $this->db->where('money < 2',NULL);
        $this->db->order_by('draw_time','desc');
        $this->db->limit(7);
        $r2 = $this->db->get()->result_array();
        if(!$r2):
            $r2 = array();
        endif;
        return (array_merge($r1 , $r2));
    }
    
    function get_dealing_hongbao() {
        $this->db->from(TBL_XHON);
        $this->db->select('user_id,open_id,money');
        $this->db->where('status',XMAS_STATUS_DEAL);
        $this->db->order_by('id','asc');
        $this->db->limit(1);
        $r = $this->db->get()->row_array();
        if(!$r):
            return false;
        endif;
        return $r;
    }
    function get_hongbao($openid) {
        if(!$openid):
            return false;
        endif;
        $this->db->from(TBL_XHON);
        $this->db->select('union_id,money,user_id,status');
        $this->db->where('open_id', $openid);
        $r = $this->db->get()->row_array();
        if($r):
            return $r;
        endif;
    }
    
    function update_hongbao($hbData,$open_id) {
        if(!$open_id):
            return false;
        endif;
        $data['status'] = $hbData['status'];
        if($hbData['draw_time']):
            $data['draw_time'] = $hbData['draw_time'];
        endif;
        $this->db->where('open_id', $open_id);
        return $this -> db -> update(TBL_XHON, $data);
        
    }
    function create_hongbao($hbData) {
        if(!$hbData['user_id']  || !$hbData['open_id']):
            return false;
        endif;
        $data['user_id'] = $hbData['user_id'];
        $data['open_id']=$hbData['open_id'];
        $data['name'] = $hbData['name'];
        $data['money'] = $hbData['money'];
        $data['create_time'] = GetCurrentTime();
        $data['status'] = ($hbData['status'])?$hbData['status']:XMAS_STATUS_CREA;
        if ($this -> db -> insert(TBL_XHON, $data)) {
                return $this -> db -> insert_id();
        } else {
                return false;
        }
    }
}
/*
$test=new Autumn_Model();
$LocationData['openid'] = 'of6p1jgJfCl_YmsN-73XgbAlP8Cs';
$LocationData['unionid']='oD7DjjoVU8sXzCZ_OQd7CVf2_Cq0';
$LocationData['latitude']=36.4;
$LocationData['longitude']=124.6;
$myid=$test->create_new_location($xmasData);
echo $myid;
 */
?>