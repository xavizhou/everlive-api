<?php

class Star_Model extends CI_Model {

  function get_stars( $constellation, $num_posts = 10 ) {

    $this->db->from('star');
    $this->db->where( array('constellation'=>$constellation) );
    if($num_posts)   $this->db->limit( $num_posts );
    //$this->db->order_by('createdDate','desc');

    $posts = $this->db->get()->result_array();

    if( is_array($posts) && count($posts) > 0 ) {
      return $posts;
    }

    return false;
  }

    function  create_new_star( $starData ) {
      $data['constellation'] = $starData['constellation'];
      $data['name'] = $starData['name'];
      $data['nick'] = $starData['nick'];
      $data['dec'] = (float) $starData['dec'];
      $data['ra'] = (float) $starData['ra'];
      $data['v'] =  $starData['v'];
      $data['active'] = ($starData['actived'])?1:0;
      $data['user_id'] = $starData['user_id'];
      if($this->db->insert(TBL_STAR,$data)){
          return $this->db->insert_id();
      }else{
          return false;
      }
    }
//
//  function get_all_other_posts( $user_id, $team_id, $is_admin, $num_posts = 10 ) {
//    // start building a query
//    $this->db->from('post');
//    $this->db->join('user','post.user_id=user.id');
//
//    // restrict to teammates if not an admin
//    if(!$is_admin){
//      $this->db->where('teamId',$team_id);
//    }
//
//    $this->db->where_not_in('user_id', array($user_id));
//    $this->db->limit( $num_posts );
//    $this->db->order_by('createdDate','desc');
//
//    $posts = $this->db->get()->result_array();
//
//    if( is_array($posts) && count($posts) > 0 ) {
//      return $posts;
//    }
//
//    return false;
//  }

  function get_star_for_user( $user_id ) {

    $this->db->from(TBL_STAR);
    $this->db->where( array('user_id'=>$user_id) );
    $stars = $this->db->get()->result_array();
    
    if( is_array($stars) && count($stars) > 0 ) {
      return $stars[0];
    }
  }
  
  function update_star_file(){
    $this->db->select($this->db->dbprefix(TBL_USER).'.id,'.$this->db->dbprefix(TBL_USER).'.status,'.$this->db->dbprefix(TBL_STAR).'.*');
    $this->db->from(TBL_USER);
    $this->db->join(TBL_STAR, $this->db->dbprefix(TBL_USER).'.id = '.$this->db->dbprefix(TBL_STAR).'.user_id','left');
    $this->db->where($this->db->dbprefix(TBL_USER).'.id >', 0);
    
    
    $result = $this->db->get()->result_array();
    
    if( is_array($result) && count($result) > 0 ) {
       foreach($result as $row){
           if(empty($row['v'])) continue;
           $s.="{$row['dec']}\t{$row['ra']}\t{$row['v']}\t{$row['id']}\t{$row['status']}\n";
       }
     }
    write(BASEPATH."../assets/star",$s);
  }
}
