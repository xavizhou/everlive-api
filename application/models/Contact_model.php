<?php
//联系人管理
class Contact_Model extends CI_Model {
    function get_contact_for_user( $user_id, $num_posts = 10 ,$offset = 0) {

      $this->db->from(TBL_CONT);
      if($user_id){
          $this->db->where( array('user_id'=>$user_id) );
      }
      $this->db->limit( $num_posts ,$offset);

      $list = $this->db->get()->result_array();

      if( is_array($list) && count($list) > 0 ) {
        return $list;
      }

      return false;
    }
    
    function get_contact_by_id( $user_id, $id) {

      $this->db->from(TBL_CONT);
      if($user_id){
          $this->db->where( array('user_id'=>$user_id) );
      }
      $this->db->where( array('id'=>$id) );
      $this->db->limit( 1 ,0);

      $list = $this->db->get()->result_array();

      if( is_array($list) && count($list) > 0 ) {
        return $list[0];
      }

      return false;
    }
    function get_contact_by_token( $token) {
      $this->db->from(TBL_CONT);
      $this->db->where( array('token'=>$token) );
      $this->db->limit( 1 ,0);
      $list = $this->db->get()->result_array();
      if( is_array($list) && count($list) > 0 ) {
        return $list[0];
      }
      return false;
    }
    
    function get_contact_by_email(  $email,$user_id) {

      $this->db->from(TBL_CONT);
      if($user_id){
          $this->db->where( array('user_id'=>$user_id) );
      }
      $this->db->where( array('email'=>$email) );
      $this->db->limit( 1 ,0);

      $list = $this->db->get()->result_array();

      if( is_array($list) && count($list) > 0 ) {
        return $list[0];
      }

      return false;
    }
    
    
    function get_receivers_for_post( $ids) {
      $this->db->from(TBL_CONT);
      $this->db->where_in('id', $ids);
      $list = $this->db->get()->result_array();

      if( is_array($list) && count($list) > 0 ) {
        return $list;
      }
      return false;
    }
    
    
    function set_valid( $contactData) {
        $data['token'] = $contactData['token'];
        $data['token_time'] = $contactData['token_time'];
        $data['valid'] = (int)$contactData['valid'];
        $this->db->where('id', $contactData['id']);
        return $this->db->update(TBL_CONT, $data);
    }
    
    
    function  create_new( $userData) {
        $data['user_id'] = $userData['user_id'];
        $data['name'] = $userData['name'];
        $data['email'] = $userData['email'];
        $data['mobile'] = $userData['mobile'];
        $data['message'] = $userData['message'];
        if($this->db->insert(TBL_CONT,$data)){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }
    function  update( $userData) {
        $data['user_id'] = $userData['user_id'];
        $data['name'] = $userData['name'];
        $data['email'] = $userData['email'];
        $data['mobile'] = $userData['mobile'];
        $data['message'] = $userData['message'];
        $this->db->where('id', $userData['id']);
        return $this->db->update(TBL_CONT, $data); 
    }
    
    function remove($id,$user_id){
        $this->db->delete(TBL_CONT, array('id' => $id,'user_id' => $user_id)); 
    }
    function removeAll($user_id){
        $this->db->delete(TBL_CONT, array('user_id' => $user_id)); 
    }
}
