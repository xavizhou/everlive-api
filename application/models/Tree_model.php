<?php

class Tree_Model extends CI_Model {
    
  function get_posts($data,$num_posts = 20 ,$offset = 0){
    $this->db->from(TBL_TREE);
    $condition ='';
    if($data['year']) $condition .= " AND A.year = '{$data['year']}'";
    if($data['tag']) $condition .= " AND A.tags LIKE  '%{$data['tag']}%'";
    
//    $data['status'] = (int)$data['status'];
//    $status = ($data['status'])?:$data['status'];
    
    $condition .= " AND B.status = ".CONT_ACCE;
    
    if($num_posts){
        $limit =  " LIMIT $offset , $num_posts";
    }
    
    $sql = 'SELECT A.year,A.position,A.tags,A.id,A.contribute_id,B.create_date,B.user_id,B.content,B.title,C.username, C.display_name FROM '.
            $this->db->dbprefix(TBL_TREE).' AS A,'.
            $this->db->dbprefix(TBL_PCON).' AS B,'.
            $this->db->dbprefix(TBL_USER).' AS C '.
            '   WHERE A.contribute_id = B.id'.
            '   AND B.user_id = C.id' .
            $condition.
            ' ORDER BY A.position ASC'.$limit;
            
    $r = $this->db->query($sql)->result_array();
    //echo $this->db->last_query();
    if ( is_array($r) && count($r) > 0) {
        return $r;
    }
    return false;
  }
  
  function get_posts_all_tags($year,$tags,$num_posts=10){
    $condition = " AND B.status = ".CONT_ACCE;
    if($year) $condition = " AND A.year = '$year'";
    
    if($num_posts){
        $limit =  " LIMIT 0 , $num_posts";
    }
    $sql = array();
    if(is_array($tags)){
        foreach($tags as $tag){
            $cond_t .= " AND A.tags LIKE  '%{$tag}%'";   
            
            $sql[] = '(SELECT A.year,A.position,A.tags,A.id,A.contribute_id,A.position,B.create_date,B.user_id,B.content,B.title,C.username, C.display_name FROM '.
                    $this->db->dbprefix(TBL_TREE).' AS A,'.
                    $this->db->dbprefix(TBL_PCON).' AS B,'.
                    $this->db->dbprefix(TBL_USER).' AS C '.
                    '   WHERE A.contribute_id = B.id'.
                    '   AND B.user_id = C.id' .
                    $condition.
                    $cond_t.
                    ' ORDER BY A.position ASC'.$limit.')';
    
        }
    }
    $sql = implode(" UNION ", $sql);
    $sql = "SELECT DISTINCT * FROM ($sql) AS AA ORDER BY AA.`position`";
            
    $r = $this->db->query($sql)->result_array();
    //echo $this->db->last_query();
    if ( is_array($r) && count($r) > 0) {
        return $r;
    }
    return false;
  }
  
  function get_posts_by_id($id){
        $this->db->from(TBL_TREE);
        $where = array('post_id'=>$id);
        $this->db->where($where);
        $r = $this->db->get()->result_array();
        
        //echo $this->db->last_query();
        if ( is_array($r) && count($r) > 0) {
            return $r;
        }
        return false;
  }
  
  function get_post_count( $data ) {

    $this->db->from(TBL_TREE);
    $condition ='';
    if($data['year']) $condition .= " AND A.year = '{$data['year']}'";
    if($data['tag']) $condition .= " AND A.tags LIKE  '%{$data['tag']}%'";
    
    $condition .= " AND B.status = ".CONT_ACCE;
    
    $sql = 'SELECT COUNT(*) AS number FROM '.
            $this->db->dbprefix(TBL_TREE).' AS A,'.$this->db->dbprefix(TBL_PCON).' AS B'.
            '   WHERE A.contribute_id = B.id'.$condition;
            
    
    $r = $this->db->query($sql)->result_array();
    if ( is_array($r) && count($r) > 0) {
        return $r[0]['number'];
    }else{
        return 0;
    }
  }
  
  
  function get_contribute($status){
      $status = (int)$status;
      if(empty($status)) return false;
    $this->db->from(TBL_PCON);
    $where = array('status'=>$status);
    $this->db->where($where);
    $r = $this->db->get()->result_array();
    if ( is_array($r) && count($r) > 0) {
        return $r;
    }
    return false;
  }
  
  
  function get_contribute_count($status){
      $status = (int)$status;
      if(empty($status)) return false;
    $this->db->from(TBL_PSOC);
    $where = array('is_contribute'=>$status);
    $this->db->where($where);
    return $this->db->get()->count_all_results();
  }
  
  
  function update_position($position){
      $data = array();
      if(is_array($position)){
          foreach($position as $k=>$v){
              $data[] = array(
                  'id'=>$k,
                  'position'=>$v
              );
          }
      }
        $this->db->update_batch(TBL_TREE, $data, 'id'); 
  }
  
  function get_all_post_by_contribute_id($contribute_id){
        $this->db->from(TBL_TREE);
        $where = array('contribute_id'=>$contribute_id);
        $this->db->where($where);
        $r = $this->db->get()->result_array();
        if(is_array($r)){
            return $r;
        }else{
            return false;
        }
  }
  
    function save_post($postData){
        $contribute_id = (int)$postData['contribute_id'];
        $post_id = $postData['post_id'];
        $tags = $postData['tags'];
        $year = $postData['year'];
        
        
        if(!is_array($year)){
            $year = array($year);
        }
        
        $r = $this->get_all_post_by_contribute_id($contribute_id);
        
        $year_to_id = array();
        if(is_array($r)){
            for($i=0;$i<count($r);$i++){
                if(!in_array($r[$i]['year'], $year)){
                    $this->db->delete(TBL_TREE, array('id' => $r[$i]['id']));
                }else{
                    $year_to_id[$r[$i]['year']] = $r[$i]['id'];
                }
            }
        }
        
        for($i=0;$i<count($year);$i++){
            $id = $year_to_id[$year[$i]];
            if ($id) {
                //update post
                $post = $r[0];
                $data = array(
                    'tags'=>$tags
                );
                $this->db->where('id', $id);
                $this->db->update(TBL_TREE,$data); 
            }else{
                //add post
                $data = array(
                    'contribute_id'=>$contribute_id,
                    'post_id'=>$post_id,
                    'tags'=>$tags,
                    'position'=>99,
                    'year'=>$year[$i]
                );
                $this->db->insert(TBL_TREE, $data); 
            }
        }
    }
  
  
    function add_post($postData){
        $contribute_id = $postData['contribute_id'];
        $post_id = $postData['post_id'];
        $tags = $postData['tags'];
        $year = $postData['year'];
        if(!is_array($year)){
            $year = array($year);
        }
        $data = array();
        for($i=0;$i<count($year);$i++){
            $data[] = array(
                'contribute_id'=>$contribute_id,
                'post_id'=>$post_id,
                'tags'=>$tags,
                'position'=>99,
                'year'=>$year[$i]
                );
        }
        $this->db->delete(TBL_TREE, array('contribute_id' => $contribute_id)); 
        $this->db->insert_batch(TBL_TREE, $data); 
    }
  
    function remove_post($id){
        $this->db->delete(TBL_TREE, array('contribute_id' => $id));
    }
  
    
    function accpet_contribute($id){
        $data = array(
               'status' => CONT_ACCE
            );
        $this->db->where('id', $id);
        $this->db->update(TBL_PCON,$data); 
    }
    
    function rollback_contribute($id){
        $data = array(
               'status' => CONT_SEND
            );
        $this->db->where('id', $id);
        $this->db->update(TBL_PCON,$data); 
        //echo $this->db->last_query();
        return;
    }
    
    function update_contribute($id,$postData){
        $data = array(
               'title' => $postData['title'],
               'content' => $postData['content'],
               'file' => $postData['file']
            );
        $this->db->where('id', $id);
        $this->db->update(TBL_PCON,$data); 
    }
    function refuse_contribute($id){
        $data = array(
               'status' => CONT_REFU
            );
        $this->db->where('id', $id);
        $this->db->update(TBL_PCON,$data); 
    }
    function cancel_contribute($id){
        $this->db->delete(TBL_TREE, array('id' => $id)); 
    }
  
  function get_post_tags($post_id,$user_id){
    $this->db->from(TBL_PTAG);
    $where = array('post_id' => $post_id,'user_id'=>$user_id);
    $this->db->where($where);
    $r = $this->db->get()->result_array();
    if ( is_array($r) && count($r) > 0) {
        return $r;
    }
    return false;
  }
  
  
    function post_search($k){//搜索文章 title or content
        //SELECT A.*,D.avatar,D.real_name,D.username FROM el_post AS A left join el_post_tag AS B on A.id = B.post_id left join el_tags AS C on C.id = B.tag_id left join el_user AS D ON A.user_id = D.id WHERE (C.value like "%英雄%" OR A.title like "%英雄%" )
                
        $k = str_replace('"', '\"', $k);
//        $sql = 'SELECT A.*,D.avatar,D.real_name,D.username FROM '.
//                $this->db->dbprefix(TBL_PUBL).' AS A'.
//                ' LEFT JOIN '.$this->db->dbprefix(TBL_PTAG).' AS B ON A.post_id = B.post_id'.
//                ' LEFT JOIN '.$this->db->dbprefix(TBL_TAGS).' AS C ON C.id = B.tag_id'.
//                ' LEFT JOIN '.$this->db->dbprefix(TBL_USER).' AS D ON A.user_id = D.id WHERE'.
//                ' (C.value like "%'.$k.'%" OR A.title like "%'.$k.'%")';
        
        $sql = 'SELECT A.*,D.avatar,D.real_name,D.username FROM '.
                $this->db->dbprefix(TBL_PUBL).' AS A'.
                ' LEFT JOIN '.$this->db->dbprefix(TBL_PTAG).' AS B ON A.post_id = B.post_id'.
                ' LEFT JOIN '.$this->db->dbprefix(TBL_TAGS).' AS C ON C.id = B.tag_id'.
                ' LEFT JOIN '.$this->db->dbprefix(TBL_USER).' AS D ON A.user_id = D.id WHERE'.
                ' (C.value like "%'.$k.'%" OR A.title like "%'.$k.'%" OR A.content like "%'.$k.'%")';
        //echo $sql;
        $r = $this->db->query($sql)->result_array();
        if ( is_array($r) && count($r) > 0) {
            return $r;
        }
        return false;
    }

  function get_post_by_id( $id) {
      if(empty($id)) return false;
    $sql = 'SELECT A.id,A.year,A.tags,B.post_id,B.create_date,B.mode,B.type,B.file,B.`file-text`,B.`file-audio`,B.`file-media`,B.files,B.key,B.user_id,B.content,B.title,C.username, C.display_name FROM '.
            $this->db->dbprefix(TBL_TREE).' AS A,'.
            $this->db->dbprefix(TBL_PCON).' AS B,'.
            $this->db->dbprefix(TBL_USER).' AS C '.
            '   WHERE A.contribute_id = B.id'.
            '   AND B.user_id = C.id' .
            '   AND B.status = '.CONT_ACCE.
            '   AND A.id = '.$id ;
            
        $r = $this->db->query($sql)->result_array();
        //echo $this->db->last_query();
        if ( is_array($r) && count($r) > 0) {
            return $r[0];
        }
        return false;
  }

  function get_post_by_contribute_id( $id) {
      if(empty($id)) return false;
    $sql = 'SELECT B.create_date,B.mode,B.type,B.file,B.files,B.`file-text`,B.`file-audio`,B.`file-media`,B.key,B.user_id,B.content,B.title,B.status,C.username,C.email, C.display_name FROM '.
            $this->db->dbprefix(TBL_PCON).' AS B,'.
            $this->db->dbprefix(TBL_USER).' AS C '.
            '   WHERE  B.user_id = C.id' .
            '   AND B.id = '.$id ;
            
        $r = $this->db->query($sql)->result_array();
        //echo $this->db->last_query();
        if ( is_array($r) && count($r) > 0) {
            return $r[0];
        }
        return false;
  }
  
  function get_paths_for_user( $user_id) {
    $this->db->from(TBL_POST);
    if($user_id){
        $where['user_id']=$user_id;
    }
    $this->db->where($where);
    $this->db->select('id, file');
    $posts = $this->db->get()->result_array();
    if( is_array($posts) && count($posts) > 0 ) {
      return $posts;
    }
    return false;
  }
  function update_paths_for_user( $user_id,$data) {
    $this->db->update_batch(TBL_POST, $data, 'id'); 
  }
  
  function get_posts_for_user( $user_id,$mode=0,$type=0, $num_posts = 10 ,$offset = 0) {

    $this->db->from(TBL_POST);
    if($user_id){
        $where['user_id']=$user_id;
    }
    if($mode){
        $where['mode']=$mode;
    }
    if($type){
        $where['type']=$type;
    }
    $this->db->where($where);
    if($num_posts){
        $this->db->limit( $num_posts ,$offset);
    }
    
    $this->db->order_by('create_date','desc');

    $posts = $this->db->get()->result_array();
    
    if( is_array($posts) && count($posts) > 0 ) {
      return $posts;
    }
    return false;
  }

  function get_post_count_for_user( $user_id ) {

    $this->db->from(TBL_POST);
    $this->db->where( array('user_id'=>$user_id) );

    return $this->db->count_all_results();
  }
  
  
  function get_publish_posts_for_user( $user_id,$mode=0,$type=0, $num_posts = 10 ,$offset = 0) {

    $this->db->from(TBL_PUBL);
    $where = array();
    if($user_id){
        $where['user_id']=$user_id;
    }
    if($mode){
        $where['mode']=$mode;
    }
    if($type){
        $where['type']=$type;
    }
    $this->db->where($where);
    if($num_posts){
        $this->db->limit( $num_posts ,$offset);
    }
    
    $this->db->order_by('update_date','desc');

    $posts = $this->db->get()->result_array();
    
    if( is_array($posts) && count($posts) > 0 ) {
      return $posts;
    }
    return false;
  }

  function get_publish_post_count_for_user( $user_id ) {

    $this->db->from(TBL_PUBL);
    $where = array();
    if($user_id){
        $where['user_id']=$user_id;
    }
    
    $this->db->where($where );

    return $this->db->count_all_results();
  }
  
  function update_publish_post_content( $id,&$content) {
    $data = array(
           'content' => $content,
           'file' => ''
        );
    $this->db->where('id', $id);
    $this->db->update(TBL_PUBL,$data); 
  }
  
  
  function transfor_entrustment($id){
        $id = (int)$id;
        $sql = 'INSERT '.$this->db->dbprefix(TBL_PENT).' (user_id,post_id,receiver_email,receiver_message,post_datetime,sendtime,imme)
                           SELECT B.user_id,A.post_id, C.email as receiver_email, C.message as receiver_message, B.update_date as post_datetime,B.deadline as sendtime,B.imme
                           FROM '.$this->db->dbprefix(TBL_PENC).' as A,'.$this->db->dbprefix(TBL_POST).' as B,'.$this->db->dbprefix(TBL_CONT).' as C
                           WHERE A.post_id = B.id AND A.contact_id = C.id AND B.user_id = "'.$id.'"';
        $query = $this->db->query($sql);
  }
  
  function get_entrustment_before_send($id){
        $id = (int)$id;
        $where .= "((sendtime <= '".GetCurrentTime()."') OR (imme = 1)) AND receiver_email != '' AND is_send = 0 ";
        if($id){
            $where .= " AND user_id = '$id'";
        }
        
        $this->db->from(TBL_PENT);
        $this->db->where($where);
        $r = $this->db->get()->result_array();
        //echo $this->db->last_query();
        return $r;
  }
  function set_entrustment_status($data){
        $this->db->where('id', $data['id']);
        $this->db->update(TBL_PENT,$data); 
  }
    function remove($id){
        $this->db->delete(TBL_POST, array('id' => $id)); 
    }
    function unactive($id){
        $data = array(
               'active' => ACTIVE_F
            );
        $this->db->where('id', $id);
        $this->db->update(TBL_PUBL,$data); 
    }
    function active($id){
        $data = array(
               'active' => ACTIVE_T
            );
        $this->db->where('id', $id);
        $this->db->update(TBL_PUBL,$data); 
    }
    
    
}
