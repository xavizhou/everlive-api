<?php

class Regcode_Model extends CI_Model {

  function get_regcode($num_posts = 10 ,$offset = 0) {
    $sql = 'SELECT A.*,B.username FROM '.
            $this->db->dbprefix(TBL_RCOD).' AS A'.
            ' LEFT JOIN '.$this->db->dbprefix(TBL_USER).' AS B ON A.user_id = B.id';
        
    $posts = $this->db->query($sql)->result_array();
        
        
    if( is_array($posts) && count($posts) > 0 ) {
      return $posts;
    }
    return false;
  }
  
  function generate_regcode($num) {
        $data = array();
        for($i=0;$i<$num;$i++){
            $data[] = array('code'=>rand(10000000,99999999));
        }
        $this->db->insert_batch(TBL_RCOD, $data); 
  }
  
    function use_regcode($RegData){
        $data['used_date'] = $RegData['used_date'];
        $data['user_id'] = $RegData['user_id'];
        $this->db->where('code', $RegData['code']);
        return $this->db->update(TBL_RCOD, $data); 
    }
    
    function remove($id){
        $this->db->delete(TBL_RCOD, array('id' => $id)); 
    }
    
    function get_regcode_by_code($ecode){
      $this->db->from(TBL_RCOD);
      $this->db->where(array('code' =>$ecode,'user_id' =>null));
      $r = $this->db->get()->result_array();
      if( is_array($r) && count($r) > 0 ) {
        return $r[0];
      }
      return false;
    }
  

  
}
