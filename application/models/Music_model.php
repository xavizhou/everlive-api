<?php

class Music_Model extends CI_Model {

  function create_new($musicData) {
    $data['name'] = $musicData['name'];
    $data['intro'] = $musicData['intro'];
    $data['url'] = $musicData['url'];
    $data['author'] = ($musicData['author'])?$musicData['author']:'未知';
    $data['active'] = ($musicData['active'])?ACTIVE_T:ACTIVE_F;
    $data['create_date'] = date('Y-m-d H:i:s');

    if ( $this->db->insert(TBL_MUSI,$data) ) {
      return $this->db->insert_id();
    } else {
      return false;
    }
  }

  function modify($musicData) {
    $data['name'] = $musicData['name'];
    $data['intro'] = $musicData['intro'];
    $data['url'] = $musicData['url'];
    $data['author'] = ($musicData['author'])?$musicData['author']:'未知';
    $data['active'] = ($musicData['active'])?ACTIVE_T:ACTIVE_F;
    $this->db->where('id', $musicData['id']);
    return $this->db->update(TBL_MUSI, $data); 
  }
  


  function get_by_id($id) {
    $this->db->from(TBL_MUSI);
    $this->db->limit( 1 ,0);
    $this->db->where('id', $id);
    $r = $this->db->get()->result_array();
    if( is_array($r) && count($r) > 0 ) {
      return $r[0];
    }
  }
  
  
  function get_music($num_posts = 10 ,$offset = 0) {

    $this->db->from(TBL_MUSI);
    $this->db->limit( $num_posts ,$offset);
    $this->db->order_by('create_date','desc');

    $posts = $this->db->get()->result_array();

    if( is_array($posts) && count($posts) > 0 ) {
      return $posts;
    }
    return false;
  }
  
  function get_fav($user_id,$num=1) {
        //首次公开，则复制数据
        $this->db->select('*');
        $this->db->from(TBL_MUSI);
        $this->db->join(TBL_UFAV, $this->db->dbprefix(TBL_UFAV).'.fav_id = '.$this->db->dbprefix(TBL_MUSI).'.id');
        $this->db->where($this->db->dbprefix(TBL_UFAV).'.type', FAVI_MUSI); 
        $this->db->order_by('create_date','desc');
        $this->db->limit( $num ,0);

        $r = $this->db->get()->result_array();

        if( is_array($r) && count($r) > 0 ) {
          return $r;
        }
        return false;
  }
  
    function remove($id){
        $this->db->delete(TBL_MUSI, array('id' => $id)); 
    }
    function unactive($id){
        $data = array(
               'active' => ACTIVE_F
            );
        $this->db->where('id', $id);
        $this->db->update(TBL_MUSI,$data); 
    }
    function active($id){
        $data = array(
               'active' => ACTIVE_T
            );
        $this->db->where('id', $id);
        $this->db->update(TBL_MUSI,$data); 
    }
    
}
