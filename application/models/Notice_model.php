<?php
//站内通知模块
class Notice_Model extends CI_Model {
  function create_new($postData) {
    $data['title'] = $postData['title'];
    $data['content'] = $postData['content'];
    $data['user_id'] = (int)$postData['user_id'];
    $data['status'] = ($postData['status'])?NOTICE_READ:NOTICE_UNRE;
    $data['create_date'] = date('Y-m-d H:i:s');

    if ( $this->db->insert(TBL_NOTI,$data) ) {
      return $data;
    } else {
      return false;
    }
  }
  function create_new_for_all($postData ,$condition=array()) {
    $data['title'] = $postData['title'];
    $data['content'] = $postData['content'];
    $data['status'] = ($postData['status'])?NOTICE_READ:NOTICE_UNRE;
    $data['create_date'] = date('Y-m-d H:i:s');
    $c = '';
    if(is_array($condition)){
        foreach($condition as $k=>$v){
            $c .= " AND $k='$v'";
        }
    }
    //insert into el_notice(`user_id`,`title`,`content`, `status`, `create_date`) SELECT `id`, 'test notice','test content',0,NOW() FROM el_user
            
    //批量创建通知
    $data['title'] =
    $sql = 'INSERT INTO '.$this->db->dbprefix(TBL_NOTI).' (`user_id`,`title`,`content`, `status`, `create_date`)
                       SELECT id, \''.$data['title'].'\', \''.$data['content'].'\','.NOTICE_UNRE.',NOW()
                       FROM '.$this->db->dbprefix(TBL_USER).'
                       WHERE 1=1 '.$c;
    return $this->db->query($sql);
  }

  function get_notice_for_user( $user_id, $num_posts = 10 ,$offset = 0) {

    $this->db->from(TBL_NOTI);
    if($user_id){
        $this->db->where( array('user_id'=>$user_id) );
    }
    $this->db->limit( $num_posts ,$offset);
    $this->db->order_by('create_date','desc');

    $posts = $this->db->get()->result_array();

    if( is_array($posts) && count($posts) > 0 ) {
      return $posts;
    }

    return false;
  }
  function get_latest_notice($user_id){
      $r = $this->get_notice_for_user( $user_id, 1,0) ;
      if($r !== false) return $r[0];
      return false;
  }

  
  function get_post_count_for_user( $user_id ) {
    $this->db->from(TBL_NOTI);
    $this->db->where( array('user_id'=>$user_id) );

    return $this->db->count_all_results();
  }
  
    function remove($id){
        $this->db->delete(TBL_NOTI, array('id' => $id)); 
    }

    function set_status($id,$user_id,$status){
        $data = array(
               'stauts' => $status
            );
        $this->db->where( array('id' => $id,'user_id'=>$user_id));
        $this->db->update(TBL_NOTI,$data); 
    }
}
