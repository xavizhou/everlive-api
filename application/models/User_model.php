<?php
class User_Model extends CI_Model {
    var $details;
    
    function validate_user( $email, $password,$only_check=false ) {
        // Build a query to retrieve the user's details
        // based on the received username and password
//        $this->db->from(TBL_USER);
//        $this->db->where('email',$email );
//        $this->db->where( 'password', sha1($password) );
//        $login = $this->db->get()->result();
//
//        // The results of the query are stored in $login.
//        // If a value exists, then the user account exists and is validated
//        if ( is_array($login) && count($login) == 1 ) {
//            // Set the users details into the $details property of this class
//            $this->details = $login[0];
//            // Call set_session to set the user's session vars via CodeIgniter
//            $this->set_session();
//            return true;
//        }

        $this->db->from(TBL_USER);
        $this->db->where('username',$email );//maybe input username 
        $this->db->where( 'password', sha1($password) );
        $this->db->where( 'status', STATUS_LIVE );
        $login = $this->db->get()->result();

        // The results of the query are stored in $login.
        // If a value exists, then the user account exists and is validated
        if ( is_array($login) && count($login) == 1 ) {
            // Set the users details into the $details property of this class
            $this->details = $login[0];
            // Call set_session to set the user's session vars via CodeIgniter
            if(!$only_check){
                $this->set_session();
            }
            return (array)$login[0];
        }
        
        return false;
    }
    function validate_user_mobile($mobile,$only_check=1){
        $this->db->from(TBL_USER);
        $this->db->where('mobile',$mobile );//maybe input username 
        $this->db->where( 'status', STATUS_LIVE );
        $login = $this->db->get()->result();

        if ( is_array($login) && count($login) == 1 ) {
            $this->details = $login[0];
            if(!$only_check){
                $this->set_session();
            }
            return (array)$login[0];
        }
        return false;
    }
    
    function valid_metero($username,$password2){
        $this->db->from(TBL_USER);
        $this->db->where('username',$username );//maybe input username 
        $this->db->where( 'password2', sha1($password2) );
        $login = $this->db->get()->result_array();

        // The results of the query are stored in $login.
        // If a value exists, then the user account exists and is validated
        if ( is_array($login) && count($login) == 1 ) {
            return $login[0];
        }
        
        $this->db->from(TBL_USER);
        $this->db->where('username',$username );//maybe input username 
        $this->db->where( 'password', sha1($password2) );
        $login = $this->db->get()->result_array();

        // The results of the query are stored in $login.
        // If a value exists, then the user account exists and is validated
        if ( is_array($login) && count($login) == 1 ) {
            return $login[0];
        }
        
        return false;
    }
    
    
    function validate_union_id( $union_id, $type) {
        $r = $this->get_user_by_union_id($union_id, $type);
        if(empty($r)) return false;
        $this->details =  (object)$r;
        $this->details->union_id = $union_id;
        $this->set_session();
        return $r;
    }
    
    function validate_open_id( $open_id, $type) {
        $r = $this->get_user_by_open_id($open_id, $type);
        if(empty($r)) return false;
        $this->details =  (object)$r;
        $this->details->open_id = $open_id;
        $this->set_session();
        return $r;
    }
    
    
    function start_metero($id){
        $id = (int)$id;
        $data = array(
               'status' => STATUS_DEAD,
               'passtime' => date('Y-m-d H:i:s')
            );
        $this->db->where('id', $id);
        return $this->db->update(TBL_USER,$data); 
    }
    
    function get_meteors($time){
        $this->db->from(TBL_USER);
        $this->db->where('status',1);
        $this->db->where('passtime > ',date('Y-m-d H:i:s',time()-$time));
        return $this->db->count_all_results();
    }
    
    function refresh_session($id){
        $this->db->from(TBL_USER);
        $this->db->where('id',$id );
        $login = $this->db->get()->result();

        // The results of the query are stored in $login.
        // If a value exists, then the user account exists and is validated
        if ( is_array($login) && count($login) == 1 ) {
            // Set the users details into the $details property of this class
            $this->details = $login[0];
            // Call set_session to set the user's session vars via CodeIgniter
            $this->set_session();
            return true;
        }
        return false;
    }

    function set_session() {
        // session->set_userdata is a CodeIgniter function that
        // stores data in CodeIgniter's session storage.  Some of the values are built in
        // to CodeIgniter, others are added.  See CodeIgniter's documentation for details.
        $t = strtotime($this->details->vip_deadline) ;
        if($t && ($t > DATE_FOREVER)){
            $is_vip = USER_FORE;
        }elseif($t && ($t > time())){
            $is_vip = USER_ADVA;
        }else{
            $is_vip = USER_FREE;
        }
        //log_message('ERROR',__LINE__.'/xxxx/'.$this->details->id);
        $this->session->set_userdata( array(
                'id'=>$this->details->id,
                'email'=>$this->details->email,
                'password'=>$this->details->password,
                'pw_sec'=>$this->details->pw_sec,
                'password2'=>$this->details->password2,
                'key'=>$this->details->key,
                'avatar'=>$this->details->avatar,
                'username'=>$this->details->username,
                'display_name'=>$this->details->display_name,
                'real_name'=>$this->details->real_name,
                'gender'=>$this->details->gender,
                'birthdate'=>$this->details->birthdate,
                'level'=>$this->details->level,
                'star_id'=>$this->details->star_id,
                'space'=>$this->details->space,
                'status'=>$this->details->status,
                'active'=>$this->details->active,
                'place'=>$this->details->place,
                'data_used'=>$this->details->data_used,
                'create_date'=>$this->details->create_date,
                'school'=>$this->details->school,
                'intro'=>$this->details->intro,
                'tags'=>$this->details->tags,
                'career'=>$this->details->career,
                'passtime'=>$this->details->passtime,
                'vip_deadline'=>$this->details->vip_deadline,
                'is_vip'=>$is_vip,
                'mobile'=>$this->details->mobile,
                'source'=>$this->details->source,
                'system_account'=>$this->details->system_account
            )
        );
    }
    
    
    function check_user($data,&$error){
        $this->load->library('validator');//validator_helper
        $this->validator->add_rule('username', 'required', _('请输入用户名'), $data['username'] );
        $this->validator->add_rule('username', 'valid_username', _('用户名不正确'), $data['username'] );
        $this->validator->add_rule('username', 'length_min', sprintf(_('用户名至少%d位'),USERNAME_MIN), array($data['username'], USERNAME_MIN));
        $this->validator->add_rule('password', 'required', _('请输入密码'), $data['password'] );
        $this->validator->add_rule('password', 'length_min', sprintf(_('密码至少%d位'),PASSWORD_MIN), array($data['password'], PASSWORD_MIN));
        //$this->validator->add_rule('password', 'valid_password', _('密码不正确'),array($data['username'], $data['password']));
        //$this->validator->add_rule('email', 'required', _('请输入您常用的电子邮箱'), $data['email'] );
        if(isset($data['email'])){
            $this->validator->add_rule('email', 'valid_email', _('您输入的电子邮箱地址不正确。'), $data['email']);
        
        }

        //$this->validator->add_rule('birthdate', 'required', _('请输入出生年月'), $data['birthdate'] );
        //$this->validator->add_rule('birthdate', 'valid_date', _('请输入正确的出生年月'), $data['birthdate']);
        //$this->validator->add_rule('real_name', 'required', _('请输入真实姓名'), $data['real_name'] );
        //$this->validator->add_rule('display_name', 'required', _('请输入昵称'), $data['display_name'] );
        
        $this->validator->validate();   
        if ($this->validator->is_valid())
        {
            return true;
        }else{
            $err = array();
            foreach($this->validator->get_errors() as $e)
            {
                $err[] = $e;
            }
            $error = $err;
            return false;
        }
    }
    
    function check_subuser($data,&$error){
        $this->load->library('validator');//validator_helper
        $this->validator->add_rule('username', 'required', _('请输入用户名'), $data['username'] );
        $this->validator->add_rule('username', 'valid_username', _('用户名不正确'), $data['username'] );
        //$this->validator->add_rule('birthdate', 'valid_date', _('请输入正确的出生年月'), $data['birthdate']);
        $this->validator->add_rule('display_name', 'required', _('请输入昵称'), $data['display_name'] );
        
        $this->validator->validate();
        if ($this->validator->is_valid())
        {
            return true;
        }else{
            $err = array();
            foreach($this->validator->get_errors() as $e)
            {
                $err[] = $e;
            }
            $error = $err;
            return false;
        }
    }
    
    function check_user_basic($data,&$error){
        $this->load->library('validator');//validator_helper
        $this->validator->add_rule('password', 'required', _('请输入密码'), $data['password'] );
        $this->validator->add_rule('password', 'length_min', sprintf(_('密码至少%d位'),PASSWORD_MIN), array($data['password'], PASSWORD_MIN));
        $this->validator->add_rule('password', 'valid_password', sprintf(_('密码至少%d位'),PASSWORD_MIN).'，'._('不能为纯字母或者纯数字，不能存在三个连续的相同字符，不能包含用户名'),array($data['username'], $data['password']));

        $this->validator->validate();
        if ($this->validator->is_valid())
        {
            return true;
        }else{
            $err = array();
            foreach($this->validator->get_errors() as $e)
            {
                $err[] = $e;
            }
            $error = $err;
            return false;
        }
    }
    
    function check_user_temp($data,&$error){
        $this->load->library('validator');//validator_helper
        $this->validator->add_rule('username', 'required', _('请输入用户名'), $data['username'] );
        $this->validator->add_rule('username', 'valid_username', _('用户名不正确'), $data['username'] );
        $this->validator->add_rule('password', 'required', _('请输入密码'), $data['password'] );
        $this->validator->add_rule('password', 'length_min', sprintf(_('密码至少%d位'),PASSWORD_MIN), array($data['password'], PASSWORD_MIN));
        $this->validator->add_rule('password', 'valid_password', sprintf(_('密码至少%d位'),PASSWORD_MIN).'，'._('不能为纯字母或者纯数字，不能存在三个连续的相同字符，不能包含用户名'),array($data['username'], $data['password']));

        $this->validator->validate();
        if ($this->validator->is_valid())
        {
            return true;
        }else{
            $err = array();
            foreach($this->validator->get_errors() as $e)
            {
                $err[] = $e;
            }
            $error = $err;
            return false;
        }
    }
    
    
    function check_user_password_adv($data,&$error){
        $this->load->library('validator');//validator_helper
        $this->validator->add_rule('password', 'valid_password_adv', sprintf(_('密码至少%d位'),PASSWORD_MIN).'，'._('不能为纯字母或者纯数字，不能存在三个连续的相同字符，不能包含用户名'),array($data['username'], $data['password']));
        
        $this->validator->validate();
        if ($this->validator->is_valid())
        {
            return true;
        }else{
            return false;
        }
    }
    
    function update_temp_account($userData){
        $data['username'] = $userData['username'];
//        $data['real_name'] = $userData['real_name'];
//        $data['display_name'] = $userData['display_name'];
        $data['password'] = sha1($userData['password']);
        $data['pw_sec'] = (int)($userData['pw_sec']);
        $data['password2'] = sha1(substr($userData['password'], 0, -1));
        $data['system_account'] = 0;
        $this->db->where('id', $userData['id']);
        return $this->db->update(TBL_USER, $data); 
        
    }
    
    function  create_new( $userData) {
        $data['username'] = $userData['username'];
        $data['contact'] = $userData['contact'];
        $data['display_name'] = $userData['display_name'];
        $data['real_name'] = $userData['real_name'];
        $data['birthdate'] = $userData['birthdate'];
        $data['mobile'] = $userData['mobile'];
        $data['gender'] = ($userData['gender'])?GENDER_M:GENDER_F;
        $data['level'] = (int) $userData['level'];
        //$data['star_id'] = (int) $userData['star_id'];
        $data['active'] = (int) $userData['active'];
        $data['email'] = $userData['email'];
        $data['place'] = $userData['place'];
        $data['avatar'] = $userData['avatar'];
        $data['status'] = $userData['status'];
        $data['password'] = sha1($userData['password']);
        $data['pw_sec'] = (int)($userData['pw_sec']);
        $data['password2'] = sha1(substr($userData['password'], 0, -1));
        $data['key'] = sha1($userData['password'].rand(10000,99999));
        $data['oss_path'] = md5($userData['username']);
        $data['data_used'] = $userData['data_used']?(int)$userData['data_used']:0;
        $data['create_date'] = date('Y-m-d H:i:s');

        $data['source'] = (int)($userData['source']);
        if(!$data['source']):
            $data['source'] = USER_SOUR_WEB;
        endif;
        $data['system_account'] = (int)($userData['system_account']);

        
        if($this->db->insert(TBL_USER,$data)){
            $data['id'] = $this->db->insert_id();
            return $data;
        }else{
            return false;
        }
    }
    
    function  register( $userData ) {
        $_data = array(
            'username'=>'',
            'contact'=>'',
            'display_name'=>'',
            'real_name'=>'',
            'birthdate'=>'',
            'gender'=>GENDER_M,
            'level'=>1,
            'active'=>1,
            'status'=>STATUS_LIVE,
            'email'=>'',
            'place'=>'',
            'avatar'=>'',
            'mobile'=>'',
            'password'=>'',
            'pw_sec'=>0,
            'data_used'=>0,
            'source'=>0,
            'system_account'=>0,
            'source'=>0
        );
        $data = array_merge($_data,$userData);
        return $this->create_new( $data ) ;
    }
    
    function  modify( $userData ) {
      $data['display_name'] = $userData['display_name'];
      $data['real_name'] = $userData['real_name'];
      $data['gender'] = ($userData['gender'])?GENDER_M:GENDER_F;
      $data['birthdate'] = $userData['birthdate'];
      $data['level'] = (int) $userData['level'];
      $data['star_id'] = (int) $userData['star_id'];
      $data['data_used'] = (int) $userData['data_used'];
      $data['active'] = (int) $userData['active'];
      $data['place'] = $userData['place'];
      $data['data_used'] = $userData['data_used']?(int)$userData['data_used']:0;
        $this->db->where('id', $userData['id']);
        return $this->db->update(TBL_USER, $data); 
    }
    
    function  update( $userData ) {
      $data['email'] = $userData['email'];
      $data['contact'] = $userData['contact'];
      $data['display_name'] = $userData['display_name'];
      $data['real_name'] = $userData['real_name'];
      $data['gender'] = ($userData['gender'])?GENDER_M:GENDER_F;
      $data['birthdate'] = $userData['birthdate'];
      
        $data['school'] = $userData['school'];
        $data['intro'] =$userData['intro'];
        $data['tags'] = $userData['tags'];
        $data['career'] = $userData['career'];
    
      $data_p['user_id'] = $userData['id'];
      if($userData['avatar'])
          $data['avatar'] = $userData['avatar'];
      //用输入代替选择 
//      $place = array();
//      $this->db->delete(TBL_UPLA, array('id' => $userData['id'])); 
//      for($i=0;$i<count($userData['p_country']);$i++){
//          $data_p['country'] = trim($userData['p_country'][$i]);
//          $data_p['state'] = trim($userData['p_state'][$i]);
//          if($data_p['country'] && $data_p['state']){
//            $this->db->insert(TBL_UPLA,$data_p);
//          }
//          $place[]="{$data_p['country']}-{$data_p['state']}";
//      }
//      $data['place'] = implode(",",$place);
      $data['place'] = $userData['place'];
        $this->db->where('id', $userData['id']);
        return $this->db->update(TBL_USER, $data);
    }
    
    function  updateFace( $userData ) {
      if($userData['avatar'])
        $data['avatar'] = $userData['avatar'];
        $this->db->where('id', $userData['id']);
        return $this->db->update(TBL_USER, $data);
    }
    
    function  updateDataUsed( $userData ) {
        $data['data_used'] = (int)$userData['data_used'];
        $this->db->where('id', $userData['id']);
        return $this->db->update(TBL_USER, $data);
    }
    
    function  updateStar( $user_id,$star_id ) {
        $user_id = (int)$user_id;
        $data['star_id'] = (int)$star_id;
        $this->db->where('id', $user_id);
        return $this->db->update(TBL_USER, $data);
    }
    
    function  update_basic( $userData ) {
        $data['password'] = sha1($userData['password']);
        $data['password2'] = sha1(substr($userData['password'], 0, -1));
        $data['pw_sec'] = (int)($userData['pw_sec']);
        $this->db->where('id', $userData['id']);
        return $this->db->update(TBL_USER, $data);
    }
    
    function update_meteor($userData){
        $r = $this->get_meteor($userData['id']);
        
        if(isset($userData['contactA'])) $data['contactA'] = (int)$userData['contactA'];
        if(isset($userData['contactB'])) $data['contactB'] = (int)$userData['contactB'];
        if(isset($userData['deadline'])) $data['deadline'] = $userData['deadline'];
        if(isset($userData['activeA'])) $data['activeA'] = $userData['activeA'];
        if(isset($userData['activeB'])) $data['activeB'] = $userData['activeB'];
        if(isset($userData['activeC'])) $data['activeC'] = $userData['activeC'];
        
        if($r !== false){
            $this->db->where('user_id', $userData['id']);
            return $this->db->update(TBL_UMET, $data);
        }else{
            $data['user_id'] = (int)$userData['id'];
            return $this->db->insert(TBL_UMET,$data);
        }
        
    }
    
    
    function update_meteor_c($userData){
        $r = $this->get_meteor($userData['id']);
        $data['deadline'] = $userData['deadline'];
        $data['activeC'] = $userData['activeC'];
        
        if($r !== false){
            $this->db->where('user_id', $userData['id']);
            return $this->db->update(TBL_UMET, $data);
        }else{
            $data['user_id'] = (int)$userData['id'];
            return $this->db->insert(TBL_UMET,$data);
        }
        
    }
    
    
    function update_meteor_b($userData){
        $r = $this->get_meteor($userData['id']);
        
        $data['contactA'] = (int)$userData['contactA'];
        $data['contactB'] = (int)$userData['contactB'];
        $data['activeB'] = $userData['activeB'];
        
        if($r !== false){
            $this->db->where('user_id', $userData['id']);
            return $this->db->update(TBL_UMET, $data);
        }else{
            $data['user_id'] = (int)$userData['id'];
            return $this->db->insert(TBL_UMET,$data);
        }
    }
    
    function update_meteor_a($userData){
        $r = $this->get_meteor($userData['id']);
        $data['activeA'] = $userData['activeA'];
        
        if($r !== false){
            $this->db->where('user_id', $userData['id']);
            return $this->db->update(TBL_UMET, $data);
        }else{
            $data['user_id'] = (int)$userData['id'];
            return $this->db->insert(TBL_UMET,$data);
        }
        
    }
    
    function set_favorite($favData){
        $data['user_id'] = (int)$favData['user_id'];
        $data['fav_id'] = (int)$favData['fav_id'];
        $data['type'] = $favData['type'];
        return $this->db->update(TBL_UFAV, $data);
    }
    //收藏的音乐
    function get_favorite($user_id,$type){
        $this->db->from(TBL_UFAV);
        $this->db->where(array('user_id'=>$user_id,'type'=>$type));
        $r = $this->db->get()->result();

        // The results of the query are stored in $login.
        // If a value exists, then the user account exists and is validated
        if ( is_array($r)) {
            return $r[0];
        }
        return false;
    }
    
    function  get_user_by_id( $id) {
        $id = (int)$id;
        if(!$id):
            return false;
        endif;
        $query = $this->db->get_where(TBL_USER, array('id' => $id), 1, 0);
        if ($query->num_rows() > 0) {
            $post = $query->row_array();
        }else{
            return false;
        }
        return $post;
    }
    
    function  get_user( $userData ,$num,$offset=0) {
        $this->db->from(TBL_USER);
        $option = array();
        if(isset($userData['username'])) $this->db->like('username', $userData['username']);
        if(isset($userData['email'])) $this->db->like('email', $userData['email']);
        if(isset($userData['real_name'])) $this->db->like('real_name', $userData['real_name']);
        if(($userData['from'])) $this->db->where('create_date >=', $userData['from']);
        if(($userData['to'])) $this->db->where('create_date <=', $userData['to']);
        if(isset($userData['status'])) $this->db->where('status', $userData['status']);
        
        if($num)   $this->db->limit( $num, $offset);
        $this->db->order_by('id','desc');
        $users = $this->db->get()->result_array();
        
        if( is_array($users) && count($users) > 0 ) {
          return $users;
        }
        return false;
    }
    
    function  get_user_number( $userData) {
        $this->db->from(TBL_USER);
        $option = array();
        if(isset($userData['username'])):
            $this->db->like('username', $userData['username']);
        else:
            $this->db->not_like('username', 'eeev');
        endif;
        if(isset($userData['email'])) $this->db->like('email', $userData['email']);
        if(isset($userData['real_name'])) $this->db->like('real_name', $userData['real_name']);
        if(isset($userData['from'])) $this->db->where('create_date >=', $userData['from']);
        if(isset($userData['to'])) $this->db->where('create_date <=', $userData['to']);
        if(isset($userData['status'])) $this->db->where('status', $userData['status']);
        $r = $this->db->count_all_results();
        return $r;
    }
    
    function get_meteor($user_id){
        $this->db->from(TBL_UMET);
        $this->db->where(array('user_id'=>$user_id));
        $this->db->limit( 1, 0);
        $this->db->order_by('id','desc');
        $r = $this->db->get()->result_array();

        if( is_array($r) && count($r) > 0 ) {
          return $r[0];
        }
        return false;
    }
    
    function get_relative($user_id){
        $this->db->from(TBL_UREL);
        $this->db->where(array('user_id'=>$user_id));
        $this->db->order_by('id','asc');
        $r = $this->db->get()->result_array();

        if( is_array($r) && count($r) > 0 ) {
          return $r;
        }
        return false;
    }
    function remove_relative_by_id($id,$user_id){
        return $this->db->delete(TBL_UREL, array('user_id' => $user_id,'id'=>$id)); 
    }
    
    function add_relative($relativeData){
        $data['user_id'] = $relativeData['user_id'];
        $data['target'] = $relativeData['target'];
        $data['relative'] = $relativeData['relative'];
        $r = $this->db->insert(TBL_UREL,$data);
        if($r){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }
    
    function user_search($k){//搜索用户 username or real name
        $this->db->from(TBL_USER);
        $k = str_replace('"', '', $k);
        $k = str_replace('%', '', $k);
        //$this->db->like('username',$k );//maybe input username 
        //$this->db->or_like('real_name',$k );//maybe input username 
        $this->db->where('(username LIKE "%'.$k.'%" OR real_name LIKE "%'.$k.'%")', null, false);
        $this->db->where('(status = "'.STATUS_DEAD.'" OR status = "'.STATUS_CREA.'")',null,false );//only for dead
        $r = $this->db->get()->result_array();
        
        //echo $this->db->last_query();
        if ( is_array($r) && count($r) > 0) {
            return $r;
        }
        return false;
    }
    
    function get_user_by_username($username){
        $this->db->from(TBL_USER);
        $this->db->where('username',$username );//maybe input username 
        $login = $this->db->get()->result_array();
        if ( is_array($login) && count($login) > 0) {
            return  $login[0];
        }
        return false;
    }
    
    function get_user_by_mobile($username){
        $this->db->from(TBL_USER);
        $this->db->where('mobile',$username );//maybe input username 
        $login = $this->db->get()->result_array();
        if ( is_array($login) && count($login) > 0) {
            return  $login[0];
        }
        return false;
    }
    function get_user_by_email($email){
        $this->db->from(TBL_USER);
        $this->db->where('email',$email );//maybe input username 
        $login = $this->db->get()->result_array();
        if ( is_array($login) && count($login) > 0) {
            return  $login[0];
        }
        return false;
    }
    
    function user_existing($username){
        $this->db->from(TBL_USER);
        $this->db->where('username',$username );//maybe input username 
        $login = $this->db->get()->result();
        if ( is_array($login) && count($login) > 0) {
            $this->details = $login[0];
            return true;
        }
        return false;
    }
    
    function email_existing($email){
        $this->db->from(TBL_USER);
        $this->db->where('email',$email );//maybe input username 
        $login = $this->db->get()->result();
        if ( is_array($login) && count($login)  > 0 ) {
            $this->details = $login[0];
            return true;
        }
        return false;
    }

    function get_fav_social_list($user_id,$type,$mode,$page=0,$num=20){
      $sql = "SELECT A1.id as id,A1.title as title ,A1.mode as mode,A1.k as k , left(A1.content,200) as content,B1.series,A1.user_id as user_id
              FROM `".$this->db->dbprefix(TBL_PSOC)."` AS A1
              JOIN `".$this->db->dbprefix(TBL_UFAP)."` AS B1 ON B1.`fav_id` = A1.`id`
              WHERE B1.`type` =  '".POST_TYPE_BIOG."'
              AND B1.`user_id` =  '$user_id'
              AND B1.`series` =  '".FAV_SHAR."'".$c1."
              ORDER BY B1.id DESC
              LIMIT ".($page*10).",".$num;
      
        $r = $this->db->query($sql)->result_array();   
        if( is_array($r) && count($r) > 0 ) {
          return $r;
        }
        return false;
    }
    
  function get_fav_post_list($user_id,$type,$mode,$page=0,$num=20) {
      switch($type){
          case POST_TYPE_ENTR:
              $_tbl = TBL_ENTR;
              
                $this->db->select($this->db->dbprefix($_tbl).'.*');
                $this->db->from($_tbl);
                $this->db->join(TBL_UFAP, $this->db->dbprefix(TBL_UFAP).'.fav_id = '.$this->db->dbprefix($_tbl).'.id');
                $this->db->where(array($this->db->dbprefix(TBL_UFAP).'.type'=>$type,$this->db->dbprefix(TBL_UFAP).'.user_id'=>$user_id));
                $this->db->limit( $num ,$page*10);
                $r = $this->db->get()->result_array();
                
              break;
          default :
              $_tbl = TBL_PUBL;
              if($mode){
                  $c1 = " AND B1.`mode` = '$mode'";
                  $c2 = " AND B2.`mode` = '$mode'";
              }
              
//            $sql = "SELECT A1.id as id,A1.title as title , left(A1.content,200) as content,B1.series,A1.user_id as user_id
//                    FROM `".$this->db->dbprefix(TBL_PUBL)."` AS A1
//                    JOIN `".$this->db->dbprefix(TBL_UFAP)."` AS B1 ON B1.`fav_id` = A1.`id`
//                    WHERE B1.`type` =  '".POST_TYPE_BIOG."'
//                    AND B1.`user_id` =  '$user_id'
//                    AND B1.`series` =  '".FAV_POST."'".$c1."
//                    UNION
//                    SELECT A2.id as id,C2.title as title,left(C2.content,200) as content,B2.series,C2.user_id as user_id
//                    FROM `".$this->db->dbprefix(TBL_TREE)."` AS A2
//                    JOIN `".$this->db->dbprefix(TBL_UFAP)."` AS B2 ON B2.`fav_id` = A2.`id`
//                    JOIN `".$this->db->dbprefix(TBL_PCON)."` AS C2 ON A2.`contribute_id` = C2.`id`
//                    WHERE B2.`type` =  '".POST_TYPE_BIOG."'
//                    AND B2.`user_id` =  '$user_id'
//                    AND B2.`series` =  '".FAV_TREE."'".$c2."
//                    LIMIT ".($page*10).",".$num;
            $sql = "SELECT A1.id as id,A1.title as title , left(A1.content,200) as content,B1.id as fav_post_id, B1.series,A1.user_id as user_id
                    FROM `".$this->db->dbprefix(TBL_PUBL)."` AS A1
                    JOIN `".$this->db->dbprefix(TBL_UFAP)."` AS B1 ON B1.`fav_id` = A1.`id`
                    WHERE B1.`type` =  '".POST_TYPE_BIOG."'
                    AND B1.`user_id` =  '$user_id'
                    AND B1.`series` =  '".FAV_POST."'".$c1."
                    UNION
                    SELECT A2.id as id,C2.title as title,left(C2.content,200) as content,B2.id as fav_post_id, B2.series,C2.user_id as user_id
                    FROM `".$this->db->dbprefix(TBL_TREE)."` AS A2
                    JOIN `".$this->db->dbprefix(TBL_UFAP)."` AS B2 ON B2.`fav_id` = A2.`id`
                    JOIN `".$this->db->dbprefix(TBL_PCON)."` AS C2 ON A2.`contribute_id` = C2.`id`
                    WHERE B2.`type` =  '".POST_TYPE_BIOG."'
                    AND B2.`user_id` =  '$user_id'
                    AND B2.`series` =  '".FAV_TREE."'".$c2."
                    ORDER BY fav_post_id DESC
                    LIMIT ".($page*10).",".$num;
            
                $r = $this->db->query($sql)->result_array();
                
              break;
      }
        
        
        
        //echo $this->db->last_query();

        if( is_array($r) && count($r) > 0 ) {
          return $r;
        }
        return false;
  }
  function get_fav_user_list($user_id,$page=0,$num=20) {
        $this->db->select($this->db->dbprefix(TBL_USER).'.*');
        $this->db->from(TBL_USER);
        $this->db->join(TBL_UFAU, $this->db->dbprefix(TBL_UFAU).'.fav_id = '.$this->db->dbprefix(TBL_USER).'.id');
        $this->db->where(array($this->db->dbprefix(TBL_UFAU).'.user_id'=>$user_id));
        $this->db->limit( $num ,$page*10);
        $r = $this->db->get()->result_array();

        if( is_array($r) && count($r) > 0 ) {
          return $r;
        }
        return false;
  }
  
  function get_fav_post($user_id,$fav_id,$type,$series) {
        $this->db->from(TBL_UFAP);
        $this->db->where(array('user_id'=>$user_id,'fav_id'=>$fav_id,'series'=>$series));
        $this->db->limit(1);
        $r = $this->db->get()->result();
        //echo $this->db->last_query();
        if( is_array($r) && count($r) > 0 ) {
          return $r;
        }
        return false;
  }
    
  function get_fav_user($user_id,$fav_id) {
        $this->db->from(TBL_UFAU);
        $this->db->where(array('user_id'=>$user_id,'fav_id'=>$fav_id));
        $this->db->limit(1);
        $r = $this->db->get()->result();
        if( is_array($r) && count($r) > 0 ) {
          return $r;
        }
        return false;
  }
  
  function add_fav_user($user_id,$fav_id) {
        $data['user_id'] = $user_id;
        $data['fav_id'] = $fav_id;
        return $this->db->insert(TBL_UFAU,$data);
  }
  
  function remove_fav_user($user_id,$fav_id) {
        $r= $this->db->delete(TBL_UFAU, array('user_id' => $user_id,'fav_id'=>$fav_id)); 
        return $r;
  }
    
  function add_fav_post($user_id,$fav_id,$type,$mode,$series) {
        $data['user_id'] = $user_id;
        $data['fav_id'] = $fav_id;
        $data['type'] = $type;
        $data['mode'] = $mode;
        $data['series'] = $series;
        return $this->db->insert(TBL_UFAP,$data);
  }
  function remove_fav_post($user_id,$fav_id,$type,$series) {
        $r= $this->db->delete(TBL_UFAP, array('user_id' => $user_id,'fav_id'=>$fav_id,'series'=>$series)); 
        
        //echo $this->db->last_query();
        return $r;
  }
    
    function login_log($email,$status){
        $data['email'] = $email;
        $data['username'] = $email;
        $data['datetime'] = date('Y-m-d H:i:s');
        $data['status'] = $status;
        $data['ip'] = ip2long (GetRemoteIP());
        return $this->db->insert(TBL_ULOG,$data);
    }
    
    function get_error_times($email,$time=600){//默认过去10分钟
        $this->db->from(TBL_ULOG);
        $this->db->where('status',1);
        $this->db->where('username',$email);
        $this->db->order_by("datetime", "desc"); 
        $this->db->limit(0,1);
        $r = $this->db->get()->result_array();
        if( is_array($r) && count($r) > 0 ) {
          $last_time = $r[0]['datetime'];
        }
        
        $this->db->from(TBL_ULOG);
        $this->db->where('status >',1);
        $this->db->where('username',$email);
        $this->db->where('datetime >', date('Y-m-d H:i:s',time() - $time));
        if($last_time) $this->db->where('datetime >', $last_time);
        return $this->db->count_all_results();
    }
    
    function is_first_log($email){
        $this->db->from(TBL_ULOG);
        $this->db->where('username',$email );//maybe input username 
        $login = $this->db->get()->result();
        if ( is_array($login) && count($login) < 2) {
            return true;
        }
        return false;
    }
    
    function bindMobile($mobile,$id){
        $data = array(
               'mobile' => $mobile
            );
        $this->db->where('id', $id);
        $this->db->update(TBL_USER,$data); 
    }
    function unbindMobile($id){
        $data = array(
               'mobile' => ''
            );
        $this->db->where('id', $id);
        $this->db->update(TBL_USER,$data); 
    }
    
    function updateVIP($vip_deadline,$id){
        $data = array(
               'vip_deadline' => $vip_deadline
            );
        $this->db->where('id', $id);
        $this->db->update(TBL_USER,$data); 
    }
    
    function updateSpace($space,$id){
        $data = array(
               'space' => $space
            );
        $this->db->where('id', $id);
        $this->db->update(TBL_USER,$data); 
    }
    
    
    function remove($id){
        $this->db->delete(TBL_USER, array('id' => $id)); 
    }
    function unactive($id){
        $data = array(
               'active' => ACTIVE_F
            );
        $this->db->where('id', $id);
        $this->db->update(TBL_USER,$data); 
    }
    function active($id){
        $data = array(
               'active' => ACTIVE_T
            );
        $this->db->where('id', $id);
        $this->db->update(TBL_USER,$data); 
    }
    
    function get_value($user_id,$option){
        $this->db->from("user_setting");
        $this->db->where('user_id', $user_id );
        $this->db->where('option', $option);
        $r = $this->db->get()->result_array();
        if ( is_array($r) && (count($r) > 0)) {
            return $r[0]['value'];
        }
        return false;
    }
    function set_value($user_id,$option,$value){
        $data = array(
               'value' => $value
            );
        $this->db->where('user_id', $user_id);
        $this->db->where('option', $option);
        $this->db->update(TBL_USET,$data); 
    }
    function add_value($user_id,$option,$value){
        $data = array(
               'user_id' => $user_id,
               'option' => $option,
               'value' => $value
            );
        return $this->db->insert(TBL_USET,$data); 
    }
    
    
    
    function update_social($user_id,$union_id,$type){
        $data = array(
            'union_id'=>$union_id
        );
        $this->db->where('user_id', $user_id);
        $this->db->where('type', $type);
        $this->db->update(TBL_USES,$data); 
    }
    function update_openid($user_id,$open_id,$type){
        $data = array(
            'open_id'=>$open_id
        );
        $this->db->where('user_id', $user_id);
        $this->db->where('type', $type);
        $this->db->update(TBL_USES,$data); 
    }
    function remove_social($user_id,$union_id,$type){
        $data = array(
            'union_id'=>$union_id,
            'type'=>$type,
        );
        if($user_id){
            $data['user_id'] = $user_id;
        }
        $this->db->where($data);
        return $this->db->delete(TBL_USES,$data); 
    }
    function add_social($user_id,$union_id,$open_id,$type){
        if(!$user_id || !$open_id || !$union_id):
            return false;
        endif;
        $data = array(
            'user_id'=>$user_id,
            'union_id'=>$union_id,
            'open_id'=>$open_id,
            'type'=>$type,
            'status'=>1
        );
        return $this->db->insert(TBL_USES,$data); 
    }
    function valid_social($user_id,$union_id,$type){
        if(!$user_id || !$union_id):
            return false;
        endif;
        $this->db->from(TBL_USES);
        $data = array(
            'user_id'=>$user_id,
            'union_id'=>$union_id
        );
        $this->db->where($data);//maybe input username 
        $login = $this->db->get()->result_array();
        if ( is_array($login) && count($login) > 0) {
            return  $login[0];
        }
        return false;
    }
    
    function clean_social($user_id,$union_id,$type){
        $data = array(
            'user_id != '.$user_id=> null,
            'union_id'=>$union_id,
            'type'=>$type
        );
        $this->db->where($data);
        $this->db->delete(TBL_USES,$data); 
        
        $data = array(
            'user_id'=> $user_id,
            'union_id != "'.$union_id.'"'=> null,
            'type'=>$type
        );
        $this->db->where($data);
        $this->db->delete(TBL_USES,$data); 
    }
    
    function remove_openid($user_id,$open_id,$type){
        $data = array(
            'open_id'=>$open_id,
            'type'=>$type,
        );
        if($user_id){
            $data['user_id'] = $user_id;
        }
        $this->db->where($data);
        return $this->db->delete(TBL_USES,$data); 
    }
    
    function add_openid($user_id,$openid,$type){
        $data = array(
            'user_id'=>$user_id,
            'open_id'=>$openid,
            'type'=>$type,
            'status'=>1
        );
        return $this->db->insert(TBL_USES,$data); 
    }
    
    function get_social($user_id,$type){
        $this->db->select('user_id, union_id, open_id,subscribe,valid');
        $this->db->from(TBL_USES);
        if($user_id){
            $data['user_id'] = $user_id;
        }else{
            return false;
        }
//        $data = array(
//            'type'=>$type,
//            'union_id != ""'=>NULL,
//            'status'=>1
//        );
//        $data = array(
//            'union_id IS NOT NULL'=>NULL
//        );
        $data['(union_id IS NOT NULL OR open_id IS NOT NULL)'] = NULL;
        $this->db->where($data);//maybe input username 
        $login = $this->db->get()->result_array();
        
        if ( is_array($login) && count($login) > 0) {
            return  $login[0];
        }
        return false;
    }
    
    function get_user_by_union_id($union_id,$type=1){
        if(!$union_id) return false;
        $sql = "SELECT A.* FROM ".$this->db->dbprefix(TBL_USER)." AS A LEFT JOIN ".
                    $this->db->dbprefix(TBL_USES)." AS B ON A.id = B.user_id WHERE"
                . " B.union_id = '$union_id' LIMIT 0,10";

        $r = $this->db->query($sql)->result_array();   
        if( is_array($r) && count($r) > 0 ) {
            return $r[0];
        }else{
            return false;
        }
        
    }
    
    function get_user_by_open_id($open_id,$type){
        $this->db->from(TBL_USES);
        $data = array(
            'type'=>$type,
            'open_id'=>$open_id.'',
            'status'=>1
        );
        $this->db->where($data);//maybe input username 
        $login = $this->db->get()->result_array();
        
        if ( is_array($login) && count($login) > 0) {
            $user_id = $login[0]['user_id'];
            return $this->get_user_by_id($user_id);
        }else{
            return false;
        }
        
    }
    
    function get_vip_users(){
//      $sql = "SELECT B.* FROM `".$this->db->dbprefix(TBL_UMET)."` as A,`".$this->db->dbprefix(TBL_USER)."` as B 
//          WHERE A.user_id = B.id 
//          AND A.deadline <= '".date('Y-m-d H:i:s',strtotime('Yesterday'))."' 
//          AND A.deadline > '2010-01-01 00:00:00' 
//          AND A.activeC = 1 
//          AND B.vip_deadline > '".date('Y-m-d H:i:s',strtotime('Yesterday'))."'
//          AND B.status = '".STATUS_LIVE."'";
      $sql = "SELECT B.* FROM `".$this->db->dbprefix(TBL_UMET)."` as A,`".$this->db->dbprefix(TBL_USER)."` as B 
          WHERE A.user_id = B.id 
          AND A.deadline <= '".date('Y-m-d H:i:s')."' 
          AND A.deadline > '2010-01-01 00:00:00' 
          AND A.activeC = 1 
          AND B.vip_deadline > '".date('Y-m-d H:i:s')."'
          AND B.status = '".STATUS_LIVE."'";
      //echo $sql;
//      $sql = "SELECT A1.id as id,A1.title as title ,A1.mode as mode,A1.k as k , left(A1.content,200) as content,B1.series,A1.user_id as user_id
//              FROM `".$this->db->dbprefix(TBL_PSOC)."` AS A1
//              JOIN `".$this->db->dbprefix(TBL_UFAP)."` AS B1 ON B1.`fav_id` = A1.`id`
//              WHERE B1.`type` =  '".POST_TYPE_BIOG."'
//              AND B1.`user_id` =  '$user_id'
//              AND B1.`series` =  '".FAV_SHAR."'".$c1."
//              ORDER BY B1.id DESC
//              LIMIT ".($page*10).",".$num;
      
        $r = $this->db->query($sql)->result_array();   
        if( is_array($r) && count($r) > 0 ) {
          return $r;
        }
        return false;
        
    }
    
    function get_user_space_used($username){
        $path = FILE_FOLDER.$username[0].'/'.$username.'/';
        $totalsize = 0; 
        $totalcount = 0; 
        $dircount = 0; 
        if ($handle = opendir ($path)) 
        {
          while (false !== ($file = readdir($handle))) 
          { 
            $nextpath = $path . '/' . $file; 
            if ($file != '.' && $file != '..' && !is_link ($nextpath)) 
            { 
              if (is_dir ($nextpath)) 
              { 

              } 
              elseif (is_file ($nextpath)) 
              { 
                $totalsize += filesize ($nextpath); 
                $totalcount++; 
              } 
            } 
          } 
        } 
        closedir ($handle); 
        $size = $totalsize;
        $size = $size >> 20;
        return $size;
    }
    
    
    function update_mobile($mobile,$id){
        $id = (int)$id;
        if(!$id) return false;
        $data = array(
               'mobile' => $mobile
            );
        $this->db->where('id', $id);
        $this->db->update(TBL_USER,$data); 
        return;
    }
    
    
    function update_fd($name,$ext_year,$id){
        $id = (int)$id;
        $data = array(
               'display_name' => $name,
               'real_name' => $name,
               'ext_year' => $ext_year
            );
        $this->db->where('id', $id);
        $this->db->update(TBL_USER,$data); 
        return;
    }
    
    function create_temp_user($union_id,$open_id,$data = array()){
        $userData = create_wx_temp_user();
        
        if(isset($data['real_name'])) $userData['real_name'] = $data['real_name'];
        if(isset($data['display_name'])) $userData['display_name'] = $data['display_name'];
        if(isset($data['source'])) $userData['source'] = $data['source'];
        
        $userData['system_account'] = 1;
        
        $this->load->model('user_model');
        $user_id = $this->register($userData);
        
        if(empty($user_id)):
            return false;
        endif;
        
        if(!$union_id):
            $union_id = $open_id;
        endif;
        
        //绑定微信号
        if($union_id):
            $this->add_social($user_id,$union_id,$open_id,SOCI_WXIN);
        elseif($open_id):
            $this->add_openid($user_id,$open_id,SOCI_WXIN);
        endif;
        return $user_id;
    }
    
    
    
    function  add_location( $userData) {
        $data['user_id'] = (int)$userData['user_id'];
        $data['latitude'] = (float)$userData['latitude'];
        $data['longitude'] = (float)$userData['longitude'];
        $data['ip'] = GetClientIP();
        
        if($this->db->insert(TBL_ULOC,$data)){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }
    
    function  get_locations( $ids) {
        $this->db->from(TBL_ULOC);
        
        if(!is_array($ids)):
            $ids = (int)$ids;
            $this->db->where('id', $ids);
        else:
            $ids = explode(",",$ids);
            $this->db->where_in('id', $ids);
        endif;
        
        return $this->db->get()->result_array();
    }
    
    function unsubscribe($union_id){
        $data = array(
            'subscribe' => 0
        );
        $this->db->where('union_id', $union_id);
        return $this->db->update(TBL_USES,$data); 
        
    }
    function subscribe($union_id){
        $data = array(
            'subscribe' => 1
        );
        $this->db->where('union_id', $union_id);
        return $this->db->update(TBL_USES,$data); 
    }
    function valid($union_id,$val=1){
        $data = array(
            'valid' => $val
        );
        $this->db->where('union_id', $union_id);
        return $this->db->update(TBL_USES,$data); 
    }
}
