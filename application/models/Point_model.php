<?php
class Point_Model extends CI_Model {
    function create_new( $pointData ) {
        $data['user_id'] = $pointData['user_id'];
        $data['point'] = (int)$pointData['point'];
        $data['memo'] = $pointData['memo'];
        $data['type'] = $pointData['type'];
        $data['ext1'] = $pointData['ext1'];
        $data['ext2'] = $pointData['ext2'];
        $data['ext3'] = $pointData['ext3'];
      
        if ( $this->db->insert(TBL_POIN,$data) ) {
          return $this->db->insert_id();
        } else {
          return false;
        }
    }
    
    function add_post($user_id,$post_id){
        $this->config->load('post', TRUE);
        $post_point = $this->config->item('post_point', 'post');
                        
        $num_post = $this->is_ava($post_point['point_type_post'],$user_id);
        $point = 0;
        if($num_post < $post_point['point_limit_post']):
            $point = $post_point['point_post'];
            $pointData = array(
                'user_id'=>$user_id,
                'point' => $point,
                'memo' => 'create new post '.$post_id,
                'type' => $post_point['point_type_post'],
                'ext1' => $post_id
            );
            $this->create_new($pointData);
        endif;
        return $point;
    }
    
    function add_publ($user_id,$post_id){
        $this->config->load('post', TRUE);
        $post_point = $this->config->item('post_point', 'post');
        $num_post = $this->is_ava($post_point['point_type_publ'],$user_id);
        $point = 0;
        if($num_post < $post_point['point_limit_publ']):
            $point = $post_point['point_publ'];
            $pointData = array(
                'user_id'=>$user_id,
                'point' => $point,
                'memo' => 'public new post '.$post_id,
                'type' => $post_point['point_type_publ'],
                'ext1' => $post_id
            );
            $this->create_new($pointData);
        endif;
        return $point;
    }
     
    function add_like($user_id,$post_id){
        $this->config->load('post', TRUE);
        $post_point = $this->config->item('post_point', 'post');
        $num_post = $this->is_ava($post_point['point_type_like'],$user_id);
        $point = 0;
        if($num_post < $post_point['point_limit_like']):
            $point = $post_point['point_like'];
            $pointData = array(
                'user_id'=>$user_id,
                'point' => $point,
                'memo' => 'like post '.$post_id,
                'type' => $post_point['point_type_like'],
                'ext1' => $post_id
            );
            $this->create_new($pointData);
        endif;
        return $point;
    }
    
    function add_likd($user_id,$post_id){
        $this->config->load('post', TRUE);
        $post_point = $this->config->item('post_point', 'post');
        $point = $post_point['point_likd'];
        $pointData = array(
            'user_id'=>$user_id,
            'point' => $point,
            'memo' => 'someone like your post '.$post_id,
            'type' => $post_point['point_type_likd'],
            'ext1' => $post_id
        );
        $this->create_new($pointData);
        return $point;
    }
    
    function add_shar($user_id,$post_id){
        $this->config->load('post', TRUE);
        $post_point = $this->config->item('post_point', 'post');
        $num_post = $this->is_ava($post_point['point_type_shar'],$user_id,0,array('ext1'=>$post_id));
        $point = 0;
        if($num_post < 1):
            $point = $post_point['point_shar'];
            $pointData = array(
                'user_id'=>$user_id,
                'point' => $point,
                'memo' => 'share post '.$post_id,
                'type' => $post_point['point_type_shar'],
                'ext1' => $post_id
            );
            $this->create_new($pointData);
        endif;
        return $point;
    }
    
    function add_shad($user_id,$post_id){
        $this->config->load('post', TRUE);
        $post_point = $this->config->item('post_point', 'post');
        $point = $post_point['point_shad'];
        $pointData = array(
            'user_id'=>$user_id,
            'point' => $point,
            'memo' => 'someone share your post '.$post_id,
            'type' => $post_point['point_type_shad'],
            'ext1' => $post_id
        );
        $this->create_new($pointData);
        return $point;
    }
    
    function is_ava($type,$user_id,$day=1,$ext=array()){
        if(!$user_id):
            return false;
        endif;
        if(!$type):
            return false;
        endif;
        
        if($ext):
            foreach($ext as $k=>$v):
                $this->db->where($k, $v);
            endforeach;
        endif;
        
        $this->db->where('type', $type);
        $this->db->where('user_id', $user_id);
        if($day):
            $this->db->where('createdate >=', date('Y-m-d',time()).' 00:00:00');
        endif;
        return $this->db->count_all_results(TBL_POIN);
        
    }
    
    function increase_point($point,$user_id){
        if(!$user_id):
            return false;
        endif;
        $p = (int)$point;
        if(!$point):
            return false;
        endif;
        $this->db->set('point', 'point+'.$p, FALSE);
        $this->db->where('id', $user_id);
        return $this->db->update(TBL_USER); 
        
    }
    
    function use_product($user_id,$point,$order_id){
        $pointData = array(
            'user_id'=>$user_id,
            'point' => $point,
            'memo' => 'convert for product '.$order_id,
            'type' => -1,
            'ext1' => $order_id
        );
        $this->create_new($pointData);
        return $point;
    }
    
    function decrease_point($point,$user_id){
        if(!$user_id):
            return false;
        endif;
        $p = (int)$point;
        if(!$point):
            return false;
        endif;
        $this->db->set('point', 'point-'.$p, FALSE);
        $this->db->where('id', $user_id);
        return $this->db->update(TBL_USER); 
        
    }
    
    
    function get_point_table_by_user_id($user_id,$createdate){
        if(!$user_id):
            return false;
        endif;
        
        $this->db->from(TBL_POIN);
        $this->db->where('user_id', $user_id);
        
        $this->db->select('DATE_FORMAT (`createdate`,"%m-%d") as date',FALSE);
        $this->db->select('SUM(point) as  points',FALSE);
        
        $this->db->where('createdate >=', $createdate);
        $this->db->order_by('createdate', 'desc');
        $this->db->group_by('date');
        
        $posts = $this->db->get()->result_array();
        if( is_array($posts) && count($posts) > 0 ) {
          return $posts;
        }
        return false;
        
    }
    
    function get_point_list_by_user_id($user_id,$createdate=''){
        if(!$user_id):
            return false;
        endif;
        
        $this->db->select('*');
        $this->db->from(TBL_POIN);
        $this->db->where('user_id', $user_id);
        if($createdate):
            $this->db->where('createdate >=', $createdate);
        endif;
        $this->db->order_by('createdate', 'desc');
        
        $posts = $this->db->get()->result_array();
        if( is_array($posts) && count($posts) > 0 ) {
          return $posts;
        }
        return false;
        
    }
  
}
