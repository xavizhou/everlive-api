<?php
class Album_Model extends CI_Model {
    function get_albums($user_id ,$offset=0,$num_posts=999){
        if(!$user_id):
            return false;
        endif;
        $this->db->from(TBL_ALBU);
        $this->db->where( 'user_id' ,$user_id);
        $this->db->limit( $num_posts ,$offset);
        $this->db->order_by('create_date','desc');
        $posts = $this->db->get()->result_array();

        if( is_array($posts) && count($posts) > 0 ) {
          return $posts;
        }
        return false;
    }
    
    function get_album_by_id($id ,$user_id){
        if(!$user_id || !$id):
            return false;
        endif;
        $query = $this->db->get_where(TBL_ALBU, array('id' => $id,'user_id'=>$user_id), 1, 0);
        if ($query->num_rows() > 0) {
            $post = $query->row_array();
        }else{
            return false;
        }
        return $post;
    }
    function get_album_by_token($token){
        if(!$token):
            return false;
        endif;
        $query = $this->db->get_where(TBL_ALBU, array('token' => $token), 1, 0);
        if ($query->num_rows() > 0) {
            $post = $query->row_array();
        }else{
            return false;
        }
        return $post;
    }
    
    function get_album_detail($album_id){
        if(!$album_id):
            return false;
        endif;
        
        $sql = 'SELECT A.text,B.file,B.type,B.place,B.date,B.user_id,C.avatar,C.display_name FROM '.
                $this->db->dbprefix(TBL_ALBR).' AS A'.
                ' LEFT JOIN '.$this->db->dbprefix(TBL_RESC).' AS B ON A.resource_id = B.id'.
                ' LEFT JOIN el_user AS C ON C.id = B.user_id'.
                ' WHERE'.
                ' A.`album_id` = "'.$album_id.'" ORDER BY `order` DESC, A.`id` ASC';
        $r = $this->db->query($sql)->result_array();
        if ( is_array($r) && count($r) > 0) {
            return $r;
        }
        return false;
    }
    
    
    function remove_album($id,$user_id){
        if(!$id || !$user_id):
            return false;
        endif;
        $this->db->delete($this->db->dbprefix(TBL_ALBR), array('album_id' => $id)); 
        return $this->db->delete($this->db->dbprefix(TBL_ALBU), array('id' => $id,'user_id'=>$user_id)); 
    }
    
    
    function create_album($albumData) {
        if(!$albumData['user_id'] || !$albumData['res_id']):
            return false;
        endif;
        
        $_default_data = array(
            'title'=>'',
            'thumb'=>'',
            'thumb2'=>'',
            'thumb3'=>'',
            'num'=>0,
            'token'=>md5(time().rand(000,999))
        );
        $albumData =  $albumData + $_default_data ;
        
        
        $data['user_id'] = $albumData['user_id'];
        $data['title'] = $albumData['title'];
        $data['thumb'] = $albumData['thumb'];
        $data['thumb2'] = $albumData['thumb2'];
        $data['thumb3'] = $albumData['thumb3'];
        $data['num'] = $albumData['num'];
        $data['token'] = $albumData['token'];
        
        if ( $this->db->insert(TBL_ALBU,$data) ) {
            $album_id = $this->db->insert_id();
            $res_ids = $albumData['res_id'];
            
            if(!is_array($res_ids)):
                $res_ids = explode(",", $albumData['res_id']);
            endif;
          
            $data = array();
            foreach($res_ids as $res_id):
                $data[] = array(
                    'resource_id'=>$res_id,
                    'text'=>'',
                    'album_id'=>$album_id
                );
            endforeach;
            $this->db->insert_batch(TBL_ALBR, $data); 
        
            return $album_id;
        } else {
          return false;
        }
    }
    
  
}
