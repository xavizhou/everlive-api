<?php
class Forest_Model extends CI_Model {
    
    //自己的树林
    function get_forest_by_id($user_id){
        if(empty($user_id)) return false;
        $sql = 'SELECT DISTINCT A.user_id,A.relative,A.is_owner,A.is_main,A.label,A.create_date,D.id,D.real_name,D.gender,D.display_name,D.username,D.avatar,D.email,D.contact,D.key FROM '.
                $this->db->dbprefix(TBL_UFOR).' AS A'.
                ' LEFT JOIN '.$this->db->dbprefix(TBL_USER).' AS D ON A.cid = D.id WHERE'.
                ' A.user_id = '.$user_id.
                ' ORDER BY A.is_main DESC,A.id ASC';
        $r = $this->db->query($sql)->result_array();
        
        if ( is_array($r) && count($r) > 0) {
            return $r;
        }
        return false;
    }
    
    
    //某小树的所有管理人员
    function get_user_by_cid($cid){
        if(empty($cid)) return false;
        $sql = 'SELECT DISTINCT A.user_id,A.relative,A.is_owner,A.is_main,A.label,A.create_date,D.id,D.real_name,D.gender,D.display_name,D.username,D.avatar ,D.key FROM '.
                $this->db->dbprefix(TBL_UFOR).' AS A'.
                ' LEFT JOIN '.$this->db->dbprefix(TBL_USER).' AS D ON A.user_id = D.id WHERE'.
                ' A.cid = '.$cid.
                ' ORDER BY A.id ASC';
        //echo $sql;
        $r = $this->db->query($sql)->result_array();
        if ( is_array($r) && count($r) > 0) {
            return $r;
        }
        return false;
        
        
        $this->db->from(TBL_UFOR);
        $where = array('cid' => $cid);
        $this->db->where($where);
        $r = $this->db->get()->result_array();
        if ( is_array($r) && count($r) > 0) {
            return $r;
        }
        return false;
    }
  function is_tree($user_id,$cid){
        $this->db->from(TBL_UFOR);
        $where = array('cid' => $cid,'user_id' => $user_id);
        $this->db->where($where);
        $r = $this->db->get()->result_array();
        if ( is_array($r) && count($r) > 0) {
            return $r;
        }
        return false;
  }
    
  function is_owner($user_id,$cid){
        $this->db->from(TBL_UFOR);
        $where = array('cid' => $cid,'user_id' => $user_id,'is_owner' => 1);
        $this->db->where($where);
        $r = $this->db->get()->result_array();
        if ( is_array($r) && count($r) > 0) {
            return $r;
        }
        return false;
  }
  function get_owner($cid){
        $this->db->from(TBL_UFOR);
        $where = array('is_owner' => 1,'cid' => $cid);
        $this->db->where($where);
        $r = $this->db->get()->result_array();
        if ( is_array($r) && count($r) > 0) {
            return $r[0];
        }
        return false;
  }
  function get_main_tree($user_id){
        $this->db->from(TBL_UFOR);
        $where = array('is_main' => 1,'user_id' => $user_id);
        $this->db->where($where);
        $r = $this->db->get()->result_array();
        if ( is_array($r) && count($r) > 0) {
            return $r[0];
        }
        return false;
  }
  
  function get_relative($user_id,$cid=''){
        if(empty($user_id)) return false;
//select distinct A.user_id,A.cid,A.label,C.display_name,D.open_id from el_user_forest A right join 
//(select distinct cid from el_user_forest where user_id = 46) B
//on A.cid = B.cid
// left join el_user C on A.cid = C.id
// left join el_user_social D on A.user_id = D.user_id
// where A.user_id != 46
        if($cid):
            $ext_c = ' AND A.cid = '.$cid;
        endif;        
        $sql = 'SELECT DISTINCT A.user_id,A.cid,A.label,C.display_name,D.open_id FROM '.
                $this->db->dbprefix(TBL_UFOR).' AS A'.
                ' RIGHT JOIN (SELECT DISTINCT cid from '.$this->db->dbprefix(TBL_UFOR).
                ' WHERE user_id = "'.$user_id.'") B '.
                ' ON A.cid = B.cid'.
                ' LEFT JOIN '.$this->db->dbprefix(TBL_USER).' C on A.cid = C.id'.
                ' LEFT JOIN '.$this->db->dbprefix(TBL_USES).' D on A.user_id = D.user_id'.
                ' WHERE A.user_id != '.$user_id.
                $ext_c;
        //echo $sql;
        $r = $this->db->query($sql)->result_array();
        if ( is_array($r) && count($r) > 0) {
            return $r;
        }
        return false;
  }
  
  function remove_partner($user_id,$cid){
      $c['cid'] = $cid;
      $c['user_id'] = $user_id;
      return $this->db->delete(TBL_UFOR, $c); 
  }
    
  function create_tree( $userData ) {
      $data['user_id'] = $userData['user_id'];
      $data['cid'] = $userData['cid'];
      $data['is_owner'] = $userData['is_owner'];
      $data['is_main'] = (int)$userData['is_main'];
      $data['label'] = $userData['label'];
      $data['create_date'] = date('Y-m-d H:i:s',time());
      $data['relative'] = $userData['relative'];
      $data['relative2'] = $userData['relative2'];
        if($this->db->insert(TBL_UFOR,$data)){
            return $this->db->insert_id();
        }else{
            return false;
        }
  }
  function delete_tree( $userData ) {
      $c['cid'] = $userData['cid'];
      $c['user_id'] = $userData['user_id'];
      return $this->db->delete(TBL_UFOR, $c); 
  }
  function delete_tree_by_owner ($userData ) {
      $c['cid'] = $userData['cid'];
      return $this->db->delete(TBL_UFOR, $c); 
  }
  
    function get_invitation_by_cid($uid){
        $this->db->from(TBL_UFOC);
        $where = array('cid' => $uid,'status' => 0);
        $this->db->where($where);
        $r = $this->db->get()->result_array();
        if ( is_array($r) && count($r) > 0) {
            return $r;
        }
        return false;
    }
    
    
    function set_main($user_id,$cid){
        $data['is_main'] = 0;
        $this->db->where('user_id', $user_id);
        $this->db->update(TBL_UFOR, $data);
        
        $data['is_main'] = 1;
        $this->db->where('user_id', $user_id);
        $this->db->where('cid', $cid);
        return $this->db->update(TBL_UFOR, $data);
        
    }
    function set_label($label,$user_id,$cid){
        $data['label'] = $label;
        $this->db->where('user_id', $user_id);
        $this->db->where('cid', $cid);
        return $this->db->update(TBL_UFOR, $data);
        
    }
  function add_invite($userData){
      $data['user_id'] =(int) $userData['user_id'];
      $data['cid'] = (int)$userData['cid'];
      $data['email'] = $userData['email'];
      $data['code'] = $userData['code'];
      $data['status'] = 0;
      $data['create_time'] = date('Y-m-d H:i:s',time());
      $data['relative'] = (int)$userData['relative'];
        if($this->db->insert(TBL_UFOC,$data)){
            return $this->db->insert_id();
        }else{
            return false;
        }
  }
  function remove_invite($user_id,$id){
      $c['id'] = $id;
      if($user_id) $c['user_id'] = $user_id;
      $this->db->delete(TBL_UFOC, $c); 
  }
  
  function code_valid($code){
        $this->db->from(TBL_UFOC);
        $where = array('code' => $code,'status' => 0);
        $this->db->where($where);
        $r = $this->db->get()->result_array();
        if ( is_array($r) && count($r) > 0) {
            return $r[0];
        }
        return false;
  }
  function code_used($code){
        $data['status'] = 1;
        $this->db->where('code', $code);
        return $this->db->update(TBL_UFOC, $data);
  }
  
  
    function growup($id){
        $data['status'] = STATUS_LIVE;
        $this->db->where('id', $id);
        return $this->db->update(TBL_USER, $data);
    }
    
    function tree_post_give($user_id){
        return $this->db->delete(TBL_POST, array('tree' => 0,'user_id'=>$user_id)); 
    }
}
?>
