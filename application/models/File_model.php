<?php
class File_Model extends CI_Model {
        /*上传单个文件
         * $upload_file $_FILE 单个文件
         * $fileFolder 上传路径
         * $validateExt 允许的文件后缀
         * $key 加密需要的key
         * 
         * 返回
         * $filename 文件名
         */
    
    function upload(&$upload_file,$fileFolder, $key,$validateExt=array()){
            if($upload_file['size'] > 0){
                    $path_parts = pathinfo($upload_file['name']);
                    if(!empty($validateExt)){
                            if(!is_array($validateExt)) $validateExt = array($validateExt);
                            if(!in_array (strtolower($path_parts["extension"]),$validateExt)) return false;
                    }

                    $patterns = array("/[^(\w,\s)]+/","/^(\s)/","/(\s)+/");
                    $replacements = array("","","_");
                    $path_parts["filename"] = preg_replace($patterns, $replacements, $path_parts["filename"]);
                    if(empty($path_parts["filename"])) $path_parts["filename"] = rand(0,1000);

                    $prex = date("ymd").'_';
                    //$upload_file['name'] = $prex.$path_parts["filename"].'.'.strtolower($path_parts["extension"]);
                    $upload_file['name'] = time().rand(0,10000).'.'.strtolower($path_parts["extension"]);
                    $uploadfile  = $fileFolder.$upload_file['name'];
                    for($i=1;$i<1000;$i++){
                            if(file_exists($uploadfile)){
                                    $upload_file['name'] = $prex.$path_parts["filename"].'_'.$i.'.'.strtolower($path_parts["extension"]);
                                    $uploadfile  = $fileFolder.$upload_file['name'];
                            }else{
                                    break;
                            }
                    }
                    if(move_uploaded_file ($upload_file['tmp_name'], $uploadfile)==true){
                            $filename = urlencode($upload_file['name']);
                            //文件路径加密
                            if($key){
                                $filename = $this->path_encryption($filename,$key);
                            }
                            else{
                                $filename = $uploadfile;
                            }
                            //文件加密
                            $this->file_encryption($filename);
                    }else{
                            return false;
                    }
            }else{
                    return false;
            }
            return $filename;
    }
    
//    
//    function upload(&$upload_file,$fileFolder, $key,$validateExt=array()){
//            if($upload_file['size'] > 0){
//                    $path_parts = pathinfo($filename);
//                    if(!empty($validateExt)){
//                            if(!is_array($validateExt)) $validateExt = array($validateExt);
//                            if(!in_array (strtolower($path_parts["extension"]),$validateExt)) return false;
//                    }
//
//                    $patterns = array("/[^(\w,\s)]+/","/^(\s)/","/(\s)+/");
//                    $replacements = array("","","_");
//                    $path_parts["filename"] = preg_replace($patterns, $replacements, $path_parts["filename"]);
//                    if(empty($path_parts["filename"])) $path_parts["filename"] = rand(0,1000);
//
//                    $prex = date("ymd").'_';
//                    //$filename = $prex.$path_parts["filename"].'.'.strtolower($path_parts["extension"]);
//                    $filename = time().rand(0,10000).'.'.strtolower($path_parts["extension"]);
//                    $uploadfile  = $fileFolder.$filename;
//                    for($i=1;$i<1000;$i++){
//                            if(file_exists($uploadfile)){
//                                    $filename = $prex.$path_parts["filename"].'_'.$i.'.'.strtolower($path_parts["extension"]);
//                                    $uploadfile  = $fileFolder.$filename;
//                            }else{
//                                    break;
//                            }
//                    }
//                    if(move_uploaded_file ($upload_file['tmp_name'], $uploadfile)==true){
//                            $filename = urlencode($filename);
//                            //文件路径加密
//                            if($key){
//                                $filename = $this->path_encryption($filename,$key);
//                            }
//                            else{
//                                $filename = $uploadfile;
//                            }
//                            //文件加密
//                            $this->file_encryption($filename);
//                    }else{
//                            return false;
//                    }
//            }else{
//                    return false;
//            }
//            return $filename;
//    }
    
    
    function download($url,$fileFolder,$key,$ext='jpg'){
            $tmp = explode("/", $url);
            $tmp = $tmp[count($tmp)-1];
            $tmp = explode(".", $tmp);
            if(count($tmp) > 1){
                $ext = $tmp[count($tmp)-1];
            }
            $filename = time().rand(0,10000).'.'.strtolower($ext);
            $uploadfile  = $fileFolder.$filename;
            for($i=1;$i<1000;$i++){
                    if(file_exists($uploadfile)){
                            $filename = time().rand(0,10000).'.'.strtolower($ext);
                            $uploadfile  = $fileFolder.$filename;
                    }else{
                            break;
                    }
            }
            
            $ch = curl_init($url); 
            $fp = fopen($uploadfile, 'w'); 
            curl_setopt($ch, CURLOPT_FILE, $fp); 
            curl_setopt($ch, CURLOPT_HEADER, 0); 
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
            curl_exec($ch); 
            curl_close($ch); 
            fclose($fp); 
            
            
            if(filesize($uploadfile) > 0){
                    $filename2 = $filename;
                    $filename = urlencode($filename);
                    //文件路径加密
                    if($key){
                        $filename = $this->path_encryption($filename,$key);
                    }
                    else{
                        $filename = $uploadfile;
                    }
                    //文件加密
                    $this->file_encryption($filename);
            }else{
                    return false;
            }
            
            return array($filename,$filename2);
    }
    
    
    function move($filename,$fileFolder,$key,$file_src,$fileFolder_src,$key_src,$append=''){
            $filename_src = $this->path_decryption($file_src,$key_src);
            if(!file_exists($fileFolder_src.$filename_src)) return false;
            $path_parts = pathinfo($filename_src);
            $ext = $path_parts["extension"];
            
            if(empty($filename)){
                $filename = time().rand(0,10000).'.'.strtolower($ext);
                $uploadfile  = $fileFolder.$filename;
                for($i=1;$i<1000;$i++){
                    if(file_exists($uploadfile)){
                            $filename = time().rand(0,10000).'.'.strtolower($ext);
                            $uploadfile  = $fileFolder.$filename;
                    }else{
                            break;
                    }
                }
            }else{
                $uploadfile  = $fileFolder.$filename;
            }
            //echo "$fileFolder_src.$filename_src";
            copy($fileFolder_src.$filename_src, $uploadfile);
            //log_message('debug',"xxxxxx $fileFolder_src.$filename_src, $uploadfile");
            //echo "xxxxxx $fileFolder_src$filename_src, $uploadfile\n";
            //if((filesize($uploadfile) > 0) && ($ext == 'txt')){
            if(filesize($uploadfile) > 0){
                //prepend $append
//                $append = trim($append);
//                if(strlen($append) > 0){
//                    read($uploadfile,$content);
//                    $content = $append.$content;
//                    write($uploadfile, $content);
//                }
                //end prepend
                
                $filename2 = $filename;
                $filename = urlencode($filename);
                //文件路径加密
                if($key){
                    $filename = $this->path_encryption($filename,$key);
                }
                else{
                    $filename = $uploadfile;
                }
                //文件加密
                $this->file_encryption($filename);
            }else{
                return false;
            }
            return array($filename,$filename2);
    }
    
    
    function copy_post_file(&$post,$user_f,$user_t){
        $fileFolder_src = FILE_FOLDER. $user_f['username'][0] . '/' . $user_f['username'] . '/';
        $key_src = $user_f['key'];
        $fileFolder = FILE_FOLDER. $user_t['username'][0] . '/' . $user_t['username'] . '/';
        $key = $user_t['key'];
        
        
        if(!file_exists(FILE_FOLDER. $user_t['username'][0])){
            mkdir(FILE_FOLDER. $user_t['username'][0], 0777, true);
        }
        if(!file_exists($fileFolder)){
            mkdir($fileFolder, 0777, true);
        }
        if(!file_exists($fileFolder.'thumb/')){
            mkdir($fileFolder.'thumb/', 0777, true);
        }
        
//        if($post['file-text']):
//            $r = $this->file_model->move('',$fileFolder,$key,$post['file-text'],$fileFolder_src,$key_src);
//            if($r):
//                $post['file-text'] = $r[0];
//            else:
//                $post['file-text'] ='';
//            endif;
//        endif;
        $post['user_id'] = $user_t['id'];
        return;
        
    }
    
    function get_file_format($filename,$fileFolder,$key){
        if(empty($filename)) return false;
        $filename = $this->path_decryption($filename,$key);
        $path_parts = pathinfo($filename);
        $ext = $path_parts["extension"];
        $ext = strtolower($ext);
        switch( $ext ){
                case "pdf": $ctype="application/pdf"; break;
                case "doc": $ctype="application/msword"; break;
                case "docx": $ctype="application/msword"; break;
                case "gif": $ctype="image/gif"; break;
                case "jpe": $ctype="image/jpeg"; break;
                case "jpeg": $ctype="image/jpeg"; break;
                case "jpg": $ctype="image/jpeg"; break;

                case "mp3": $ctype="audio/mp3"; break;
                case "m4a": $ctype="audio/mp3"; break;
                case "mp4": $ctype="video/mp4"; break;
                case "m4v": $ctype="video/mp4"; break;
                case "mov": $ctype="video/mp4"; break;
                //case "mov": $ctype="video/quicktime"; break;
                case "flv": $ctype="video/x-flv"; break;
                default:
                        $ctype="application/unknown";
        }
        return $ctype;
    }
    
    function rangeDownload($file) {
            $fp = @fopen($file, 'rb');

            $size   = filesize($file); // File size
            $length = $size;           // Content length
            $start  = 0;               // Start byte
            $end    = $size - 1;       // End byte
            // Now that we've gotten so far without errors we send the accept range header
            /* At the moment we only support single ranges.
             * Multiple ranges requires some more work to ensure it works correctly
             * and comply with the spesifications: http://www.w3.org/Protocols/rfc2616/rfc2616-sec19.html#sec19.2
             *
             * Multirange support annouces itself with:
             * header('Accept-Ranges: bytes');
             *
             * Multirange content must be sent with multipart/byteranges mediatype,
             * (mediatype = mimetype)
             * as well as a boundry header to indicate the various chunks of data.
             */
            header("Accept-Ranges: 0-$length");
            // header('Accept-Ranges: bytes');
            // multipart/byteranges
            // http://www.w3.org/Protocols/rfc2616/rfc2616-sec19.html#sec19.2
            if (isset($_SERVER['HTTP_RANGE'])) {

                    $c_start = $start;
                    $c_end   = $end;
                    // Extract the range string
                    list(, $range) = explode('=', $_SERVER['HTTP_RANGE'], 2);
                    // Make sure the client hasn't sent us a multibyte range
                    if (strpos($range, ',') !== false) {

                            // (?) Shoud this be issued here, or should the first
                            // range be used? Or should the header be ignored and
                            // we output the whole content?
                            header('HTTP/1.1 416 Requested Range Not Satisfiable');
                            header("Content-Range: bytes $start-$end/$size");
                            // (?) Echo some info to the client?
                            exit;
                    }
                    // If the range starts with an '-' we start from the beginning
                    // If not, we forward the file pointer
                    // And make sure to get the end byte if spesified
                    if ($range0 == '-') {

                            // The n-number of the last bytes is requested
                            $c_start = $size - substr($range, 1);
                    }
                    else {

                            $range  = explode('-', $range);
                            $c_start = $range[0];
                            $c_end   = (isset($range[1]) && is_numeric($range[1])) ? $range[1] : $size;
                    }
                    /* Check the range and make sure it's treated according to the specs.
                     * http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html
                     */
                    // End bytes can not be larger than $end.
                    $c_end = ($c_end > $end) ? $end : $c_end;
                    // Validate the requested range and return an error if it's not correct.
                    if ($c_start > $c_end || $c_start > $size - 1 || $c_end >= $size) {

                            header('HTTP/1.1 416 Requested Range Not Satisfiable');
                            header("Content-Range: bytes $start-$end/$size");
                            // (?) Echo some info to the client?
                            exit;
                    }
                    $start  = $c_start;
                    $end    = $c_end;
                    $length = $end - $start + 1; // Calculate new content length
                    fseek($fp, $start);
                    header('HTTP/1.1 206 Partial Content');
            }
            // Notify the client the byte range we'll be outputting
            header("Content-Range: bytes $start-$end/$size");
            header("Content-Length: $length");

            // Start buffered download
            $buffer = 1024 * 8;
            while(!feof($fp) && ($p = ftell($fp)) <= $end) {

                    if ($p + $buffer > $end) {

                            // In case we're only outputtin a chunk, make sure we don't
                            // read past the length
                            $buffer = $end - $p + 1;
                    }
                    set_time_limit(0); // Reset time limit for big files
                    echo fread($fp, $buffer);
                    flush(); // Free up memory. Otherwise large files will trigger PHP's memory limit.
            }

            fclose($fp);

    }
    
    function read($filename,$fileFolder,$key,&$content,$echo = true){
            //$filename = urldecode($filename) ;
            if(empty($filename)) return false;
            $filename = $this->path_decryption($filename,$key);
            if(!file_exists( $fileFolder.$filename )){
                return false;
            }else{
            }
            
            $path_parts = pathinfo($filename);
            $ext = $path_parts["extension"];
            switch( $ext ){
                    case "pdf": $ctype="application/pdf"; break;
                    case "doc": $ctype="application/msword"; break;
                    case "docx": $ctype="application/msword"; break;
                    case "gif": $ctype="image/gif"; break;
                    case "jpe": $ctype="image/jpeg"; break;
                    case "jpeg": $ctype="image/jpeg"; break;
                    case "jpg": $ctype="image/jpeg"; break;

                    case "mp3": $ctype="audio/mp3"; break;
                    case "mp4": $ctype="video/mp4"; break;
                    case "m4v": $ctype="video/mp4"; break;
                    case "mov": $ctype="video/quicktime"; break;
                    case "flv": $ctype="video/x-flv"; break;
                    default:
                            $ctype="application/unknown";
            }
            
            $handle = fopen($fileFolder.$filename, "r");
            $content = '';
            //$content = fread($handle, filesize($fileFolder.$filename));
            if($echo){
                header("Content-Type: $ctype");
                $this->rangeDownload($fileFolder.$filename);
            }else{
                //$this->file_decryption($filename,$key,&$content);
                while (!feof($handle)) {
                  $content .= fread($handle, 8192);
                }
            }
            
            
            fclose($handle);
            return true;
    }

    function write($filename,$fileFolder,$content,$key){
            if (file_exists($fileFolder.$filename) && !is_writable($fileFolder.$filename)) return false;
            if (!$handle = fopen($fileFolder.$filename, 'w')) return false;
            if (fwrite($handle, $content) === FALSE) return false;
            fclose($handle);
            
            
            //文件路径加密
            $filename = $this->path_encryption($filename,$key);
            //文件加密
            $this->file_encryption($filename,$key);

            return $filename;
    }
    
    function path_encryption($path, $key='')
    {
          return  $this->encryption($path, $key);
    }
    
    function path_decryption($path, $key='')
    {
          return  $this->decryption($path, $key);
    }
    
    /*对单个文件加密
     * $file 文件路径
     * 
     * 返回
     * true/false
     */
    function file_encryption($file,$key=''){
        return true;
    }
    
    
    /*对单个文件解密
     * $file 文件路径
     * $content 文件内容
     * 返回
     * true/false
     */
    function file_decryption($file,$key='',&$content){
        return true;
    }
            
    
    
    function encryption($data, $key)
    {
            $key	=	md5($key);
            $x		=	0;
            $len	=	strlen($data);
            $l		=	strlen($key);
            for ($i = 0; $i < $len; $i++)
            {
                if ($x == $l) 
                {
                        $x = 0;
                }
                $char .= $key{$x};
                $x++;
            }
            for ($i = 0; $i < $len; $i++)
            {
                $str .= chr(ord($data{$i}) + (ord($char{$i})) % 256);
            }
            return base64_encode($str);
    }
    
    function decryption($data, $key)
    {
            $key = md5($key);
        $x = 0;
        $data = base64_decode($data);
        $len = strlen($data);
        $l = strlen($key);
        for ($i = 0; $i < $len; $i++)
        {
            if ($x == $l) 
            {
                    $x = 0;
            }
            $char .= substr($key, $x, 1);
            $x++;
        }
        for ($i = 0; $i < $len; $i++)
        {
            if (ord(substr($data, $i, 1)) < ord(substr($char, $i, 1)))
            {
                $str .= chr((ord(substr($data, $i, 1)) + 256) - ord(substr($char, $i, 1)));
            }
            else
            {
                $str .= chr(ord(substr($data, $i, 1)) - ord(substr($char, $i, 1)));
            }
        }
        return $str;
    }
    
    
    
    function remove($id,$user_id){
      $this->db->delete('page', array('id' => $id,'user_id' => $user_id)); 
    }
    
    function valid_resource($id,$user_id){
        if(!$id || !$user_id):
            return false;
        endif;
        
        if(is_array($id)):
            $id = implode(",", $id);
        endif;
        
        $ext2 = ' AND (CC.user_id = '.$user_id.' OR A.user_id = '.$user_id.')';
        $limit = " LIMIT 0,100";
        
        $sql = 'SELECT DISTINCT A.id,A.file FROM `'.$this->db->dbprefix(TBL_RESC).'` AS A 
            RIGHT JOIN `'.$this->db->dbprefix(TBL_REST).'` AS B 
                ON A.id=B.resource_id LEFT JOIN 
                (
                SELECT * FROM 
                    (SELECT user_id,cid,is_owner FROM `'.$this->db->dbprefix(TBL_UFOR).'` WHERE user_id = '.$user_id.') AS C 
                        LEFT JOIN 
                        `'.$this->db->dbprefix(TBL_TREE).'` AS D ON C.cid = D.id
                ) AS CC
                ON B.tree_id=CC.cid
                WHERE 1=1 '
                .$ext2
                .' AND A.id IN ('.$id.') '.$limit;
        
        $r = $this->db->query($sql)->result_array();
        if ( is_array($r) && count($r) > 0) {
            return $r;
        }
        return false;
    }
    
    function remove_resource($id,$user_id){
        if(!$id || !$user_id) return false;
        $sql = 'SELECT DISTINCT A.id,B.id as rid FROM `'.$this->db->dbprefix(TBL_RESC).'` AS A '
                . 'RIGHT JOIN `'.$this->db->dbprefix(TBL_REST).'` AS B ON A.id=B.resource_id '
                . 'LEFT JOIN ('
                . 'SELECT cid,is_owner FROM `'.$this->db->dbprefix(TBL_UFOR).'` WHERE user_id = '.$user_id.''
                . ') AS C ON B.tree_id=C.cid '
                . 'WHERE ((A.user_id = '.$user_id.') OR (C.is_owner=1) ) AND A.id = '.$id;
        $r = $this->db->query($sql)->result_array();
        
        //echo $this->db->last_query();die();
        if ( is_array($r) && count($r) > 0) {
            $rid = $r[0]['rid'];
            return $this->db->delete($this->db->dbprefix(TBL_REST), array('id' => $rid)); 
        }
        return false;
    }
    
    function add_resource(){
        
    }
    
    function get_resource(&$total,$tree_id,$user_id='',$only_self=0,$num_posts = 10 ,$offset = 0){
        //$only_self = 1 ;
        if(empty($user_id)) return false;
        $ext = '';
        if($tree_id):
            $ext = ' AND CC.cid = '.$tree_id.' ';
        endif;
        
        if(!$only_self):
            $ext2 = ' AND (CC.user_id = '.$user_id.' OR A.user_id = '.$user_id.')';
        else:
            $ext2 = ' AND (A.user_id = '.$user_id.')';
        endif;
        
        
        $limit = " LIMIT $offset,$num_posts";
        
        if(!$only_self || $tree_id):
            $sql = 'FROM `'.$this->db->dbprefix(TBL_RESC).'` AS A 
                RIGHT JOIN `'.$this->db->dbprefix(TBL_REST).'` AS B 
                    ON A.id=B.resource_id LEFT JOIN 
                    (
                    SELECT * FROM 
                        (SELECT user_id,cid,is_owner FROM `'.$this->db->dbprefix(TBL_UFOR).'` WHERE user_id = '.$user_id.') AS C 
                            LEFT JOIN 
                            `'.$this->db->dbprefix(TBL_TREE).'` AS D ON C.cid = D.id
                    ) AS CC
                    ON B.tree_id=CC.cid
                    LEFT JOIN `'.$this->db->dbprefix(TBL_USER).'` AS E ON E.id = A.user_id
                    WHERE 1=1 AND A.status = 1 '
                    .$ext2
                    .$ext
                    .' ORDER by A.`date` DESC,A.place,A.id DESC ';
        else:
            $sql =  'FROM `'.$this->db->dbprefix(TBL_RESC).'` AS A '
                . 'RIGHT JOIN `'.$this->db->dbprefix(TBL_REST).'` AS B '
                . 'ON A.id=B.resource_id '
                . 'WHERE  A.status = 1  AND '
                . 'A.user_id = '.$user_id.' '
                . 'ORDER by A.`date` DESC,A.place,A.id DESC ';
        endif;
        
        
        
//        $sql = 'SELECT DISTINCT A.user_id,A.relative,A.is_owner,A.is_main,A.label,A.create_date,D.id,D.real_name,D.gender,D.display_name,D.username,D.avatar,D.email,D.contact,D.key FROM '.
//                $this->db->dbprefix(TBL_UFOR).' AS A'.
//                ' LEFT JOIN '.$this->db->dbprefix(TBL_USER).' AS D ON A.cid = D.id WHERE'.
//                ' A.user_id = '.$user_id.
//                ' ORDER BY A.is_main DESC,A.id ASC';
        
//        $sql =  'FROM `'.$this->db->dbprefix(TBL_RESC).'` AS A '
//                . 'LEFT JOIN `'.$this->db->dbprefix(TBL_REST).'` AS B '
//                . 'ON A.id=B.resource_id '
//                . 'LEFT JOIN el_user_forest AS C '
//                . 'ON B.tree_id=C.cid '
//                . 'LEFT JOIN el_tree AS D '
//                . 'ON C.cid = D.id '
//                . 'WHERE '
//                . 'C.user_id = '.$user_id.' OR A.user_id = '.$user_id.' '
//                . $ext
//                . 'ORDER by A.`date` DESC,A.place '.$limit;
        
        $sql_c = 'SELECT COUNT(DISTINCT A.id) AS num '
                . $sql;
        //echo $sql_c;die();
        $r = $this->db->query($sql_c)->result_array();
        
        
        $total = $r[0]['num'];
        
        if(!$only_self || $tree_id):
            $sql = 'SELECT DISTINCT E.avatar,E.username,E.display_name,CC.is_owner,A.id,A.user_id,A.file,A.place,A.date,A.status '
                . $sql.$limit;
        else:
            $sql = 'SELECT DISTINCT A.id,A.user_id,A.file,A.place,A.date,A.status '
                . $sql.$limit;
        endif;
        $r = $this->db->query($sql)->result_array();
        
        //echo $this->db->last_query();die();
        
        if ( is_array($r) && count($r) > 0) {
            return $r;
        }
        return false;
        
    }
    
    function get_resource_reqiure_sync($tree_id,$user_id='',$only_self=0){
        if(empty($user_id)) return false;
        $ext = '';
        if($tree_id):
            $ext = ' AND CC.cid = '.$tree_id.' ';
        endif;
        
        if(!$only_self):
            $ext2 = ' AND (CC.user_id = '.$user_id.' OR A.user_id = '.$user_id.')';
        else:
            $ext2 = ' AND (A.user_id = '.$user_id.')';
        endif;
        
        if(!$only_self || $tree_id):
            $sql = 'FROM `'.$this->db->dbprefix(TBL_RESC).'` AS A 
                RIGHT JOIN `'.$this->db->dbprefix(TBL_REST).'` AS B 
                    ON A.id=B.resource_id LEFT JOIN 
                    (
                    SELECT * FROM 
                        (SELECT user_id,cid,is_owner FROM `'.$this->db->dbprefix(TBL_UFOR).'` WHERE user_id = '.$user_id.') AS C 
                            LEFT JOIN 
                            `'.$this->db->dbprefix(TBL_TREE).'` AS D ON C.cid = D.id
                    ) AS CC
                    ON B.tree_id=CC.cid
                    LEFT JOIN `'.$this->db->dbprefix(TBL_USER).'` AS E ON E.id = A.user_id
                    WHERE 1=1 AND A.status != 1 '
                    .$ext2
                    .$ext;
        else:
            $sql =  'FROM `'.$this->db->dbprefix(TBL_RESC).'` AS A '
                . 'RIGHT JOIN `'.$this->db->dbprefix(TBL_REST).'` AS B '
                . 'ON A.id=B.resource_id '
                . 'WHERE  A.status != 1  AND '
                . 'A.user_id = '.$user_id.' ';
        endif;
        
        
        $sql_c = 'SELECT COUNT(DISTINCT A.id) AS num '
                . $sql;
        $r = $this->db->query($sql_c)->result_array();
        
        
        $total = $r[0]['num'];
        return $total;
        
    }
    
    
    function create_resource($resData) {
        if(!$resData['user_id']) return false;
        if(!$resData['file']) return false;
        $_default_data = array(
            'type'=>'image',
            'date'=>'',
            'place'=>'',
            'gps'=>'',
            'exif'=>'',
            'status'=>0
        );
        $resData =  $resData + $_default_data ;
        
        $data['user_id'] = $resData['user_id'];
        $data['file'] = $resData['file'];
        $data['type'] = $resData['type'];
        $data['create_date'] = date('Y-m-d H:i:s');
        $data['date'] = $resData['date'];
        $data['place'] = $resData['place'];
        $data['gps'] = $resData['gps'];
        $data['exif'] = $resData['exif'];
        $data['status'] = $resData['status'];
        
        if ( $this->db->insert(TBL_RESC,$data) ) {
          return $this->db->insert_id();
        } else {
          return false;
        }
    }
    function resource_tree($res_ids,$tree_ids){
        if(!is_array($res_ids) || !$res_ids){
            return false;
        }
        if(!is_array($tree_ids) || !$tree_ids){
            return false;
        }
        $data = array();
        foreach($res_ids as $res_id):
            foreach ($tree_ids as $tree_id):
            $data[] = array(
                'resource_id'=>$res_id,
                'tree_id'=>$tree_id
            );
            endforeach;
        endforeach;
        $this->db->insert_batch(TBL_REST, $data); 
    }
    
    
    function get_media_cron($limit = 10){
        $this->db->select('id,user_id,file');
        $this->db->where('status', 0);
        $this->db->order_by('id', 'asc');
        $this->db->from(TBL_RESC);
        $this->db->limit($limit);
        $posts = $this->db->get()->result_array();

        if( is_array($posts) && count($posts) > 0 ) {
          return $posts;
        }
        return false;
    }
    
    function get_media($res_id){
        if(!$res_id):
            return false;
        endif;
        $this->db->select('id,user_id,file');
        $this->db->where_in('id', $res_id);
        $this->db->where('status', 0);
        $this->db->from(TBL_RESC);
        
        $posts = $this->db->get()->result_array();

        if( is_array($posts) && count($posts) > 0 ) {
          return $posts;
        }
        return false;
    }
    
    function set_resource_status($res_id,$status=1){
        if(!$res_id):
            return false;
        endif;
        $data['status'] = $status;
        $this->db->where_in('id', $res_id);
        return $this->db->update(TBL_RESC, $data); 
    }
    
    function update_resource($id,$resData){
        if(!$id):
            return false;
        endif;
        
        $data['place'] = $resData['place'];
        $data['gps'] = $resData['gps'];
        $data['exif'] = $resData['exif'];
        $data['file'] = $resData['file'];
        $data['date'] = $resData['date'];
        $data['status'] = $resData['status'];
        $this->db->where('id', $id);
        return $this->db->update(TBL_RESC, $data); 
    }
    
    
    function get_covers($ids){
        if(!$ids) return false;
        $ids = implode(",", $ids);
        $sql = 'SELECT file,tree_id,COUNT(B.id) as num FROM `'.$this->db->dbprefix(TBL_REST).'` as A '
                . 'LEFT JOIN `'.$this->db->dbprefix(TBL_RESC).'` as B ON A.resource_id=B.id '
                . 'WHERE tree_id in ('.$ids.') GROUP BY tree_id';
        $r = $this->db->query($sql)->result_array();
        
        if ( is_array($r) && count($r) > 0) {
            return $r;
        }
        return false;
        
        
    }
    
    function get_image_num_by_id($tree_id){
      if(!$tree_id):
          return false;
      endif;
      $this->db->from(TBL_REST);
      $this->db->where('tree_id', $tree_id);
      $r = $this->db->count_all_results();
      return $r;

    }
    
    
    function get_all_status(){
        $sql = 'SELECT t,COUNT(create_date) as num, create_date '
                . 'FROM (SELECT timestampdiff (second ,create_date,update_date) as t, create_date FROM `'.$this->db->dbprefix(TBL_RESC).'` '
                . 'WHERE id > 237 order by id desc) as A GROUP BY create_date DESC';
        //echo $sql;
        //$sql = 'SELECT (update_date - create_date) as t,COUNT(create_date) as num,create_date FROM `'.$this->db->dbprefix(TBL_REST).'` WHERE id > 237 GROUP BY create_date ORDBER BY id DESC';
        $r = $this->db->query($sql)->result_array();
        
        if ( is_array($r) && count($r) > 0) {
            return $r;
        }
    }
    
    
    function  get_resource_number( $postData) {
        $this->db->from(TBL_PSOC);
        $option = array();
        $c = '';
        if(isset($postData['from'])) $c .= ' AND create_date >="'.$postData['from'].'"';
        if(isset($postData['to'])) $c .= ' AND create_date <"'.$postData['to'].'"';
        
        $sql = 'SELECT  COUNT( num ) AS cou_num,SUM( num ) AS sum_num,AVG(t/num) as avg_p,AVG(t) as avg_t,AVG(num) as avg_n '
                . 'FROM (SELECT t,COUNT(create_date) as num '
                . 'FROM (SELECT timestampdiff (second ,create_date,update_date) as t, create_date FROM `'.$this->db->dbprefix(TBL_RESC).'` '
                . 'WHERE 1=1 '.$c.' order by id desc) as A GROUP BY create_date) AS B';
        
        $r = $this->db->query($sql)->result_array();
        
        if ( is_array($r) && count($r) > 0) {
            return $r[0];
        }
    }

  
}
