<?php

class Order_Model extends CI_Model {
  function add_order( $orderData ) {
      $data['user_id'] = $orderData['user_id'];
      $data['is_point'] = (int)$orderData['is_point'];
      $data['sku'] = $orderData['sku'];
      $data['order_time'] = date('Y-m-d H:i:s',time());
      $data['pay_time'] = 0;
      $data['price'] = 1*$orderData['price'];
      $data['email'] = $orderData['email'];
      $data['message'] = $orderData['message'];
      $data['num'] = ($orderData['num'])?$orderData['num']:1;
      $data['total'] = ($orderData['total'])?$orderData['total']:$data['price'];
      $data['address'] = $orderData['address'];
      $data['status'] = ORDER_NOTPAY;
      $data['book_id'] = $orderData['book_id'];
      $data['book_title'] = $orderData['book_title'];

        if($this->db->insert(TBL_ORDE,$data)){
            return $this->db->insert_id();
        }else{
            return false;
        }
  }
  function pay_order( $orderData ) {
      $data['pay_time'] = date('Y-m-d H:i:s',time());
      $data['ecode'] = $orderData['ecode'];
      $data['status'] = ORDER_PAYED;
      if($orderData['id']){
          $this->db->where('id', $orderData['id']);
          return $this->db->update(TBL_ORDE, $data); 
      }
  }
  function use_order( $id) {
      $data['pay_time'] = date('Y-m-d H:i:s',time());
      $data['status'] = ORDER_USED;
      if($id){
          $this->db->where('id', $id);
          return $this->db->update(TBL_ORDE, $data); 
      }
  }
  
  function get_orders(&$total,$condition,$start=0,$num=0) {
      
    $this->db->start_cache();
    $this->db->from(TBL_ORDE);
    if(isset($condition['status'])){
        $this->db->where('status',$condition['status']);
    }
    if(isset($condition['sku'])){
        $this->db->like('sku',$condition['sku']);
    }
    if(isset($condition['user_id'])){
        $this->db->where('user_id',$condition['user_id']);
    }
    $this->db->stop_cache();
    $total = $this->db->count_all_results();
    
    if($num){
        $this->db->limit( $num,$start );
    }
    $this->db->order_by('id','desc');
    $posts = $this->db->get()->result_array();
    $this->db->flush_cache();

    if( is_array($posts) && count($posts) > 0 ) {
      return $posts;
    }

    return false;
  }
  
  function get_order_by_id($id){
    $this->db->from(TBL_ORDE);
    $this->db->where(array('id' =>$id));
    $orders = $this->db->get()->result_array();
    if( is_array($orders) && count($orders) > 0 ) {
      return $orders[0];
    }
    return false;
  }
  
  function get_ecode($ecode,$sku=''){
    $this->db->from(TBL_ECOD);
    $cond = array('ecode' =>$ecode);
    if($sku){
        $cond['sku'] = $sku;
    }
    $this->db->where($cond);
    $r = $this->db->get()->result_array();
    if( is_array($r) && count($r) > 0 ) {
      return $r[0];
    }
    return false;
  }
  
  function add_ecode($EcodeData) {
//      $data['ecode'] = $EcodeData['ecode'];
//      $data['sku'] = $EcodeData['sku'];
//      return $this->db->insert_batch(TBL_ECOD, $data); 
      $data = array(
               'ecode' => $EcodeData['ecode'],
               'sku' => $EcodeData['sku']
            );
    return $this->db->insert(TBL_ECOD,$data);
  }
  
  
  function use_ecode($id){
    $this->db->delete(TBL_ECOD, array('id' => $id)); 
  }
  
  
  function used($id){
    $data = array('status' => ORDER_USED);
    $this->db->where('id', $id);
    $this->db->update(TBL_ORDE, $data); 
  }
}
