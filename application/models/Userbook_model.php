<?php
class UserBook_Model extends CI_Model {

  function create_new($bookData) {
    $data['title'] = $bookData['title'];
    $data['user_id'] = (int)$bookData['user_id'];
    $data['subtitle'] = $bookData['subtitle'];
    $data['file'] = $bookData['file'];
    $data['status'] = USER_BOOK_NORM;
    $data['createtime'] = date('Y-m-d H:i:s');
    $data['token'] = md5(time().rand(10000,99999));
    $data['page'] = (int)$bookData['page'];
    $data['from'] = (int)$bookData['from'];
    $data['to'] = (int)$bookData['to'];
    $data['is_pdf'] = (int)$bookData['is_pdf'];
    $data['status'] = BOOK_STATUS_CREA;
    
    if ( $this->db->insert(TBL_UBOO,$data) ) {
      return $this->db->insert_id();
    } else {
      return false;
    }
  }

  function modify($bookData) {
    if(isset($bookData['title'])) $data['title'] = $bookData['title'];
    if(isset($bookData['subtitle'])) $data['subtitle'] = $bookData['subtitle'];
    if(isset($bookData['status'])) $data['status'] = $bookData['status'];
    if(isset($bookData['page'])) $data['page'] = $bookData['page'];
    
    $this->db->where('id', $bookData['id']);
    $this->db->where('user_id', $bookData['user_id']);
    
    return $this->db->update(TBL_UBOO, $data); 
  }
  
  function recommend($id) {
    $data=array(
        'status'=>BOOK_STATUS_RECO
    );
    $this->db->where('id', $id);
    return $this->db->update(TBL_UBOO, $data); 
  }  
  
  function unrecommend($id) {
    $data=array(
        'status'=>BOOK_STATUS_CONT
    );
    $this->db->where('id', $id);
    return $this->db->update(TBL_UBOO, $data); 
  }
  function unshare($id) {
    $data=array(
        'status'=>BOOK_STATUS_NORM
    );
    $this->db->where('id', $id);
    return $this->db->update(TBL_UBOO, $data); 
  }
  
    function remove($id,$user_id){
        $this->db->delete(TBL_UBOO, array('id' => $id,'user_id' => $user_id)); 
    }
    
    
    function get_book_by_id( $id,$user_id='') {
        $id=(int)$id;
        $user_id=(int)$user_id;
        if($user_id){
            $where['user_id']=$user_id;
        }
        $where['id'] = $id;
        $query = $this->db->get_where(TBL_UBOO, $where, 1, 0);

        if ($query->num_rows() > 0) {
            $post = $query->row_array();
            
            $where=array(
                'book_id' => $id,
                'option' => BOOK_PURCHASE_FREE,
            );
            $query = $this->db->get_where(TBL_UBOH, $where, 1, 0);
            if ($query->num_rows() > 0) {
                $post['purchased_free'] = true;
            }else{
                $post['purchased_free'] = false;
            }
            
            return $post;
        }
        return false;
    }
    
    function free_purchase( $book_id) {
      $data['book_id'] = $book_id;
      $data['option'] = BOOK_PURCHASE_FREE;
      if ( $this->db->insert(TBL_UBOH,$data) ) {
        return true;
      } else {
        return false;
      }
        
    }
    
    
    function get_book_account($bookCondition) {
        $this->db->where($bookCondition);
        $this->db->from(TBL_UBOO);
        return $this->db->count_all_results();
    }
    
    function update_remote_status_by_id($status,$page,$book_id,$proposal_time=''){
        $data['status'] = $status;
        $data['page'] = (int)$page;
        
        if($proposal_time){
            $data['proposal_time'] = $proposal_time;
        }
        
        $this->db->where('book_id', $book_id);
        return $this->db->update(TBL_UBOR, $data); 
    }
    
    function get_dealing_list(){
        $statuss = array(BOOK_STATUS_DEAL,BOOK_STATUS_REND);
        $this->db->where_in('status', $statuss);
        
        $this->db->from(TBL_UBOR);
        return $this->db->count_all_results();
    }
    
    function get_to_deal_list($num=0,$start=0){
        if($num){
            $this->db->limit($num,$start);
        }
        $query = $this->db->get_where(TBL_UBOR, array('status'=>BOOK_STATUS_COMP));
        if ($query->num_rows() > 0) {
            $post = $query->result_array();
            return $post;
        }
        return false;
    }
    
    function get_book_list($bookCondition,$order='order',$num=0,$start=0) {
        switch($order){
            case 'order':
                $this->db->order_by("order", "desc");
            case 'latest':
                $this->db->order_by("id", "desc");
                break;
            default:
                $this->db->order_by("fav", "asc");
                break;
        }
        if($num){
            $this->db->limit($num,$start);
        }
        
        $query = $this->db->get_where(TBL_UBOO, $bookCondition);
          if ($query->num_rows() > 0) {
              $post = $query->result_array();
              return $post;
          }
          return false;
    }
    
    function update_is_pdf($book_id,$min) {
        $this->db->set('is_pdf', 1);
        $this->db->where('id', $book_id);
        $this->db->where('fav >= ', $min);
        $this->db->where('is_pdf', 0);
        $this->db->update(TBL_UBOO); 
        return true;
    }
    
    function add_fav($bookData) {
      $data['book_id'] = $bookData['book_id'];
      $data['cookie'] = $bookData['cookie'];
      $data['ip'] = $bookData['ip'];
      if ( $this->db->insert(TBL_UBOF,$data) ) {
        $this->db->set('fav', 'fav+1', FALSE);
        $this->db->where('id', $bookData['book_id']);
        $this->db->update(TBL_UBOO); 
        return true;
      } else {
        return false;
      }
    }
    
    function get_fav($bookData) {
        $where['cookie']=$bookData['cookie'];
        $where['book_id']=$bookData['book_id'];
        $query = $this->db->get_where(TBL_UBOF, $where, 1, 0);
        if ($query->num_rows() > 0) {
            return false;
        }
        
        $where['ip'] = $bookData['ip'];
        $where['book_id']=$bookData['book_id'];
        $where['createtime >=']=date('Y-m-d H:i:s',time() - (24 * 60 * 60));
        $query = $this->db->get_where(TBL_UBOF, $where, 5, 0);
        if ($query->num_rows() >= 5) {
            return false;
        }
        return true;
    }
    
    
    function update_position($position){
        $data = array();
        if(is_array($position)){
            foreach($position as $k=>$v){
                $data[] = array(
                    'id'=>$k,
                    'order'=>$v
                );
            }
        }
          $this->db->update_batch(TBL_UBOO, $data, 'id'); 
    }

}
