<?php

class Mobile_Model extends CI_Model {
//  function generate_regcode($num) {
//        $data = array();
//        for($i=0;$i<$num;$i++){
//            $data[] = array('code'=>rand(10000000,99999999));
//        }
//        $this->db->insert_batch(TBL_RCOD, $data); 
//  }
//  
//    function use_regcode($RegData){
//        $data['used_date'] = $RegData['used_date'];
//        $data['user_id'] = $RegData['user_id'];
//        $this->db->where('code', $RegData['code']);
//        return $this->db->update(TBL_RCOD, $data); 
//    }
//    
    function remove($id){
        $this->db->delete(TBL_MCOD, array('id' => $id)); 
    }
    
    function use_code($codeData){
        $data['update_time'] = GetCurrentTime();
        $data['status'] = 1;
        $this->db->where('id', $codeData['id']);
        return $this->db->update(TBL_MCOD, $data); 
    }
    
    function get_code_by_mobile($mobile,$time=1800){
      $this->db->from(TBL_MCOD);
      $this->db->select('id,code,create_time');
      $this->db->where(array('mobile' =>$mobile));
      $this->db->where('create_time >',  date('Y-m-d H:i:s',time()-$time));
      $this->db->order_by('create_time','desc');
      $r = $this->db->get()->result_array();
      if( is_array($r) && count($r) > 0 ) {
        return $r[0];
      }
      return false;
    }
    
    function get_num_by_ip($ip,$time=1800){
      $this->db->from(TBL_MCOD);
      $this->db->select('id,code,create_time');
      $this->db->where(array('ip' =>$ip));
      $this->db->where('create_time >',  date('Y-m-d H:i:s',time()-$time));
        return $this->db->count_all_results(TBL_POIN);
    }
    
    function create_code_by_mobile($postData){
        $data['mobile'] = $postData['mobile'];
        $data['code'] = $postData['code'];
        $data['ip'] = $postData['ip'];
        $data['status'] = 0;
        if ( $this->db->insert(TBL_MCOD,$data) ) {
          return $data;
        } else {
          return false;
        }
    }
  
}
