<?php

class Family_Model extends CI_Model {

    function create_new($nodeData) {
        $_data = array(
            'tree_id'=>'',
            'user_id'=>'',
            'avatar'=>'',
            'name'=>'',
            'gender'=>'',
            'first_name'=>'',
            'last_name'=>''
        );
        $data = array_merge($_data,$nodeData);
        if(!$data['tree_id'] || !$data['name']):
            return false;
        endif;
        if ( $this->db->insert(TBL_FAMI,$data) ) {
          return $this->db->insert_id();
        } else {
          return false;
        }
    }
  
    function modify($nodeData) {
        if(!$nodeData['id'] || !$nodeData['name']):
            return false;
        endif;
        $data['name'] = $nodeData['name'];
        $data['avatar'] = $nodeData['avatar'];
        $this->db->where('id', $nodeData['id']);
        return $this->db->update(TBL_FAMI, $data); 
    }
  
    function remove($id){
        if(!$id):
            return false;
        endif;
        $this->db->delete($this->db->dbprefix(TBL_FAMI), array('id' => $id)); 
        return;
    }
  
    function add_relative($relativeData){
        if(!$relativeData['id1'] || !$relativeData['id2'] || !$relativeData['relative1'] || !$relativeData['relative2']):
            return false;
        endif;
        if($relativeData['id1']  ==  $relativeData['id2']):
            return false;
        endif;
        $data['id1'] = $relativeData['id1'];
        $data['id2'] = $relativeData['id2'];
        $data['relative1'] = $relativeData['relative1'];
        $data['relative2'] = $relativeData['relative2'];
        if ( $this->db->insert(TBL_FREL,$data) ) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }
    
    function add_relative_batch($nodes,$node_id){
        if(!$nodes || !$node_id):
            return false;
        endif;
        
        $data = array();
        foreach($nodes as $node):
            if(!$node['id2'] || !$node['relative1'] || !$node['relative2']):
                continue;
            endif;
            if($node['id2'] == $node_id):
                continue;
            endif;
            $data[] = array(
                'id1'=>$node_id,
                'id2'=>$node['id2'],
                'relative1'=>$node['relative1'],
                'relative2'=>$node['relative2']
            );
        endforeach;
        if(!$data):
            return false;
        endif;
        return $this->db->insert_batch(TBL_FREL, $data); 
    }

  
    function remove_relative($id){
        if(!$id):
            return false;
        endif;
        $this->db->delete($this->db->dbprefix(TBL_FREL), array('id1' => $id)); 
        $this->db->delete($this->db->dbprefix(TBL_FREL), array('id2' => $id)); 
        return;
    }
    
  
  function get_nodes_by_tree_id($tree_id) {
    $this->db->select('id');
    $this->db->from(TBL_FAMI);
    $this->db->where('tree_id', $tree_id);
    $r = $this->db->get()->result_array();
    if( is_array($r) && count($r) > 0 ) {
      return $r;
    }else{
        return false;
    }
  }
    function get_root($tree_id){
        if(empty($tree_id)) return false;
        $this->db->select('id,name');
        $this->db->from(TBL_FAMI);
        $this->db->where('tree_id', $tree_id);
        $this->db->order_by('id', 'asc');
        $this->db->limit(1);
        $r = $this->db->get()->row_array();
        if ( is_array($r) && count($r) > 0) {
            return $r;
        }
        return false;
        
    }
    function get_node_by_user_id($user_id,$tree_id){
        if(empty($user_id)) return false;
        if(empty($tree_id)) return false;
        $this->db->select('id');
        $this->db->from(TBL_FAMI);
        $this->db->where('user_id', $user_id);
        $this->db->where('tree_id', $tree_id);
        $r = $this->db->get()->row_array();
        if ( is_array($r) && count($r) > 0) {
            return $r;
        }
        return false;
        
    }
  
    function get_nodes($node_id){
        if(!$node_id) return false;
        $node_id = (int)$node_id;
        $sql = 'SELECT id1 AS id,relative1 as relative FROM `'.$this->db->dbprefix(TBL_FREL).'` where `id2` = '.$node_id.'
                UNION
                SELECT id2 AS id,relative2 as relative FROM `'.$this->db->dbprefix(TBL_FREL).'` where id1 = '.$node_id.'';
        $r = $this->db->query($sql)->result_array();
        
        if ( is_array($r) && count($r) > 0) {
            return $r;
        }
        return false;
    }
    function get_nodes_by_node_id($node_id){
        if(!$node_id) return false;
        $node_id = (int)$node_id;
        $sql = 'SELECT A.id,name,avatar,relative FROM `'.$this->db->dbprefix(TBL_FAMI).'` AS A RIGHT JOIN 
                    (SELECT id1 AS id,relative1 as relative FROM `'.$this->db->dbprefix(TBL_FREL).'` where `id2` = '.$node_id.'
                    UNION
                    SELECT id2 AS id,relative2 as relative FROM `'.$this->db->dbprefix(TBL_FREL).'` where id1 = '.$node_id.')
                    AS B ON A.id = B.id';
        $r = $this->db->query($sql)->result_array();
        
        if ( is_array($r) && count($r) > 0) {
            return $r;
        }
        return false;
    }
    
    
//    function get_nodes_by_user_id($user_id){
//        if(empty($user_id)) return false;
//        $sql = 'SELECT DISTINCT A.user_id,A.relative,A.is_owner,A.is_main,A.label,A.create_date,D.id,D.real_name,D.gender,D.display_name,D.username,D.avatar,D.email,D.contact,D.key FROM '.
//                $this->db->dbprefix(TBL_UFOR).' AS A'.
//                ' LEFT JOIN '.$this->db->dbprefix(TBL_USER).' AS D ON A.cid = D.id WHERE'.
//                ' A.user_id = '.$user_id.
//                ' ORDER BY A.is_main DESC,A.id ASC';
//        $r = $this->db->query($sql)->result_array();
//        
//        if ( is_array($r) && count($r) > 0) {
//            return $r;
//        }
//        return false;
//    }
    
    function chk_unique($node_id,$relative){
        if(!$node_id) return false;
        if(!$relative) return false;
        $this->db->from(TBL_FREL);
        $this->db->where("(`id1` = '$node_id' AND `relative2` = '$relative')",NULL );
        $this->db->or_where("(`id2` = '$node_id' AND `relative1` = '$relative')",NULL );
        return $this->db->count_all_results();
    }
  
    function chk_node($id,$tree_id){
        if(!$id) return false;
        if(!$tree_id) return false;
        $this->db->from(TBL_FAMI);
        $this->db->where("id",$id );
        $this->db->where("tree_id",$tree_id );
        $r = $this->db->get()->row_array();
        if ( is_array($r) && count($r) > 0) {
            return $r;
        }
        return false;
    }
  
    
}
