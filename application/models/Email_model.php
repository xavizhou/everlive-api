<?php
class Email_Model extends CI_Model {
    function sendmail($data,&$error) {
        if(EMAIL_DB):
            $mailData['to'] = $data['to'];
            $mailData['cc'] = $data['cc'];
            $mailData['subject'] = $data['subject'];
            $mailData['body'] = $data['message'];
            $mailData['status'] = ACTIVE_F;
            $mailData['createdate'] = date('Y-m-d H:i:s');

            if ( $this->db->insert(TBL_MAIL,$mailData) ) {
              return $this->db->insert_id();
            } else {
              return false;
            }
            return true;
        endif;
            
        //return true;
        $this->load->library('email');
        //$this->email->from($data['from'], $data['from_name']);
        $this->email->from(SYS_MAIL);
        $this->email->to($data['to']); 
        if($data['cc']){
            $this->email->cc($data['cc']); 
        }
        $this->email->subject($data['subject']);
        $this->email->message($data['message']);

        if($this->email->send()==true){
            return true;
        }
        else{
            //echo $this->email->print_debugger();
            return false;
        }

        //echo $this->email->print_debugger();
        return true;
    }
    
    
    function get_email_list($mailData,&$total){
        $this->db->from(TBL_MAIL);
        if(isset($mailData['status'])) $where['status']=$mailData['status'];
        if($mailData['from']) $where['createdate >=']=$mailData['from'];
        if($mailData['to']) $where['createdate <=']=$mailData['to'];
        $start = ($mailData['start'])?$mailData['start']:0;
        $limit = ($mailData['limit'])?$mailData['limit']:999;
        
        $this->db->where($where);
        $total = $this->db->count_all_results();
        
        
        $this->db->from(TBL_MAIL);
        $this->db->where($where);
        $this->db->order_by("createdate", "desc"); 
        $this->db->limit($limit, $start);
        $posts = $this->db->get()->result_array();
        if( is_array($posts) && count($posts) > 0 ) {
          return $posts;
        }
        return false;
    }
}
