<?php

class Public_Model extends CI_Model {

  function get_post_tags($post_id,$user_id){
    $this->db->from(TBL_PTAG);
    $where = array('post_id' => $post_id,'user_id'=>$user_id);
    $this->db->where($where);
    $r = $this->db->get()->result_array();
    if ( is_array($r) && count($r) > 0) {
        return $r;
    }
    return false;
  }
  
  
    function post_search($k){//搜索文章 title or content
        //SELECT A.*,D.avatar,D.real_name,D.username FROM el_post AS A left join el_post_tag AS B on A.id = B.post_id left join el_tags AS C on C.id = B.tag_id left join el_user AS D ON A.user_id = D.id WHERE (C.value like "%英雄%" OR A.title like "%英雄%" )
                
        $k = str_replace('"', '\"', $k);
//        $sql = 'SELECT A.*,D.avatar,D.real_name,D.username FROM '.
//                $this->db->dbprefix(TBL_PUBL).' AS A'.
//                ' LEFT JOIN '.$this->db->dbprefix(TBL_PTAG).' AS B ON A.post_id = B.post_id'.
//                ' LEFT JOIN '.$this->db->dbprefix(TBL_TAGS).' AS C ON C.id = B.tag_id'.
//                ' LEFT JOIN '.$this->db->dbprefix(TBL_USER).' AS D ON A.user_id = D.id WHERE'.
//                ' (C.value like "%'.$k.'%" OR A.title like "%'.$k.'%")';
        
        $sql = 'SELECT A.*,D.avatar,D.real_name,D.username FROM '.
                $this->db->dbprefix(TBL_PUBL).' AS A'.
                ' LEFT JOIN '.$this->db->dbprefix(TBL_PTAG).' AS B ON A.post_id = B.post_id'.
                ' LEFT JOIN '.$this->db->dbprefix(TBL_TAGS).' AS C ON C.id = B.tag_id'.
                ' LEFT JOIN '.$this->db->dbprefix(TBL_USER).' AS D ON A.user_id = D.id WHERE'.
                ' (C.value like "%'.$k.'%" OR A.title like "%'.$k.'%" OR A.content like "%'.$k.'%")';
        //echo $sql;
        $r = $this->db->query($sql)->result_array();
        if ( is_array($r) && count($r) > 0) {
            return $r;
        }
        return false;
    }

  function get_post_by_id( $id,$user_id='') {
      $id=(int)$id;
      $user_id=(int)$user_id;
//    if($user_id){
//        $where['user_id']=$user_id;
//    }
        $query = $this->db->get_where(TBL_POST, array('id' => $id,'user_id'=>$user_id), 1, 0);
        if ($query->num_rows() > 0) {
            $post = $query->row_array();
            return $post;
        }
        return false;
  }
  
  function get_paths_for_user( $user_id) {
    $this->db->from(TBL_POST);
    if($user_id){
        $where['user_id']=$user_id;
    }
    $this->db->where($where);
    $this->db->select('id, file');
    $posts = $this->db->get()->result_array();
    if( is_array($posts) && count($posts) > 0 ) {
      return $posts;
    }
    return false;
  }
  function update_paths_for_user( $user_id,$data) {
    $this->db->update_batch(TBL_POST, $data, 'id'); 
  }
  
  function get_posts_for_user( $user_id,$mode=0,$type=0, $num_posts = 10 ,$offset = 0) {

    $this->db->from(TBL_POST);
    if($user_id){
        $where['user_id']=$user_id;
    }
    if($mode){
        $where['mode']=$mode;
    }
    if($type){
        $where['type']=$type;
    }
    $this->db->where($where);
    if($num_posts){
        $this->db->limit( $num_posts ,$offset);
    }
    
    $this->db->order_by('create_date','desc');

    $posts = $this->db->get()->result_array();
    
    if( is_array($posts) && count($posts) > 0 ) {
      return $posts;
    }
    return false;
  }
function  get_post_number( $postData) {
    $this->db->from(TBL_PUBL);
    $option = array();
    if(isset($postData['from'])) $this->db->where('create_date >=', $postData['from']);
    if(isset($postData['to'])) $this->db->where('create_date <=', $postData['to']);
    if(isset($postData['from2'])) $this->db->where('update_date >=', $postData['from2']);
    if(isset($postData['to2'])) $this->db->where('update_date <=', $postData['to2']);
    $r = $this->db->count_all_results();
    return $r;
}

  function get_post_count_for_user( $user_id ) {

    $this->db->from(TBL_POST);
    $this->db->where( array('user_id'=>$user_id) );

    return $this->db->count_all_results();
  }
  
  
  function get_publish_posts_for_user( $user_id,$mode=0,$type=0, $num_posts = 10 ,$offset = 0) {

    $this->db->from(TBL_PUBL);
    $where = array();
    if($user_id){
        $where['user_id']=$user_id;
    }
    if($mode){
        $where['mode']=$mode;
    }
    if($type){
        $where['type']=$type;
    }
    $this->db->where($where);
    if($num_posts){
        $this->db->limit( $num_posts ,$offset);
    }
    
    $this->db->order_by('update_date','desc');

    $posts = $this->db->get()->result_array();
    
    if( is_array($posts) && count($posts) > 0 ) {
      return $posts;
    }
    return false;
  }

  function get_publish_post_count_for_user( $user_id ) {

    $this->db->from(TBL_PUBL);
    $where = array();
    if($user_id){
        $where['user_id']=$user_id;
    }
    
    $this->db->where($where );

    return $this->db->count_all_results();
  }
  
  function update_publish_post_content( $id,&$content) {
    $data = array(
           'content' => $content,
           'file' => ''
        );
    $this->db->where('id', $id);
    $this->db->update(TBL_PUBL,$data); 
  }
  
  
  function transfor_entrustment($id){
        $id = (int)$id;
        $sql = 'INSERT '.$this->db->dbprefix(TBL_PENT).' (user_id,post_id,receiver_email,receiver_message,post_datetime,sendtime,imme)
                           SELECT B.user_id,A.post_id, C.email as receiver_email, C.message as receiver_message, B.update_date as post_datetime,B.deadline as sendtime,B.imme
                           FROM '.$this->db->dbprefix(TBL_PENC).' as A,'.$this->db->dbprefix(TBL_POST).' as B,'.$this->db->dbprefix(TBL_CONT).' as C
                           WHERE A.post_id = B.id AND A.contact_id = C.id AND B.user_id = "'.$id.'"';
        $query = $this->db->query($sql);
  }
  
  function get_entrustment_before_send($id){
        $id = (int)$id;
        $where .= "((sendtime <= '".GetCurrentTime()."') OR (imme = 1)) AND receiver_email != '' AND is_send = 0 ";
        if($id){
            $where .= " AND user_id = '$id'";
        }
        
        $this->db->from(TBL_PENT);
        $this->db->where($where);
        $r = $this->db->get()->result_array();
        //echo $this->db->last_query();
        return $r;
  }
  function set_entrustment_status($data){
        $this->db->where('id', $data['id']);
        $this->db->update(TBL_PENT,$data); 
  }
    function remove($id){
        $this->db->delete(TBL_POST, array('id' => $id)); 
    }
    function unactive($id){
        $data = array(
               'active' => ACTIVE_F
            );
        $this->db->where('id', $id);
        $this->db->update(TBL_PUBL,$data); 
    }
    function active($id){
        $data = array(
               'active' => ACTIVE_T
            );
        $this->db->where('id', $id);
        $this->db->update(TBL_PUBL,$data); 
    }
    
    
}
