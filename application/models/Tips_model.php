<?php

class Tips_Model extends CI_Model {

  function create_new_post($postData) {
    $data['title'] = $postData['title'];
    $data['subtitle'] = $postData['subtitle'];
    $data['cover_img'] = $postData['cover_img'];
    $data['title'] = $postData['title'];
    $data['mode'] = $postData['mode'];
    $data['type'] = $postData['type'];
    $data['year'] = (int)$postData['year'];
    $data['receivers'] = $postData['receivers'];
    $data['file'] = $postData['file'];
    $data['file-text'] = $postData['file-text'];
    $data['file-audio'] = $postData['file-audio'];
    $data['file-media'] = $postData['file-media'];
    $data['files'] = $postData['files'];
    $data['content'] = $postData['content'];
    $data['tags'] = $postData['tags'];
    $data['location'] = $postData['location'];
    $data['tree'] = $postData['tree'];
    $data['user_id'] = (int)$postData['user_id'];
    $data['author_id'] = ($postData['author_id'])?$postData['author_id']:$postData['user_id'];
    $data['writer_id'] = ($postData['writer_id'])?$postData['writer_id']:$postData['author_id'];
    $data['active'] = ($postData['active'])?ACTIVE_T:ACTIVE_F;
    $data['create_date'] = date('Y-m-d H:i:s');
    $data['deadline'] = $postData['deadline'];
    $data['social_token'] = $postData['social_token'];
    $data['imme'] = (int)$postData['imme'];
    $data['source'] = (int)$postData['source'];
    $data['date'] = $postData['date'];
    if(!$data['source']):
        $data['source'] = POST_SOUR_WEB;
    endif;
    $data['status'] = POST_SOUR_WEB;
    $data['scene'] = $postData['scene'];
    
    if ( $this->db->insert(TBL_TIPS,$data) ) {
      return $data;
    } else {
      return false;
    }
  }
  
  function edit_post($postData) {
    $data['title'] = $postData['title'];
    $data['subtitle'] = $postData['subtitle'];
    $data['cover_img'] = $postData['cover_img'];
    
    $data['file-text'] = $postData['file-text'];
    $data['file-audio'] = $postData['file-audio'];
    $data['file-media'] = $postData['file-media'];
    $data['files'] = $postData['files'];
    $data['content'] = $postData['content'];
    $data['tags'] = $postData['tags'];
    $data['location'] = $postData['location'];
    
    $data['mode'] = $postData['mode'];
    $data['year'] = (int)$postData['year'];
    $data['type'] = $postData['type'];
    $data['receivers'] = $postData['receivers'];
    $data['files'] = $postData['files'];
    $data['file'] = $postData['file'];
    $data['content'] = $postData['content'];
    $data['tags'] = $postData['tags'];
    $data['deadline'] = $postData['deadline'];
    $data['imme'] = (int)$postData['imme'];
    $data['date'] = $postData['date'];

    $this->db->where('id', $postData['id']);
    if ( $this->db->update(TBL_TIPS,$data) ) {
      return $data;
    } else {
      return false;
    }
  }

  function get_tips_by_id( $id,$user_id='') {
      $id=(int)$id;
      $user_id=(int)$user_id;
      if(!$user_id){
          $query = $this->db->get_where(TBL_TIPS, array('id' => $id), 1, 0);
      }else{
          $query = $this->db->get_where(TBL_TIPS, array('id' => $id,'user_id'=>$user_id), 1, 0);
      }
      
        if ($query->num_rows() > 0) {
            $post = $query->row_array();
            return $post;
        }
        return false;
  }
  

  function get_posts_for_user( $user_id,$mode=0,$type=0, $num_posts = 10 ,$offset = 0,$year=0) {

    $this->db->from(TBL_TIPS);
    if($user_id){
        $where['user_id']=$user_id;
    }
    if($mode){
        $where['mode']=$mode;
    }
    if($type){
        $where['type']=$type;
    }
    if($year){
        $where['year']=$year;
    }
    //$where['active']=ACTIVE_T;
    $this->db->where($where);
    if($num_posts){
        $this->db->limit( $num_posts ,$offset);
    }
    
    $this->db->order_by('create_date','desc');

    $posts = $this->db->get()->result_array();
    
    if( is_array($posts) && count($posts) > 0 ) {
      return $posts;
    }
    return false;
  }

  
  function get_post_year_by_user( $user_ids,$mode=0,$type=0) {
    $this->db->distinct();
    $this->db->from(TBL_TIPS);
    $user_ids = array_filter( $user_ids, 'strlen' );
    if(!is_array($user_ids)):
        $user_ids=array($user_ids);
    endif;
    $where['user_id in ('.implode(',', $user_ids).')']= null;

    if($mode){
        $where['mode']=$mode;
    }
    if($type){
        $where['type']=$type;
    }
    $where['active']=ACTIVE_T;
    $this->db->where($where);
    if($num_posts){
        $this->db->limit( $num_posts ,$offset);
    }
    
    $this->db->select('year');
    $posts = $this->db->get()->result_array();
    if( is_array($posts) && count($posts) > 0 ) {
      return $posts;
    }
    return false;
  }
  
    function get_post_count_for_user( $user_id ,$mode=0,$type=0,$year=0) {
      $where = array('user_id'=>$user_id) ;
      if($year){
          $where['year']=$year;
      }
      if($mode){
          $where['mode']=$mode;
      }
      if($type){
          $where['type']=$type;
      }
      //$where['active']=ACTIVE_T;
      $this->db->from(TBL_TIPS);
      $this->db->where( $where );

      return $this->db->count_all_results();
    }
  
  
    function remove($id){
        $this->db->delete(TBL_TIPS, array('id' => $id)); 
    }
    function unactive($id){
        $data = array(
               'active' => ACTIVE_F
            );
        $this->db->where('id', $id);
        $this->db->update(TBL_TIPS,$data); 
    }
    function active($id){
        $data = array(
               'active' => ACTIVE_T
            );
        $this->db->where('id', $id);
        $this->db->update(TBL_TIPS,$data); 
    }

    //get post tips by scene
    function get_tips_by_scene($scene){
        $query = $this->db->get_where(TBL_TIPC, array('scene' => $scene), 1, 0);
        if ($query->num_rows() > 0) {
            $post = $query->row_array();
        }else{
            return false;
        }
        return $post;
    }
    
    //post tips used
    function used_tips($scene,$user_id,$tips_id){
        $data = array();
        if($user_id) $data['user_id'] = $user_id;
        if($tips_id) $data['tips_id'] = $tips_id;

        $this->db->where('scene', $scene);
        if ( $this->db->update(TBL_TIPC,$data) ) {
          return $data;
        } else {
          return false;
        }
    }
    function tips_bind_scene($scene,$user_id,$id){
        $data = array();
        
        if($user_id) $where['user_id'] = $user_id;
        if($id) $where['id'] = $id;
        $data['scene'] = $scene;

        $this->db->where($where);
        if ( $this->db->update(TBL_TIPS,$data) ) {
          return true;
        } else {
          return false;
        }
    }
    
    function clear_draft($scene,$tips_id){
        $data['active'] = ACTIVE_T;
        $this->db->where('id', $tips_id);
        $this->db->update(TBL_TIPS,$data);
                
        $this->db->delete(TBL_TIPS, array('scene' => $scene,'id <> '.$tips_id => NULL)); 
        return;
    }
    
    function remove_question($tips_id){
        return $this->db->delete(TBL_TIPQ, array('tips_id' => $tips_id)); 
    }
    function add_question($tips_id,$question,$answer){
        $data['tips_id'] = $tips_id;
        if($question && $answer){
            $data['question'] = $question;
            $data['answer'] = $answer;

            $this->remove_question($tips_id);
            
            if ( $this->db->insert(TBL_TIPQ,$data) ) {
              return $this->db->insert_id();
            } else {
              return false;
            }
        }else{
            return false;
        }
    }
    
    
    function get_question($tips_id){
        
        $query = $this->db->get_where(TBL_TIPQ, array('tips_id' => $tips_id), 1, 0);
        if ($query->num_rows() > 0) {
            $post = $query->row_array();
        }else{
            return false;
        }
        return $post;
    }
    
    
    function add_visit($tips_id){
        $data['tips_id'] = $tips_id;
        $data['ip'] = GetClientIP();

        if ( $this->db->insert(TBL_TIPV,$data) ) {
          return $data;
        } else {
          return false;
        }
    }
    function get_visits($tips_id){
        $this->db->where('tips_id', $tips_id);
        $this->db->from(TBL_TIPV);
        return $this->db->count_all_results();
    }
    
    function duplicate($scene,$user_id,$tips_id){
        $tips_id = (int)$tips_id;
        $user_id = (int)$user_id;
        if(!$tips_id || !$user_id || !$scene) return false;
        
//        $sql = 'DESCRIBE '.$this->db->dbprefix(TBL_TIPS);
//        //echo $sql;
//        $r = $this->db->query($sql)->result_array();
//        if ( empty($r) || !is_array($r)) {
//            return false;
//        }
//        $arr_col = array();
//        foreach($r as $row){
//            if($row['Key'] || $row['Extra'])    continue;
//            $arr_col[] = '`'.$row['Field'].'`';
//        }
//        $fields = implode(",", $arr_col);
        $fields = '`active`,`date`,`user_id`,`author_id`,`writer_id`,`title`,`content`,`year`,`file`,`files`,`file-text`,`file-audio`,`file-media`,`tags`,`location`,`mode`,`type`,`receivers`,`deadline`,`imme`,`tree`,`social_token`,`source`,`status`';
        $sql = 'INSERT INTO '.$this->db->dbprefix(TBL_TIPS).'('.$fields.') '
                . 'SELECT '.$fields.' FROM '.$this->db->dbprefix(TBL_TIPS)
                .' WHERE `id`='.$tips_id.' AND `user_id` ='.$user_id;
        $this->db->query($sql);
        $id = $this->db->insert_id();
        
        if(!$id){
            return false;
        }
        
        $data = array(
               'create_date' => GetCurrentTime(),
               'scene' => $scene
            );
        $this->db->where('id', $id);
        $this->db->update(TBL_TIPS,$data); 
        
        $result = $this->used_tips($scene,$user_id,$id);
        
        //need to copy question
        $question = $this->get_question($tips_id);
        if($question){
            $this->add_question($id,$question['question'],$question['answer']);
        }
        
        return $result;
        
    }
}
