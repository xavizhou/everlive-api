<?php

class Wechat_Model extends CI_Model {
    function  insert( $wxData) {
        $data['key'] = $wxData['key'];
        $data['open_id'] = $wxData['open_id'];
        $data['ip'] = ip2long(GetClientIP());
        if($this->db->insert(TBL_WECH,$data)){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }
}
