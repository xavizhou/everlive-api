<?php

class Ecode_Model extends CI_Model {

  function get_ecode($sku,$num_posts = 10 ,$offset = 0) {
    $this->db->from(TBL_ECOD);
    $this->db->where('sku',$sku);
    $posts = $this->db->get()->result_array();

    if( is_array($posts) && count($posts) > 0 ) {
      return $posts;
    }
    return false;
  }
  
  function generate_ecode($num,$sku) {
        $data = array();
        for($i=0;$i<$num;$i++){
            $data[] = array('ecode'=>md5(time().rand(0,1000)),'sku'=>$sku);
        }
        $this->db->insert_batch(TBL_ECOD, $data); 
  }
  
    function remove($id){
        $this->db->delete(TBL_ECOD, array('id' => $id)); 
    }
}
