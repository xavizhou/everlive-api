<?php
//标签管理
class Tag_Model extends CI_Model {
    function get_tags_for_user( $user_id, $num_posts = 10 ,$offset = 0) {
        //select * from el_tags as A left join (SELECT tag_id,count(id) as `count` FROM el_post_tag where user_id = 52 group by tag_id) as B on A.id = B.tag_id where user_id = 52
        if($num_posts) $limit = " LIMIT 0,$num_posts";
        $sql = 'SELECT A.id,A.value,A.order, B.count FROM '.
                $this->db->dbprefix(TBL_TAGS).' AS A'.
                ' LEFT JOIN 
                    (SELECT tag_id,COUNT(id) as `count` 
                    FROM '.$this->db->dbprefix(TBL_PTAG).' 
                    where user_id = "'.$user_id.'" group by tag_id) as B 
                    ON A.id = B.tag_id WHERE A.user_id = "'.$user_id.'"
                        AND (A.value REGEXP \'.*[^0-9].*\')
                        ORDER BY A.order ASC,B.count DESC '.$limit;
        $r = $this->db->query($sql)->result_array();
        if ( is_array($r) && count($r) > 0) {
            return $r;
        }
      return false;
    }
    
    
    function update_tags_for_user( $user_id, $tagData) {
        if(!is_array($tagData)) return true;
        foreach($tagData as $id=>$order){
            $data = array(
               'order' => $order
            );
            $this->db->where(array('id'=> $id,'user_id'=>$user_id));
            $this->db->update(TBL_TAGS, $data); 
        }
        return true;
    }
    
    function get_tag_by_id( $user_id, $id) {

      $this->db->from(TBL_TAGS);
      if($user_id){
          $this->db->where( array('user_id'=>$user_id) );
      }
      $this->db->where( array('id'=>$id) );
      $this->db->limit( 1 ,0);

      $list = $this->db->get()->result_array();

      if( is_array($list) && count($list) > 0 ) {
        return $list[0];
      }

      return false;
    }
    
    function get_tag_by_value( $value,$user_id) {

      $this->db->from(TBL_TAGS);
      $this->db->where( array('user_id'=>$user_id) );
      $this->db->where( array('value'=>$value) );
      $this->db->limit( 1 ,0);

      $list = $this->db->get()->result_array();

      if( is_array($list) && count($list) > 0 ) {
        return $list[0];
      }

      return false;
    }
    
    function get_tags_for_post( $tags) {
      $this->db->from(TBL_TAGS);
      $this->db->where_in('id', $tags);
      $list = $this->db->get()->result_array();

      if( is_array($list) && count($list) > 0 ) {
        return $list;
      }
      return false;
    }
    
    function  create_new( $userData) {
        $data['user_id'] = $userData['user_id'];
        $data['value'] = $userData['value'];
        $data['count'] = 1;
        $data['order'] = 999;
        if($this->db->insert(TBL_TAGS,$data)){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }
    
    function remove($id,$user_id){
        $this->db->delete(TBL_CONT, array('id' => $id,'user_id' => $user_id)); 
    }
    function removeAll($user_id){
        $this->db->delete(TBL_CONT, array('user_id' => $user_id)); 
    }
}
