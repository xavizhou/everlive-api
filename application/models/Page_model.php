<?php

class Page_Model extends CI_Model {
  function create_new_page( $pageData ) {
      $data['title'] = $pageData['title'];
      $data['content'] = $pageData['content'];
      $data['create_date'] = date('Y-m-d H:i:s',time());
      $data['active'] = ($pageData['actived'])?1:0;
      if($pageData['id']){
          $this->db->where('id', $id);
          return $this->db->update('page', $data); 
      }else{
          if($this->db->insert('page',$data)){
              return $this->db->insert_id();
          }else{
              return false;
          }
      }



      
  }
  function get_pages( $num_posts = 10 ) {

    $this->db->from('page');
    if($num_posts)   $this->db->limit( $num_posts );
    $posts = $this->db->get()->result_array();

    if( is_array($posts) && count($posts) > 0 ) {
      return $posts;
    }

    return false;
  }

  function get_page_by_id($id){
    $this->db->from('page');
    $this->db->where(array('id' =>$id));
    $pages = $this->db->get()->result_array();
    if( is_array($pages) && count($pages) > 0 ) {
      return $pages[0];
    }
    return false;
  }
  
  function remove($id,$user_id){
    $this->db->delete('page', array('id' => $id,'user_id' => $user_id)); 
  }
  
}
