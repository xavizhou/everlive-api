<?php
//消息管理
class Message_Model extends CI_Model {
    function  create_new( $userData) {
        $data['user_id'] = $userData['user_id'];
        $data['template'] = $userData['template'];
        $data['status'] = $userData['status'];
        $data['from_user_id'] = $userData['from_user_id'];
        $data['createdate'] = date('Y-m-d H:i:s');
        $data['message'] = $userData['message'];
        if($this->db->insert(TBL_USEM,$data)){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }
    
    function get_message_for_user( $user_id, $num_posts = 10 ,$offset = 0) {
      $this->db->from(TBL_USEM);
      if(empty($user_id)) return false;
      
      $this->db->where( array('user_id'=>$user_id,'status'=>NOTICE_UNRE));
      $this->db->limit( $num_posts ,$offset);
      //$this->db->order_by('createdate','desc');

      $posts = $this->db->get()->result_array();

      if( is_array($posts) && count($posts) > 0 ) {
        return $posts;
      }
      return false;
    }
    
    function read_message_for_user($user_id,$timestamp,$template=''){ 
        $this->db->set('status', NOTICE_READ);
        $this->db->where('user_id', $user_id); 
        if($template) $this->db->where('template', $template); 
        if($timestamp) $this->db->where('createdate > ', date('Y-m-d H:i:s',  strtotime($timestamp))); 
        return $this->db->update(TBL_USEM); 
    }
    
  function get_latest_notice($user_id){
      $r = $this->get_notice_for_user( $user_id, 1,0) ;
      if($r !== false) return $r[0];
      return false;
  }

  
  function get_post_count_for_user( $user_id ) {
    $this->db->from(TBL_NOTI);
    $this->db->where( array('user_id'=>$user_id) );

    return $this->db->count_all_results();
  }
  
    function remove($id){
        $this->db->delete(TBL_NOTI, array('id' => $id)); 
    }

    function set_status($id,$user_id,$status){
        $data = array(
               'stauts' => $status
            );
        $this->db->where( array('id' => $id,'user_id'=>$user_id));
        $this->db->update(TBL_NOTI,$data); 
    }
}
