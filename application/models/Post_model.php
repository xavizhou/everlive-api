<?php

class Post_Model extends CI_Model {

  function create_new_post($postData) {
    $data['title'] = $postData['title'];
    $data['subtitle'] = $postData['subtitle'];
    $data['cover_img'] = $postData['cover_img'];
    $data['mode'] = $postData['mode'];
    $data['type'] = (int)$postData['type'];
    $data['year'] = (int)$postData['year'];
    $data['receivers'] = $postData['receivers'];
    $data['file'] = $postData['file'];
    $data['files'] = $postData['files'];
    $data['file-text'] = $postData['file-text'];
    $data['file-audio'] = $postData['file-audio'];
    $data['file-media'] = $postData['file-media'];
    $data['content'] = $postData['content'];
    $data['tags'] = $postData['tags'];
    $data['location'] = $postData['location'];
    $data['tree'] = $postData['tree'];
    $data['user_id'] = (int)$postData['user_id'];
    $data['author_id'] = ($postData['author_id'])?$postData['author_id']:$postData['user_id'];
    $data['writer_id'] = ($postData['writer_id'])?$postData['writer_id']:$postData['author_id'];
    $data['active'] = ($postData['active'])?ACTIVE_T:ACTIVE_F;
    $data['create_date'] = ($postData['create_date'])?$postData['create_date']:date('Y-m-d H:i:s');
    $data['publish_date'] = ($postData['publish_date'])?$postData['publish_date']:date('Y-m-d H:i:s');
    $data['deadline'] = $postData['deadline'];
    $data['social_token'] = $postData['social_token'];
    $data['imme'] = (int)$postData['imme'];
    $data['source'] = (int)$postData['source'];
    $data['status'] = isset($postData['status'])?$postData['status']:POST_STATUS_SAVE;
    $data['template'] = isset($postData['template'])?(int)$postData['template']:0;
    $data['oss_status'] = 0;
    
    if(!$data['source']):
        $data['source'] = POST_SOUR_WEB;
    endif;

    if ( $this->db->insert(TBL_POST,$data) ) {
      return $data;
    } else {
      return false;
    }
  }
  
  function edit_post($postData) {
    $data['title'] = $postData['title'];
    $data['subtitle'] = $postData['subtitle'];
    $data['cover_img'] = $postData['cover_img'];
    $data['mode'] = $postData['mode'];
    $data['year'] = (int)$postData['year'];
    $data['type'] = $postData['type'];
    $data['receivers'] = $postData['receivers'];
    $data['files'] = $postData['files'];
    $data['file'] = $postData['file'];
    $data['file-text'] = $postData['file-text'];
    $data['file-audio'] = $postData['file-audio'];
    $data['file-media'] = $postData['file-media'];
    $data['content'] = $postData['content'];
    $data['tags'] = $postData['tags'];
    $data['deadline'] = $postData['deadline'];
    $data['imme'] = (int)$postData['imme'];
    if($postData['publish_date']){
        $data['publish_date']=$postData['publish_date'];
    }
    if($postData['create_date']){
        $data['create_date']=$postData['create_date'];
    }

    $this->db->where('id', $postData['id']);
    if ( $this->db->update(TBL_POST,$data) ) {
      return $data;
    } else {
      return false;
    }
  }

  
  function set_post_tags($post_id,$tags,$user_id){
    $this->db->delete(TBL_PTAG, array('post_id' => $post_id,'user_id'=>$user_id)); 
    $data = array();
    if(is_array($tags)){
        for($i=0;$i<count($tags);$i++){
            $data[] = array('post_id' => $post_id,'user_id'=>$user_id,'tag_id'=>$tags[$i]);
        }
    }
    if($data){
        return $this->db->insert_batch(TBL_PTAG, $data); 
    }
  }
  
  function get_post_tags($post_id,$user_id){
    $this->db->from(TBL_PTAG);
    $where = array('post_id' => $post_id,'user_id'=>$user_id);
    $this->db->where($where);
    $r = $this->db->get()->result_array();
    if ( is_array($r) && count($r) > 0) {
        return $r;
    }
    return false;
  }
  
  function get_post_num_by_author_id($author_id){
    if(!$author_id):
        return false;
    endif;
    $this->db->from(TBL_POST);
    $this->db->where('author_id', $author_id);
    $r = $this->db->count_all_results();
    return $r;
      
  }
  
  function get_post_num_by_id($user_id){
    if(!$user_id):
        return false;
    endif;
    $this->db->from(TBL_POST);
    $this->db->where('user_id', $user_id);
    $r = $this->db->count_all_results();
    return $r;
      
  }
  function get_audio_num_by_id($user_id){
    if(!$user_id):
        return false;
    endif;
    $this->db->from(TBL_POST);
    $this->db->where('file-audio !=', "");
    $this->db->where('user_id', $user_id);
    $r = $this->db->count_all_results();
    return $r;
  }
  function get_media_num_by_id($user_id){
    if(!$user_id):
        return false;
    endif;
    $this->db->from(TBL_POST);
    $this->db->where('file-media !=', "");
    $this->db->where('user_id', $user_id);
    $r = $this->db->count_all_results();
    return $r;
  }
  function get_image_num_by_id($user_id){
    if(!$user_id):
        return false;
    endif;
    $this->db->select('files');
    $this->db->from(TBL_POST);
    $this->db->where('user_id', $user_id);
    $r = $this->db->get()->result_array();
    $s = 0;
    if ( is_array($r)  && count($r) > 0) {
        for($i=0;$i<count($r);$i++){
            if($r[$i]['files']):
                $s += count(explode(",", $r[$i]['files']));
            endif;
        }
    }
    return $s;
  }
    function  get_post_number( $postData) {
        $this->db->from(TBL_POST);
        $option = array();
        if(isset($postData['from'])) $this->db->where('create_date >=', $postData['from']);
        if(isset($postData['to'])) $this->db->where('create_date <=', $postData['to']);
        if(isset($postData['from2'])) $this->db->where('update_date >=', $postData['from2']);
        if(isset($postData['to2'])) $this->db->where('update_date <=', $postData['to2']);
        $r = $this->db->count_all_results();
        return $r;
    }
    function  get_social_number( $postData) {
        $this->db->from(TBL_PSOC);
        $option = array();
        if(isset($postData['from'])) $this->db->where('create_date >=', $postData['from']);
        if(isset($postData['to'])) $this->db->where('create_date <=', $postData['to']);
        if(isset($postData['from2'])) $this->db->where('update_date >=', $postData['from2']);
        if(isset($postData['to2'])) $this->db->where('update_date <=', $postData['to2']);
        $r = $this->db->count_all_results();
        return $r;
    }
  
    function post_search($k){//搜索文章 title or content
        //SELECT A.*,D.avatar,D.real_name,D.username FROM el_post AS A left join el_post_tag AS B on A.id = B.post_id left join el_tags AS C on C.id = B.tag_id left join el_user AS D ON A.user_id = D.id WHERE (C.value like "%英雄%" OR A.title like "%英雄%" )
        $k = str_replace('"', '\"', $k);
        
        $sql = 'SELECT A.*,D.avatar,D.real_name,D.username FROM '.
                $this->db->dbprefix(TBL_PUBL).' AS A'.
                ' LEFT JOIN '.$this->db->dbprefix(TBL_PTAG).' AS B ON A.post_id = B.post_id'.
                ' LEFT JOIN '.$this->db->dbprefix(TBL_TAGS).' AS C ON C.id = B.tag_id'.
                ' LEFT JOIN '.$this->db->dbprefix(TBL_USER).' AS D ON A.user_id = D.id WHERE'.
                ' (C.value like "%'.$k.'%" OR A.title like "%'.$k.'%")';
        //echo $sql;
        $r = $this->db->query($sql)->result_array();
        if ( is_array($r) && count($r) > 0) {
            return $r;
        }
        return false;
    }

  function get_post_by_id( $id,$user_id='') {
      $id=(int)$id;
      $user_id=(int)$user_id;
//    if($user_id){
//        $where['user_id']=$user_id;
//    }
      //no idea why comment upon check
      if($user_id == -1){
          $query = $this->db->get_where(TBL_POST, array('id' => $id), 1, 0);
      }else{
          $query = $this->db->get_where(TBL_POST, array('id' => $id,'user_id'=>$user_id), 1, 0);
      }
      
        if ($query->num_rows() > 0) {
            $post = $query->row_array();
            return $post;
        }
        return false;
  }
  
    function get_first_post_by_cid($cid){
        if(empty($cid)) return false;
        $sql = 'SELECT DISTINCT A.create_date,D.id,D.real_name,D.gender,D.display_name,D.username,D.avatar ,D.key FROM '.
                $this->db->dbprefix(TBL_POST).' AS A'.
                ' LEFT JOIN '.$this->db->dbprefix(TBL_USER).' AS D ON A.author_id = D.id WHERE'.
                ' A.user_id = '.$cid.
                ' GROUP BY A.author_id'.
                ' ORDER BY A.create_date ASC';
        
        $r = $this->db->query($sql)->result_array();
        
        if ( is_array($r) && count($r) > 0) {
            return $r;
        }
        return false;
    }
  
  
    function get_post_by_author_id( $id,$author_id='') {
      $id=(int)$id;
      $author_id=(int)$author_id;
        $query = $this->db->get_where(TBL_POST, array('id' => $id,'author_id'=>$author_id), 1, 0);
        if ($query->num_rows() > 0) {
            $post = $query->row_array();
            return $post;
        }
        return false;
  }
  function get_paths_for_user( $user_id) {
    $this->db->from(TBL_POST);
    if($user_id){
        $where['user_id']=$user_id;
    }
    $this->db->where($where);
    $this->db->select('id, file');
    $posts = $this->db->get()->result_array();
    if( is_array($posts) && count($posts) > 0 ) {
      return $posts;
    }
    return false;
  }
  function update_paths_for_user( $user_id,$data) {
    $this->db->update_batch(TBL_POST, $data, 'id'); 
  }
  
  function get_post_count_by_author( $author_id ,$mode=0) {
    if(empty($author_id)):
        return false;
    endif;
    $where['author_id'] = $author_id;
    if($mode){
        $where['mode']=$mode;
    }
    $this->db->from(TBL_POST);
    $this->db->where( $where );
    return $this->db->count_all_results();
  }
  
  function get_posts_by_author($author_id,$mode=0,$num_posts = 10,$offset = 0){ 
      if(empty($author_id)):
        return false;
    endif;
    
    $limit = " LIMIT $offset,$num_posts";
    $sql = 'SELECT A.id,A.title,A.create_date,A.user_id,B.status FROM '.$this->db->dbprefix(TBL_POST).' AS A
        LEFT JOIN '.$this->db->dbprefix(TBL_PCON).' AS B 
        ON B.post_id = A.id WHERE A.author_id = "'.$author_id.'" ORDER BY A.publish_date DESC,A.id DESC '.$limit;

    $posts = $this->db->query($sql)->result_array();
    if( is_array($posts) && count($posts) > 0 ) {
      return $posts;
    }
    return false;
  }
  
  function get_posts_for_user( $user_id,$mode=0,$type=0, $num_posts = 10 ,$offset = 0,$year=0,$createdate='') {
    if(empty($user_id)):
        return false;
    endif;

    $this->db->from(TBL_POST);
    if(!is_array($user_id)):
        $user_id=array($user_id);
    endif;
    $user_id = array_diff($user_id, array(null,'null','',' '));
    $where['user_id in ('.implode(',', $user_id).')']= null;
    
//    if($user_id){
//        $where['user_id']=$user_id;
//    }
    if($mode){
        $where['mode']=$mode;
    }
    if($type){
        $where['type']=$type;
    }
    if($year && IsNumerics($year)){
        $where['year']=$year;
    }elseif($year){
        $where['create_date <= '] = $year.' 23:59:59';
    }
    if($createdate){
        $tmp = strtotime($createdate);
        if($tmp):
            $where['create_date >=']=date('Y-m',$tmp).'-01 00:00:00';
            $where['create_date <']=date('Y-m-t',$tmp).' 23:59:59';
        endif;
        
    }
    $this->db->where($where);
    if($num_posts){
        $this->db->limit( $num_posts ,$offset);
    }
    
    $this->db->order_by('create_date','desc');

    $posts = $this->db->get()->result_array();
    if( is_array($posts) && count($posts) > 0 ) {
      return $posts;
    }
    return false;
  }

  
  
  function get_posts_for_book( $user_ids,$mode=0,$type=0, $num_posts = 10 ,$years=0,$yeare=0) {
    if(empty($user_ids)):
        return false;
    endif;

    $this->db->from(TBL_POST);
    if(!is_array($user_ids)):
        $user_ids=array($user_ids);
    endif;
    $where['user_id in ('.implode(',', $user_ids).')']= null;

    if($mode){
        $where['mode']=$mode;
    }
    if($type){
        $where['type']=$type;
    }
    if($years){
        $where['year >=']=$years;
    }
    if($yeare){
        $where['year <=']=$yeare;
    }
    $this->db->where($where);
    if($num_posts){
        $this->db->limit( $num_posts ,$offset);
    }
    $this->db->order_by('year asc, create_date asc');
    //$this->db->order_by('year','asc');

    $posts = $this->db->get()->result_array();
    //echo $this->db->last_query();
    if( is_array($posts) && count($posts) > 0 ) {
      return $posts;
    }
    return false;
  }
  
  function get_post_year_by_user( $user_ids,$mode=0,$type=0) {
    if(empty($user_ids)):
        return false;
    endif;
    $this->db->distinct();
    $this->db->from(TBL_POST);
    $user_ids = array_filter( $user_ids, 'strlen' );
    if(!is_array($user_ids)):
        $user_ids=array($user_ids);
    endif;
    $where['user_id in ('.implode(',', $user_ids).')']= null;

    if($mode){
        $where['mode']=$mode;
    }
    if($type){
        $where['type']=$type;
    }
    $this->db->where($where);
//    if($num_posts){
//        $this->db->limit( $num_posts ,$offset);
//    }
    
    $this->db->select('year');
    $posts = $this->db->get()->result_array();
    if( is_array($posts) && count($posts) > 0 ) {
      return $posts;
    }
    return false;
  }
  
  function get_post_count_for_user( $user_id ,$mode=0,$type=0,$year=0,$createdate='') {
    //$where = array('user_id'=>$user_id) ;
    if(empty($user_id)):
        return false;
    endif;
    if(!is_array($user_id)):
        $user_id=array($user_id);
    endif;
    $user_id = array_diff($user_id, array(null,'null','',' '));
    $where['user_id in ('.implode(',', $user_id).')']= null;
    
    if($year && IsNumerics($year)){
        $where['year']=$year;
    }elseif($year){
        $where['create_date <= '] = $year.' 23:59:59';
    }
    if($mode){
        $where['mode']=$mode;
    }
    if($type){
        $where['type']=$type;
    }
    if($createdate){
        $tmp = strtotime($createdate);
        if($tmp):
            $where['create_date >=']=date('Y-m',$tmp).'-01 00:00:00';
            $where['create_date <']=date('Y-m-t',$tmp).' 23:59:59';
        endif;
        
    }
    $this->db->from(TBL_POST);
    $this->db->where( $where );

    return $this->db->count_all_results();
  }
  
  
  
  function get_posts_for_subuser( $user_id,$author_id,$mode=0,$type=0, $num_posts = 10 ,$offset = 0,$year=0,$createdate='') {
    if(empty($user_id)):
        return false;
    endif;
    $this->db->from(TBL_POST);
    if($user_id){
        $where['user_id']=$user_id;
    }
    if($author_id){
        $where['author_id']=$author_id;
    }
    if($mode){
        $where['mode']=$mode;
    }
    if($type){
        $where['type']=$type;
    }
    if($year && IsNumerics($year)){
        $where['year']=$year;
    }elseif($year){
        $where['create_date <= '] = $year.' 23:59:59';
    }
    if($createdate){
        $tmp = strtotime($createdate);
        if($tmp):
            $where['create_date >=']=date('Y-m',$tmp).'-01 00:00:00';
            $where['create_date <']=date('Y-m-t',$tmp).' 23:59:59';
        endif;
        
    }
    $this->db->where($where);
    if($num_posts){
        $this->db->limit( $num_posts ,$offset);
    }
    
    $this->db->order_by('create_date','desc');

    $posts = $this->db->get()->result_array();
    if( is_array($posts) && count($posts) > 0 ) {
      return $posts;
    }
    return false;
  }

  function get_post_count_for_subuser( $user_id,$author_id,$mode=0,$type=0, $year=0,$createdate='') {
    if(empty($user_id)):
        return 0;
    endif;
    
    if($author_id){
        $where['author_id']=$author_id;
    }
    if($user_id){
        $where['user_id']=$user_id;
    }
    
    if($year && IsNumerics($year)){
        $where['year']=$year;
    }elseif($year){
        $where['create_date <= '] = $year.' 23:59:59';
    }
    
    if($mode){
        $where['mode']=$mode;
    }
    if($type){
        $where['type']=$type;
    }
    if($createdate){
        $tmp = strtotime($createdate);
        if($tmp):
            $where['create_date >=']=date('Y-m',$tmp).'-01 00:00:00';
            $where['create_date <']=date('Y-m-t',$tmp).' 23:59:59';
        endif;
        
    }
    $this->db->from(TBL_POST);
    $this->db->where($where);

    return $this->db->count_all_results();
  }
  
  
  
  function get_posts_for_tree( $user_id,$year=0) {
    $this->db->from(TBL_POST);
    $where['user_id']=$user_id;
    $where['type']=POST_TYPE_BIOG;
    $where['tree']=1;
    if($year){
        $where['year']=$year;
    }
    $this->db->where($where);
    $this->db->order_by('year','desc');

    $posts = $this->db->get()->result_array();
    
    if( is_array($posts) && count($posts) > 0 ) {
      return $posts;
    }
    return false;
  }
  
  
  function get_publish_posts_for_user_by_year( $user_id,$year) {
    $this->db->from(TBL_PUBL);
    if($user_id){
        $where['user_id']=$user_id;
    }
    if($year){
        $where['year']=$year;
    }
    $this->db->where($where);
    
    $this->db->order_by('update_date','desc');

    $posts = $this->db->get()->result_array();
    
    //echo $this->db->last_query();
    if( is_array($posts) && count($posts) > 0 ) {
      return $posts;
    }
    return false;
  }
  
  function get_publish_posts_for_user( $user_id,$mode=0,$type=0, $num_posts = 10 ,$offset = 0) {

    $this->db->from(TBL_PUBL);
    if($user_id){
        $where['user_id']=$user_id;
    }
    if($mode){
        $where['mode']=$mode;
    }
    if($type){
        $where['type']=$type;
    }
    if($year){
        $where['year']=$year;
    }
    $this->db->where($where);
    if($num_posts){
        $this->db->limit( $num_posts ,$offset);
    }
    
    $this->db->order_by('update_date','desc');

    $posts = $this->db->get()->result_array();
    
    //echo $this->db->last_query();
    if( is_array($posts) && count($posts) > 0 ) {
      return $posts;
    }
    return false;
  }

  function get_publish_post_count_for_user( $user_id ) {

    $this->db->from(TBL_PUBL);
    $this->db->where( array('user_id'=>$user_id) );

    return $this->db->count_all_results();
  }
  
  function update_publish_post_content( $id,&$content) {
    $data = array(
           'content' => $content,
           'file' => ''
        );
    $this->db->where('id', $id);
    $this->db->update(TBL_PUBL,$data); 
  }
  
  function set_imme_by_id($id,$imme){
        $data = array(
               'imme' => $imme
            );
        $this->db->where('id', $id);
        $this->db->update(TBL_POST,$data); 
  }
  
  function transfor_entrustment($id){
        $id = (int)$id;
        $sql = 'INSERT '.$this->db->dbprefix(TBL_PENT).' (user_id,post_id,receiver_email,receiver_message,post_datetime,sendtime,imme)
                           SELECT B.user_id,A.post_id, C.email as receiver_email, C.message as receiver_message, B.update_date as post_datetime,B.deadline as sendtime,B.imme
                           FROM '.$this->db->dbprefix(TBL_PENC).' as A,'.$this->db->dbprefix(TBL_POST).' as B,'.$this->db->dbprefix(TBL_CONT).' as C
                           WHERE A.post_id = B.id AND A.contact_id = C.id AND B.user_id = "'.$id.'"';
        
        $query = $this->db->query($sql);
  }
  
  function get_entrustment_before_send($id){
        $id = (int)$id;
        //$where .= "((sendtime <= '".GetCurrentTime()."') OR (imme = 1)) AND receiver_email != '' AND is_send = 0 ";
        $where .= "((sendtime <= '".GetCurrentTime()."') OR (imme = 1)) AND is_send = 0 ";
        
        $sql = 'SELECT A.*,B.username,B.real_name,B.display_name ,B.gender 
            FROM `'.$this->db->dbprefix(TBL_PENT).'` as A,'.$this->db->dbprefix(TBL_USER).' as B 
            WHERE ((A.sendtime <= "'.GetCurrentTime().'") OR (A.imme = 1)) 
                AND A.is_send = 0 
                AND A.user_id = B.id';
                
        if($id){
            $sql .= " AND user_id = '$id'";
        }
        
        $r = $this->db->query($sql)->result_array();
        
        //echo $this->db->last_query();
        return $r;
  }
  
  function set_entrustment_status($data){
        $this->db->where('id', $data['id']);
        $this->db->update(TBL_PENT,$data); 
  }
    function remove($id){
        $this->db->delete(TBL_POST, array('id' => $id)); 
    }
    function unactive($id){
        $data = array(
               'active' => ACTIVE_F
            );
        $this->db->where('id', $id);
        $this->db->update(TBL_POST,$data); 
    }
    function active($id){
        $data = array(
               'active' => ACTIVE_T
            );
        $this->db->where('id', $id);
        $this->db->update(TBL_POST,$data); 
    }
    
    function set_tree($id,$user_id,$v){
        $data = array(
               'tree' => $v
            );
        $this->db->where('id', $id);
        $this->db->where('user_id', $user_id);
        $this->db->update(TBL_POST,$data); 
    }
    function get_tree_number($author_id,$user_id,$year){
        $this->db->where('tree', 1);
        $this->db->where('author_id', $author_id);
        $this->db->where('user_id', $user_id);
        $this->db->where('year', $year);
        $this->db->where('type', POST_TYPE_BIOG);
        $this->db->from(TBL_POST);
        return $this->db->count_all_results();
    }
    
    function post_to_social_check($id,$user_id,&$pid){//是否公开过
        $id = (int)$id;
        $user_id = (int)$user_id;
        
        $sql = 'SELECT * FROM '.$this->db->dbprefix(TBL_POST).'
            WHERE `id` = '.$id.'
            AND ((`author_id` = "'.$user_id.'") OR (`user_id` = "'.$user_id.'")) LIMIT 0,1';//写给别人的或者别人写给我的 自己都能分享
        
        //$query = $this->db->get_where(TBL_POST, array('id' => $id,'author_id'=>$user_id), 1, 0);
        
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $post = $query->row_array();
        }else{
            return false;
        }
        
        if($post['social_token']){
            $pid = $post['social_token'];
            return true;
        }
        
        $this->db->from(TBL_PSOC);
        //$this->db->where( array('user_id'=>$user_id,'post_id'=>$id,'update_date'=>$post['update_date']));
        //$this->db->where( array('user_id'=>$user_id,'post_id'=>$id));//不需要判断更新时间
        $this->db->where( array('post_id'=>$id));//不需要判断更新时间
        $this->db->limit( 1 ,0);
        $this->db->order_by('create_date','desc');
        
        $query = $this->db->get()->result_array();
        //echo $this->db->last_query();
        foreach ($query as $row)
        {
           $pid = $row['k'];
        }
        return true;
        
    }
    
    function post_to_social($id,$user_id){
        $id = (int)$id;
        $user_id = (int)$user_id;
        if(empty($id) || empty($user_id)) return false;
        
        $pid = 0;
        $r = $this->post_to_social_check($id,$user_id,$pid);
        
        if($r == false) return false;
        if($pid) return $pid;
        $k = md5(time().rand());
        //首次公开，则复制数据
        $sql = 'INSERT '.$this->db->dbprefix(TBL_PSOC).' (post_id,create_date,update_date, user_id, title,content,file,mode,type,k)
                           SELECT id, create_date, update_date,user_id, title,content,file,mode,type,"'.$k.'"
                           FROM '.$this->db->dbprefix(TBL_POST).'
                           WHERE `id` = '.$id;
        $query = $this->db->query($sql);
        $id = $this->db->insert_id();
        return $k;
        //return array($id,$k);
    }
    
    
    function get_social_list(&$num,$num_posts = 10 ,$offset = 0) {
      $this->db->from(TBL_PSOC);
      $num = $this->db->count_all_results();

      $this->db->from(TBL_PSOC);
      if($num_posts){
          $this->db->limit( $num_posts ,$offset);
      }

      $this->db->order_by('id','desc');

      $posts = $this->db->get()->result_array();
      if( is_array($posts) && count($posts) > 0 ) {
        return $posts;
      }
      return false;
    }

    function get_post_by_k($k){
        if(!$k){
            return false;
        }
        $sql = 'SELECT B.* FROM '.$this->db->dbprefix(TBL_PSOC).' AS A 
                LEFT JOIN '.$this->db->dbprefix(TBL_POST).' AS B 
                ON A.post_id = B.id where A.k="'.$k.'"';
        
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }else{
            return false;
        }
        
    }
  
    function get_social_post_by_k( $k) {
      $id=(int)$id;
      $query = $this->db->get_where(TBL_PSOC, array('k' => $k), 1, 0);
      if ($query->num_rows() > 0) {
        $post = $query->row_array();
        return $post;
      }
      return false;
    }
    
    function get_social_post_by_id( $id) {
      $id=(int)$id;
      $query = $this->db->get_where(TBL_PSOC, array('id' => $id), 1, 0);
      if ($query->num_rows() > 0) {
        $post = $query->row_array();
        return $post;
      }
      return false;
    }

    function contribute_by_id($id,$user_id){
        $data['is_contribute'] = 1;

        $this->db->where(array('user_id'=>$user_id,'id'=>$id));
        if ( $this->db->update(TBL_PSOC,$data) ) {
          return true;
        } else {
          return false;
        }
    }
    
    
    
    function get_contribute_count_by_author( $author_id) {
        if(empty($author_id)):
            return false;
        endif;

        $sql = 'SELECT COUNT(*) as "number" FROM '.$this->db->dbprefix(TBL_PCON).' AS A
            LEFT JOIN '.$this->db->dbprefix(TBL_POST).' AS B 
            ON A.post_id = B.id WHERE B.author_id = "'.$author_id.'" ORDER BY A.update_date DESC,A.id DESC '.$limit;
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $r = $query->row_array();
            return $r['number'];
        }else{
            return false;
        }
    }
  
    function get_contribute_by_author($author_id,$num_posts = 10,$offset = 0){ 
        if(empty($author_id)):
            return false;
        endif;

        $limit = " LIMIT $offset,$num_posts";
        $sql = 'SELECT A.id,A.title,A.create_date,A.status FROM '.$this->db->dbprefix(TBL_PCON).' AS A
            LEFT JOIN '.$this->db->dbprefix(TBL_POST).' AS B 
            ON A.post_id = B.id WHERE B.author_id = "'.$author_id.'" ORDER BY A.create_date DESC,A.id DESC '.$limit;

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }else{
            return false;
        }

    }
  
    
    function get_contribute_by_post_id($post_id){
      $id=(int)$id;
      $query = $this->db->get_where(TBL_PCON, array('post_id' => $post_id), 1, 0);
      if ($query->num_rows() > 0) {
        $post = $query->row_array();
        return $post;
      }
      return false;
    }
    
    function create_contribute($postData) {
      $data['post_id'] = $postData['id'];
      $data['title'] = $postData['title'];
      $data['category'] = $postData['category'];
      $data['mode'] = $postData['mode'];
      $data['type'] = $postData['type'];
      $data['file'] = $postData['file'];
      $data['files'] = $postData['files'];
      $data['file-text'] = $postData['file-text'];
      $data['file-audio'] = $postData['file-audio'];
      $data['file-media'] = $postData['file-media'];
      $data['content'] = $postData['content'];
      $data['user_id'] = (int)$postData['user_id'];
      $data['create_date'] = date('Y-m-d H:i:s');
      $data['key'] = $postData['key'];

      if ( $this->db->insert(TBL_PCON,$data) ) {
        return $this->db->insert_id();
      } else {
        return false;
      }
    }
    
    function publish_posts($user_id,$token){
        $user_id = (int)$user_id;
        //首次公开，则复制数据
//        $sql = 'INSERT '.$this->db->dbprefix(TBL_PUBL).' (post_id,create_date,update_date, user_id, title,content,file,files,mode,type,`key`)
//                           SELECT id, create_date, update_date,user_id, title,content,file,files,mode,type,"'.$token.'"
//                           FROM '.$this->db->dbprefix(TBL_POST).'
//                           WHERE `user_id` = '.$user_id.'
//                           AND `imme` = 1
//                           AND `type` = '.POST_TYPE_BIOG;
        $sql = 'INSERT '.$this->db->dbprefix(TBL_PUBL).' (post_id,create_date,update_date, user_id, title,content,file,files,mode,type,year,`key`)
                           SELECT id, create_date, update_date,user_id, title,content,file,files,mode,type,year,"'.$token.'"
                           FROM '.$this->db->dbprefix(TBL_POST).'
                           WHERE `user_id` = '.$user_id.'
                           AND `tree` = 1
                           AND `type` = '.POST_TYPE_BIOG;        
        $query = $this->db->query($sql);
        return $query;
    }
    
    
    //公开嘱托
    function publish_entrustment($id,$user_id){
        $id = (int)$id;
        $user_id = (int)$user_id;
        if(empty($id) || empty($user_id)) return false;
        
        //get the post
        $query = $this->db->get_where(TBL_POST, array('id' => $id,'user_id'=>$user_id), 1, 0);
        if ($query->num_rows() > 0) {
            $post = $query->row_array();
        }else{
            return false;
        }
        //所有联系人                $this->load->model('contact_model');
        $ids = explode(",",$post['receivers']);
        if($ids){
            $this->db->from(TBL_CONT);
            $this->db->where_in('id', $ids);
            $list = $this->db->get()->result_array();
        }else{
            $list = array();
        }
        
//        $data['title'] = $post['title'];
//        $data['mode'] = $post['mode'];
//        $data['type'] = $post['type'];
//        $data['receivers'] = $post['receivers'];
//        $data['file'] = $post['file'];
//        $data['content'] = $post['content'];
//        $data['tags'] = $post['tags'];
//        $data['user_id'] = (int)$post['user_id'];
//        $data['active'] = 1;
//        $data['create_date'] = $post['create_date'];
//        $data['deadline'] = date('Y-m-d H:i:s');
//        $data['receiver_id'] = 0;
////        for($i=0;$i<count($list);$i++){
////            $data['receiver'] = $list[$i]['email'];
////        }
//
//        if ( $this->db->insert(TBL_ENTU,$data) ) {
//          return $data;
//        } else {
//          return false;
//        }
//        return $this->db->insert_id();
        $url = md5(time().rand(1,10000));
        //首次公开，则复制数据
        $sql = 'INSERT '.$this->db->dbprefix(TBL_PENT).' (post_id,create_date,update_date, user_id, title,content,file,mode,url)
                           SELECT id, create_date, update_date,user_id, title,content,file,mode,\''.$url.'\'
                           FROM '.$this->db->dbprefix(TBL_POST).'
                           WHERE `id` = '.$id;
        $query = $this->db->query($sql);
        return $this->db->insert_id();
    }
    

    function get_publish_by_id($id){//搜索文章 title or conten
        $id = (int)$id;
        //get the post
        $query = $this->db->get_where(TBL_PUBL, array('id' => $id), 1, 0);
        if ($query->num_rows() > 0) {
            $post = $query->row_array();
        }else{
            return false;
        }
        return $post;
    }
    
//    function get_entrustment_by_id($id){
//        $id = (int)$id;
//        //get the post
//        $query = $this->db->get_where(TBL_PENT, array('id' => $id), 1, 0);
//        if ($query->num_rows() > 0) {
//            $post = $query->row_array();
//        }else{
//            return false;
//        }
//        return $post;
//    }
    
//    function get_entrustment_by_url($url){
//        $query = $this->db->get_where(TBL_PENT, array('url' => $url), 1, 0);
//        if ($query->num_rows() > 0) {
//            $post = $query->row_array();
//        }else{
//            return false;
//        }
//        return $post;
//    }
    function get_entrustment_by_key($key){
        $query = $this->db->get_where(TBL_PENT, array('token' => $key), 1, 0);
        if ($query->num_rows() > 0) {
            $post = $query->row_array();
        }else{
            return false;
        }
        return $post;
    }
    
    function remove_entrustment_by_key($key){
        return $this->db->delete(TBL_PENT, array('token' => $key)); 
    }
    
    function set_entrustment_contact($post_id,$receivers){
        $this->db->delete(TBL_PENC, array('post_id' => $post_id)); 
        $data = array();
        if(is_array($receivers)){
            foreach($receivers as $receiver){
                $data[] = array(
                    'post_id' => $post_id,
                    'contact_id' => $receiver
                );
            }
            $this->db->insert_batch(TBL_PENC, $data); 
        }
    }
    
//    
//    //get post tips by scene
//    function get_tips_by_scene($scene){
//        $query = $this->db->get_where(TBL_TIPC, array('scene' => $scene), 1, 0);
//        if ($query->num_rows() > 0) {
//            $post = $query->row_array();
//        }else{
//            return false;
//        }
//        return $post;
//    }
//    
//    //post tips used
//    function used_tips($scene,$user_id,$post_id){
//        $data = array();
//        if($user_id) $data['user_id'] = $user_id;
//        if($post_id) $data['post_id'] = $post_id;
//
//        $this->db->where('scene', $scene);
//        if ( $this->db->update(TBL_TIPC,$data) ) {
//          return $data;
//        } else {
//          return false;
//        }
//    }
    
     
    function add_visit($post_id,$user_id,$visit_id){
        $data['post_id'] = $post_id;
        $data['user_id'] = $user_id;
        $data['visit_id'] = $visit_id;
        $data['ip'] = GetClientIP();

        if ( $this->db->insert(TBL_POSV,$data) ) {
          return $data;
        } else {
          return false;
        }
    }
    
    function get_visits_by_post_id($post_id){
        $this->db->where('post_id', $post_id);
        $this->db->from(TBL_POSV);
        return $this->db->count_all_results();
    }
    
    function get_visits_by_visit_id($user_id,$visit_id){
        $this->db->distinct();
        $this->db->select('post_id','user_id','visit_id');
        
        if(is_array($user_id)):
            $this->db->where_in('user_id', $user_id);
        else:
            $this->db->where('user_id', $user_id);
        endif;
        
        $this->db->where('visit_id', $visit_id);
        $this->db->from(TBL_POSV);
        return $this->db->count_all_results();
    }
    
    function get_visit_list_by_visit_id($user_id,$visit_id){
        $this->db->distinct();
        $this->db->select('post_id','user_id','visit_id');
        
        if(is_array($user_id)):
            $this->db->where_in('user_id', $user_id);
        else:
            $this->db->where('user_id', $user_id);
        endif;
        
        $this->db->where('visit_id', $visit_id);
        $this->db->from(TBL_POSV);
        return $this->db->get()->result_array();
    }
    
    function get_need_visits_by_visit_id($user_id,$visit_id){
        if(is_array($user_id)):
            $_c = "A.`user_id` IN (".  implode(",", $user_id).")";
        else:
            $_c = "A.`user_id` = '$user_id'";
        endif;
        
        $sql = 'SELECT COUNT(*) as "number" FROM '.$this->db->dbprefix(TBL_POST).' as A
            WHERE A.`id` NOT IN 
            (SELECT DISTINCT `post_id` FROM '.$this->db->dbprefix(TBL_POSV).' WHERE `visit_id` = "'.$visit_id.'")
            AND '.$_c.' 
            AND A.`author_id` != "'.$visit_id.'"';//写给别人的或者别人写给我的 自己都能分享
        
        //$query = $this->db->get_where(TBL_POST, array('id' => $id,'author_id'=>$user_id), 1, 0);
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $r = $query->row_array();
            return $r['number'];
        }else{
            return false;
        }
        
        //SELECT * FROM `el_post` as A WHERE A.id not in (select distinct post_id from el_post_view where visit_id = 47) and user_id = 1167 and author_id != 47 ORDER BY `id` desc
    }
    
    
    function add_like($post_id,$user_id){
        $data['post_id'] = $post_id;
        $data['user_id'] = $user_id;
        $data['ip'] = GetClientIP();
        if ( $this->db->insert(TBL_POSL,$data) ) {
          return $data;
        } else {
          return false;
        }
    }

    
    function get_liked_users_by_post_id($post_id){
        $post_id = (int)$post_id;
        if(empty($post_id)){
            return false;
        }
        $sql = 'SELECT username,display_name,avatar FROM '.$this->db->dbprefix(TBL_POSL).' AS A
            LEFT JOIN '.$this->db->dbprefix(TBL_USER).' AS B ON A.user_id = B.id '
                . 'WHERE A.post_id = "'.$post_id.'"';//写给别人的或者别人写给我的 自己都能分享
        
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }else{
            return false;
        }
        
        //SELECT * FROM `el_post` as A WHERE A.id not in (select distinct post_id from el_post_view where visit_id = 47) and user_id = 1167 and author_id != 47 ORDER BY `id` desc
    }
    
    function get_likes_by_post_id($post_id){
        $this->db->where('post_id', $post_id);
        $this->db->from(TBL_POSL);
        return $this->db->count_all_results();
    }
    
    function is_liked($post_id,$user_id){
        if(!$user_id || !$post_id):
            return false;
        endif;
        $this->db->where('user_id', $user_id);
        $this->db->where('post_id', $post_id);
        $this->db->from(TBL_POSL);
        return $this->db->count_all_results();
    }
    
    
    function add_comment($post_id,$user_id,$comment){
        $data['post_id'] = $post_id;
        $data['user_id'] = $user_id;
        $data['comment'] = $comment;
        
        if ( $this->db->insert(TBL_POSC,$data) ) {
          return $data;
        } else {
          return false;
        }
        
    }
    
    
    function get_comment_by_post_id($post_id){
        if(!$post_id):
            return false;
        endif;
        
        $sql = 'SELECT A.user_id,A.post_id,A.comment,A.create_date,B.avatar,B.display_name FROM '.$this->db->dbprefix(TBL_POSC).' AS A
            LEFT JOIN '.$this->db->dbprefix(TBL_USER).' AS B ON A.user_id = B.id '
                . 'WHERE A.post_id = "'.$post_id.'" ORDER BY A.create_date DESC';//写给别人的或者别人写给我的 自己都能分享
        
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }else{
            return false;
        }
    }
    
    //copy_post
    //copy post from one tree to another tree
    function copy_post($post_ids,$tree_id,$cate_id=0,$subcate_id=0){
        if(!$post_ids):
            return false;
        endif;
        if(!is_array($post_ids)):
            $post_ids = array($post_ids);
        endif;
        
        for($i=0;$i<count($post_ids);$i++){
            $data[] = array('post_id' => $post_ids[$i],'tree_id'=>$tree_id,'cate_id'=>$cate_id,'subcate_id'=>$subcate_id);
        }
        
        if($data){
            return $this->db->insert_batch(TBL_PTRE, $data); 
        }
    }
    
    
    function get_category_by_cid($tree_id){
        $this->db->distinct();
        $this->db->select('id,name,date,cate_id');
        
        $this->db->where('tree_id', $tree_id);
        $this->db->from(TBL_CATE);
        return $this->db->get()->result_array();
    }
    
    function add_subcategory($cateData){
        $_data = array(
            'tree_id'=>'',
            'user_id'=>'',
            'cate_id'=>'',
            'name'=>'',
            'date'=>'',
            'ext'=>''
        );
        $data = array_merge($_data,$cateData);
        if(!$data['tree_id'] || !$data['user_id'] || !$data['cate_id'] || !$data['name']):
            return false;
        endif;
        if(!$data['date']):
            $data['date'] = GetCurrentDate();
        endif;

        if ( $this->db->insert(TBL_CATE,$data) ) {
          return $this->db->insert_id();
        } else {
          return false;
        }
    }
    
    
    function is_my_category($user_id,$id){
        $user_id = (int)$user_id;
        $id = (int)$id;
        
        if(!$id || !$user_id):
            return false;
        endif;
        $sql = 'SELECT A.id,A.tree_id,A.name,A.date,B.is_main,B.is_owner '
                . 'FROM `'.$this->db->dbprefix(TBL_CATE).'` AS A '
                . 'LEFT JOIN '.$this->db->dbprefix(TBL_UFOR).' AS B '
                . 'ON A.tree_id = B.cid AND A.user_id = B.user_id '
                . 'WHERE A.user_id = '.$user_id.' AND A.id = '.$id;
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }else{
            return false;
        }
    }
    
    function update_subcategory($cateData){
        if(!$cateData['id']) return false;
        if(!$cateData['name']) return false;
        
        $this->db->where('id', $cateData['id']);
        
        $data['name'] = $cateData['name'];
        if(isset($cateData['date'])):
            $data['date'] = $cateData['date'];
        endif;
        
        if ( $this->db->update(TBL_CATE,$data) ) {
          return true;
        } else {
          return false;
        }
        
    }
    
}
