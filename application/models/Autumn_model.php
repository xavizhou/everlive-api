<?php
define('TBL_ALO', 'autumn_location');
define('TBL_ARE', 'autumn_relationship');
define('TBL_AHB', 'autumn_hongbao');
define('TBL_AML','autumn_moneylog');
define('TBL_ABK','autumn_bank');

class Autumn_Model extends CI_Model {

	//openid,latitude,longitude,whether need status
	function create_new_location($locationData) {
            if(!$locationData['unionid'] || !$locationData['openid'] || !$locationData['latitude'] || !$locationData['longitude']):
                return false;
            endif;
		$data['openid'] = $locationData['openid'];
		$data['unionid']=$locationData['unionid'];
		$data['latitude'] = $locationData['latitude'];
		$data['longitude'] = $locationData['longitude'];
		if ($this -> db -> insert(TBL_ALO, $data)) {
			return $this -> db -> insert_id();
		} else {
			return false;
		}
	}
	
	
	function getlocation_by_unionid($unionid){
            if(!$unionid):
                return false;
            endif;
            $this->db->from(TBL_ALO);
            $data = array(
                'unionid'=>$unionid
            );
            $this->db->where($data);//maybe input username 
            $r = $this->db->get()->result_array();
                    if ( is_array($r) && count($r) > 0) {
                        return $r[0];
                    }
            return false;
        }
	
	function create_new_relationship($relationshipData) {
            if(!$relationshipData['sponsor_unionid'] || !$relationshipData['support_unionid']):
                return false;
            endif;
		$data['sponsor_unionid'] = $relationshipData['sponsor_unionid'];
		$data['support_unionid'] = $relationshipData['support_unionid'];
		$data['tinydistance'] = $relationshipData['tinydistance'];
		if ($this -> db -> insert(TBL_ARE, $data)) {
			return $this -> db -> insert_id();
		} else {
			return false;
		}
	}

	function check_relationship($sponsor_unionid,$support_unionid){
            if(!$sponsor_unionid || !$support_unionid):
                return false;
            endif;
                $sql = 'SELECT DISTINCT * FROM '.
                        $this->db->dbprefix(TBL_ARE).' WHERE'.
                        ' (`sponsor_unionid` =  "'.$sponsor_unionid.'" AND `support_unionid` =  "'.$support_unionid.'")'.
                        ' OR (`support_unionid` =  "'.$sponsor_unionid.'" AND `sponsor_unionid` =  "'.$support_unionid.'")';
                //echo $sql;
                $r = $this->db->query($sql)->result_array();
        
//		$this->db->from(TBL_ARE);
//		$this->db->where('sponsor_unionid',$sponsor_unionid);
//		$this->db->where('support_unionid',$support_unionid);
//		//$this->db->where('status',1);
//		$r=$this->db->get()->result_array();
		if ( is_array($r) && count($r) > 0) {
                    return $r[0];
		}
		return false;
		
	}
	
	function update_relationship($relationshipData){
            if(!$relationshipData['sponsor_unionid'] || !$relationshipData['support_unionid']):
                return false;
            endif;
		$this->db->from(TBL_ARE);
        $this->db->where('sponsor_unionid',$relationshipData['sponsor_unionid'] );//maybe input username 
        $this->db->where( 'support_unionid',$relationshipData['support_unionid']);
		$data['status']=$relationshipData['status'];
		return $this->db->update(TBL_ARE,$data);
        //$login = $this->db->get()->result();
        //return $this->db->update(TBL_ARE, $data); 
	}
	
	
	function updatestatus_by_unionid($unionid){
            if(!$unionid):
                return false;
            endif;
		$data=array(
			'status'=>1
		);
		$this->db->where('support_unionid',$unionid);
		$this->db->update(TBL_ARE,$data);
		
	}

    function downstatus_by_unionid($unionid){
            if(!$unionid):
                return false;
            endif;
        $data=array(
            'status'=>0
        );
        $this->db->where('support_unionid',$unionid);
        $this->db->update(TBL_ARE,$data);
    }
	
	function getfriends_by_unionid($unionid){
            if(!$unionid):
                return false;
            endif;
		$this->db->from(TBL_ARE);
		$this->db->where('sponsor_unionid',$unionid);
		$this->db->where('status',1);
		$friends = $this->db->get()->result_array();

		if( is_array($friends) && count($friends) > 0 ) {
			return $friends;
		}
        return false;

	}

    //下面该函数join TBL_USER TBL_USES
    //得到的数据结构 opc8DuOwGzPYe205_QeAQuUjpgj8
//oD7Djju3FB320hwUZKhpadMH0ano
//120
//282
//joyceyk
//阳光
//14081637592672.jpg

    function getfriends_and_avatar($unionid){
//        $sql="SELECT *
//FROM `".$this->db->dbprefix(TBL_ARE)."` , `".$this->db->dbprefix(TBL_USER)."` , `".$this->db->dbprefix(TBL_USES)."`
//WHERE `".$this->db->dbprefix(TBL_ARE)."`.support_unionid = `".$this->db->dbprefix(TBL_USES)."`.union_id
//AND `".$this->db->dbprefix(TBL_USES)."`.user_id = `".$this->db->dbprefix(TBL_USER)."`.id";
//        $sql="SELECT `el_autumn_relationship`.sponsor_unionid, `el_autumn_relationship`.support_unionid, `el_autumn_relationship`.tinydistance, `el_user`.id, `el_user`.username, `el_user`.display_name, `el_user`.avatar
//FROM `el_autumn_relationship` , `el_user` , `el_user_social`
//WHERE `el_autumn_relationship`.support_unionid = `el_user_social`.union_id
//        AND `el_user_social`.user_id = `el_user`.id
//        AND `el_autumn_relationship`.status =1
//        AND `el_autumn_relationship`.sponsor_unionid ='".$unionid."'";
        if(!$unionid):
            return false;
        endif;

        $sql="SELECT DISTINCT `".$this->db->dbprefix(TBL_ARE)."`.sponsor_unionid, `".$this->db->dbprefix(TBL_ARE)."`.support_unionid, `".$this->db->dbprefix(TBL_ARE)."`.tinydistance, `".$this->db->dbprefix(TBL_USER)."`.id, `".$this->db->dbprefix(TBL_USER)."`.username, `".$this->db->dbprefix(TBL_USER)."`.display_name, `".$this->db->dbprefix(TBL_USER)."`.avatar FROM `".$this->db->dbprefix(TBL_ARE)."` , `".$this->db->dbprefix(TBL_USER)."` , `".$this->db->dbprefix(TBL_USES)."` WHERE `".$this->db->dbprefix(TBL_ARE)."`.support_unionid = `".$this->db->dbprefix(TBL_USES)."`.union_id AND `".$this->db->dbprefix(TBL_USES)."`.user_id = `".$this->db->dbprefix(TBL_USER)."`.id AND `".$this->db->dbprefix(TBL_ARE)."`.status =1 AND `".$this->db->dbprefix(TBL_ARE)."`.sponsor_unionid ='".$unionid."' AND  `el_autumn_relationship`.support_unionid IS NOT NULL  AND  `el_autumn_relationship`.support_unionid <> '0'";
        $r = $this->db->query($sql)->result_array();
        
        if( is_array($r) && count($r) > 0 ) {
            return $r;
        }
        return false;
        //return $sql;


    }

    //用于统计邀请的朋友
    function getfriends_num($unionid){
        if(!$unionid):
            return 0;
        endif;
        $this->db->from(TBL_ARE);
        $this->db->where('sponsor_unionid',$unionid);
        $this->db->where('status',1);
        $friends = $this->db->get()->result_array();
        if( is_array($friends) && count($friends) > 0 ) {
            return count($friends);
        }
        return 0;
    }

    //用于统计帮助过的人，限制最多帮助数量
    function getSupport_num($support_unionid){
        if(!$support_unionid):
            return false;
        endif;
        $this->db->from(TBL_ARE);
        $this->db->where('support_unionid',$support_unionid);
        $this->db->where('status',1);
        $friends = $this->db->get()->result_array();
        if( is_array($friends) && count($friends) > 0 ) {
            return count($friends);
        }
        return 0;

    }


	function activatedistance_by_unionid($support_unionid){
            //更新关系表状态，更新distance的信息
            $this->db->from(TBL_ARE);
            $this->db->where('support_unionid',$support_unionid);
            $this->db->where('status',0); //把状态更改为1
            $related=$this->db->get()->result_array();
            $this->updatestatus_by_unionid($support_unionid);
            if(is_array($related) && count($related)>0){
                foreach ($related as $onerow) {
                    if($onerow['sponsor_unionid']):
                        $this->update_hongbao_distance($onerow['sponsor_unionid'],$onerow['tinydistance']);
                    
                    endif;
                }
                return true;
            }
            return false;

	}

    //用于在用户取消关注时更新相关被帮助者的距离缩减
    function  diactivatedistance_by_unionid($support_unionid){
        if(!$support_unionid):
            return false;
        endif;
        $this->db->from(TBL_ARE);
        $this->db->where('support_unionid',$support_unionid);
        $this->db->where('status',1); //把状态更改为0
        $related=$this->db->get()->result_array();
        $this->downstatus_by_unionid($support_unionid);
        if(is_array($related) && count($related)>0){
            foreach ($related as $onerow) {
                    if($onerow['sponsor_unionid']):
                        $this->down_hongbao_distance($onerow['sponsor_unionid'],$onerow['tinydistance']);
                    endif;
                
            }
            return true;
        }
        return false;
    }



    /**
     * @param $unionid
     * @param $tinydistance
     */
    function update_hongbao_distance($unionid,$tinydistance){
        if(!$unionid) return false;
//        $sql = 'SELECT DISTINCT * FROM '.
//                $this->db->dbprefix(TBL_ARE).' WHERE'.
//                ' (`sponsor_unionid` =  "'.$sponsor_unionid.'" AND `support_unionid` =  "'.$support_unionid.'")'.
//                ' OR (`support_unionid` =  "'.$sponsor_unionid.'" AND `sponsor_unionid` =  "'.$support_unionid.'")';
        $sql = 'SELECT SUM(tinydistance) as S FROM '
                . $this->db->dbprefix(TBL_ALO).' as A LEFT JOIN '
                . $this->db->dbprefix(TBL_ARE).' as B on A.unionid = B.sponsor_unionid '
                . 'left join el_user_social as C on B.support_unionid = C.union_id '
                . 'where A.unionid = "'.$unionid.'" and C.subscribe = 1 AND C.user_id IS NOT NULL';
        
        //SELECT * FROM `el_autumn_location` as A left join el_autumn_relationship as B on A.unionid = B.sponsor_unionid left join el_user_social as C on B.support_unionid = C.union_id where unionid = 'opc8DuOwGzPYe205_QeAQuUjpgj8' and B.status = 1 AND C.user_id IS NOT NULL
        
        //echo $sql;
        $r = $this->db->query($sql)->result_array();
        if ( is_array($r) && count($r) > 0) {
            $total = (int)$r[0]['S'];
        }else{
            $total = 0;
        }
        
        $this->db->from(TBL_AHB);
        $this->db->where('unionid',$unionid);
        $this->db->set('total_distance',$total,FALSE);
        $this->db->update(TBL_AHB);
//        $this->db->from(TBL_AHB);
//        $this->db->where('unionid',$unionid);
//        $newdistance="total_distance+" .$tinydistance;
//        $this->db->set('total_distance',$newdistance,FALSE);
//        $this->db->update(TBL_AHB);
    }

    function down_hongbao_distance($unionid,$tinydistance){
        return $this->update_hongbao_distance($unionid,$tinydistance);
        $this->db->from(TBL_AHB);
        $this->db->where('unionid',$unionid);
        $newdistance="total_distance-" .$tinydistance;
        $this->db->set('total_distance',$newdistance,FALSE);
        $this->db->update(TBL_AHB);
    }



	function create_new_hongbao($hongbaoData){
            if(!$hongbaoData['openid'] || !$hongbaoData['unionid']):
                return false;
            endif;
		$data['openid']=$hongbaoData['openid'];
		$data['unionid']=$hongbaoData['unionid'];
		if($hongbaoData['total_distance']){
			$data['total_distance']=$hongbaoData['total_distance'];
		}
		if ($this->db->insert(TBL_AHB,$data)){
			return $this->db->insert_id();
		}	else {
			return false;
		}
	}
	
	function update_hongbao($hongbaoData){
            if(!$hongbaoData['unionid']):
                return false;
            endif;
		$this->db->from(TBL_AHB);
		$this->db->where('unionid',$hongbaoData['unionid']);
		//($bookData['author'])?$bookData['author']:'未知';
		if(isset($hongbaoData['stepA'])) $data['stepA']=$hongbaoData['stepA'];
		if(isset($hongbaoData['stepB'])) $data['stepB']=$hongbaoData['stepB'];
		if(isset($hongbaoData['stepC'])) $data['stepC']=$hongbaoData['stepC'];
		if(isset($hongbaoData['stepD'])) $data['stepD']=$hongbaoData['stepD'];
		if(isset($hongbaoData['total_distance'])) $data['total_distance']=$hongbaoData['total_distance'];
		if(isset($hongbaoData['used_distance'])) $data['used_distance']=$hongbaoData['used_distance'];
        if(isset($hongbaoData['money'])) $data['money']=$hongbaoData['money'];
		return $this->db->update(TBL_AHB,$data);
	}
	
	function gethongbao_by_unionid($unionid){
            if(!$unionid):
                return false;
            endif;
		$this->db->from(TBL_AHB);
        $data = array(
            'unionid'=>$unionid
        );
        $this->db->where($data);//maybe input username 
        $r = $this->db->get()->result_array();
	if ( is_array($r) && count($r) > 0) {
            return $r[0];
		}
        return false;
	}


    //l领取红包的数据库判断操作
    function exchange_hongbao($unionid,$step){
            if(!$unionid):
                return false;
            endif;
        $this->db->from(TBL_AHB);
        $this->db->where('unionid',$unionid);
        $r=$this->db->get()->result_array();
        if ( is_array($r) && count($r) > 0) {
            $myhongbao=$r[0];
            $dist = $myhongbao['total_distance'];//-$myhongbao['used_distance'];
            switch($step) {
                case 'stepA':
                    if(($myhongbao['stepA']==0) && ($dist>=100)){
                        $hongbaoData['unionid']=$unionid;
                        $hongbaoData['used_distance']=$myhongbao['used_distance']+100;
                        $hongbaoData['stepA']=1;
                        $hongbaoData['money']=$myhongbao['money']+1;
                        $this->update_hongbao($hongbaoData);

                        return true;
                    }

                    break;
                case 'stepB':
                    if(($myhongbao['stepB']==0) && ($dist>=500)){
                        $hongbaoData['unionid']=$unionid;
                        $hongbaoData['used_distance']=$myhongbao['used_distance']+500;
                        $hongbaoData['stepB']=1;
                        $hongbaoData['money']=$myhongbao['money']+2;
                        $this->update_hongbao($hongbaoData);
                        return true;
                    }

                    break;

                case  'stepC':
                    if(($myhongbao['stepC']==0) && ($dist>=2000) && ($this->getfriends_num($unionid)>=5)){
                        $hongbaoData['unionid']=$unionid;
                        $hongbaoData['used_distance']=$myhongbao['used_distance']+2000;
                        $hongbaoData['stepC']=1;
                        $hongbaoData['money']=$myhongbao['money']+5;
                        $this->update_hongbao($hongbaoData);
                        return true;
                    }

                    break;

                case 'stepD':
                    if(($myhongbao['stepD']==0) && ($dist>=10000) && ($this->getfriends_num($unionid)>=7)){
                        $hongbaoData['unionid'] = $unionid;
                        $hongbaoData['used_distance'] = $myhongbao['used_distance'] + 10000;
                        $hongbaoData['stepD'] = 1;
                        $hongbaoData['money']=$myhongbao['money']+10;
                        $this->update_hongbao($hongbaoData);
                        return true;
                    }
                    break;

                default:
                    return false;
                    break;


            }


        }
        return false;
    }

    //更新账户余额，减去取出的钱数
    function out_bank($draw_money){
        $draw_money = (int)$draw_money;
            if(!$draw_money):
                return false;
            endif;
        $this->db->from(TBL_ABK);
        $this->db->where('status',0);
        $newmoney="amount-" .$draw_money;
        $this->db->set('amount',$newmoney,FALSE);
        $this->db->update(TBL_ABK);

    }


    function get_bank(){
        $this->db->from(TBL_ABK);
        $this->db->where('status',0);
        $this->db->order_by('date','desc');
        $r = $this->db->get()->result_array();

        if( is_array($r) && count($r) > 0 ) {
            return $r[0];
        }
        return false;
    }

    function insert_log($moneylog)
    {
        $data['openid'] = $moneylog['openid'];
        $data['money'] = $moneylog['money'];
        if ($this->db->insert(TBL_AML, $data)) {
            return $this->db->insert_id();
        }else{
            return false;
        }
    }

    function draw_money($unionid,&$err=0){
            if(!$unionid):
                return false;
            endif;
        $remain=$this->get_bank();
        $myhongbao=$this->gethongbao_by_unionid($unionid);

        if((($remain['amount']-$myhongbao['money'])>=0) && ($myhongbao['money'] > 0)){
            $moneylog['openid']=$myhongbao['openid'];
            $moneylog['money']=$myhongbao['money'];
            $this->insert_log($moneylog);  //记录发放的红包
            //下面3行为红包发放模块，仅在正式服务器上运行
            if(ENVIRONMENT == 'production'):
                //测试 压缩到1/100
                //$myhongbao['money'] = 1 + ($myhongbao['money']-1)/100;
            
                //covert to 分
                $myhongbao['money'] *= 100;
            
                log_message('DEBUG','xxx draw '."{$myhongbao['openid']},{$myhongbao['money']}");
                $this->load->library('WXHongBao');
                $this->wxhongbao->newhb($myhongbao['openid'],$myhongbao['money']);
                
                $this->wxhongbao->setWishing('有种团圆不畏距离和时差，心在一起，家人就在一起！');
                
                $draw_result = $this->wxhongbao->send();
                if(!$draw_result):
                    $errMsg = $this->wxhongbao->err();
                    $err = 1;
                    log_message('ERROR',"DRAW ERR {$myhongbao['openid']},{$myhongbao['money']} ".  $errMsg);
                endif;
                
            endif;
            //更新余额以及将积累的money重置
            if($draw_result):
                $newhongbao['unionid']=$myhongbao['unionid'];
                $newhongbao['money']=0;
                $this->update_hongbao($newhongbao);

                $this->out_bank($moneylog['money']);
                $err = 0;
                return true;
            else:
                $err = 2;
                return false;
            endif;
        }
        else{
                $err = 2;
            return false;
        }





    }

}
/*
$test=new Autumn_Model();
$LocationData['openid'] = 'of6p1jgJfCl_YmsN-73XgbAlP8Cs';
$LocationData['unionid']='oD7DjjoVU8sXzCZ_OQd7CVf2_Cq0';
$LocationData['latitude']=36.4;
$LocationData['longitude']=124.6;
$myid=$test->create_new_location($locationData);
echo $myid;
 */
?>