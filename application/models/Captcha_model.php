<?php
class Captcha_Model extends CI_Model {
    function check_captcha($captcha){
        
        // First, delete old captchas
        $expiration = time()-7200; // Two hour limit
        $expiration = time()-3600*24*10; // 过去10天的都不清除
        $this->db->query("DELETE FROM ".$this->db->dbprefix(TBL_CAPT)." WHERE `captcha_time` < ".$expiration);	
        //og_message('DEBUG','check captcha:'.$this->db->last_query());

        // Then see if a captcha exists:
        $sql = "SELECT COUNT(*) AS count FROM ".$this->db->dbprefix(TBL_CAPT)." WHERE word = ? AND ip_address = ? AND captcha_time > ?";
        
        //$pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        //$pool = '0123456789abcdefghijk mn pqrstuvwxy ABCDEFGH JKLMN PQRSTUVWXY ';
//        $captcha = str_replace("l", "1", $captcha);
//        $captcha = str_replace("o", "0", $captcha);
//        $captcha = str_replace("z", "2", $captcha);
//        $captcha = str_replace("I", "1", $captcha);
//        $captcha = str_replace("O", "0", $captcha);
//        $captcha = str_replace("Z", "2", $captcha);
                
                
        $binds = array($captcha, $this->input->ip_address(), $expiration);
        $query = $this->db->query($sql, $binds);
        //log_message('DEBUG','check captcha:'.$this->db->last_query());
        $row = $query->row();

        if ($row->count == 0)
        {
            return false;
        }
        return true;
    }
    
    function create_captcha($w=99,$h=44){
//        $this->load->helper('recaptchalib');
//        
//        return ;
        //captcha
        $this->load->helper('captcha');
        $vals = array(
        'word'	 => '',
        'word_length' => 4,
        'font_size' => 24,
        'img_path'	 => './captcha/',
        'img_url'	 => base_url().'/captcha/',
        'font_path'	 => base_url().'/assets/fonts/HelveticaNeueLTPro-Th.ttf',
        'img_width'	 => $w,
        'img_height' => $h,
        'expiration' => 7200
        );
        
        $cap = create_captcha2($vals);
        
        if(!$cap) return false;
        
        if(!$cap['time']) $cap['time'] = time();
        $data = array(
            'captcha_time'	=> $cap['time'],
            'ip_address'	=> $this->input->ip_address(),
            'word'	 => $cap['word']
        );

        $query = $this->db->insert_string('captcha', $data);
        $this->db->query($query);
        
        
        return $cap;
        
        
    }
}
