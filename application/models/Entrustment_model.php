<?php

class Entrustment_Model extends CI_Model {

  function create_new_post($postData) {
    $data['title'] = $postData['title'];
    $data['mode'] = $postData['mode'];
    $data['file'] = $postData['file'];
    $data['content'] = $postData['content'];
    $data['tags'] = $postData['tags'];
    $data['create_date'] = $postData['create_date'];
    $data['update_date'] = $postData['update_date'];
    $data['user_id'] = (int)$postData['user_id'];
    $data['post_id'] = (int)$postData['post_id'];
    $data['author_id'] = (int)$postData['author_id'];

    if ( $this->db->insert(TBL_ENTR,$data) ) {
      return $data;
    } else {
      return false;
    }
  }
  
  function edit_post($postData) {
    $data['title'] = $postData['title'];
    $data['mode'] = $postData['mode'];
    $data['type'] = $postData['type'];
    $data['receivers'] = $postData['receivers'];
    $data['file'] = $postData['file'];
    $data['content'] = $postData['content'];
    $data['tags'] = $postData['tags'];
    $data['deadline'] = $postData['deadline'];
    $data['imme'] = (int)$postData['imme'];

    $this->db->where('id', $postData['id']);
    if ( $this->db->update(TBL_ENTR,$data) ) {
      return $data;
    } else {
      return false;
    }
  }


  function get_post_by_id( $id,$user_id='') {
      $id=(int)$id;
      $user_id=(int)$user_id;
//    if($user_id){
//        $where['user_id']=$user_id;
//    }
        $query = $this->db->get_where(TBL_ENTR, array('id' => $id,'user_id'=>$user_id), 1, 0);
        if ($query->num_rows() > 0) {
            $post = $query->row_array();
            return $post;
        }
        return false;
  }
  
  function get_post_by_postId( $post_id,$user_id) {
      $post_id=(int)$post_id;
      $user_id=(int)$user_id;
        $query = $this->db->get_where(TBL_ENTR, array('post_id' => $post_id,'user_id'=>$user_id), 1, 0);
        if ($query->num_rows() > 0) {
            $post = $query->row_array();
            return $post;
        }
        return false;
  }
  function remove_post_by_postId( $post_id,$user_id) {
      $post_id=(int)$post_id;
      $user_id=(int)$user_id;
      return $this->db->delete(TBL_ENTR, array('post_id' => $post_id,'user_id' => $user_id)); 
  }
  
  function get_posts_for_user( $user_id,$mode=0,$type=0, $num_posts = 10 ,$offset = 0) {

    $this->db->from(TBL_ENTR);
    if($user_id){
        $where['user_id']=$user_id;
    }
    if($mode){
        $where['mode']=$mode;
    }
    if($type){
        $where['type']=$type;
    }
    $this->db->where($where);
    if($num_posts){
        $this->db->limit( $num_posts ,$offset);
    }
    
    $this->db->order_by('create_date','desc');

    $posts = $this->db->get()->result_array();
    if( is_array($posts) && count($posts) > 0 ) {
      return $posts;
    }
    return false;
  }

  function get_paths_for_user( $user_id) {
    $this->db->from(TBL_ENTR);
    if($user_id){
        $where['user_id']=$user_id;
    }
    $this->db->where($where);
    $this->db->select('id, file');
    $posts = $this->db->get()->result_array();
    if( is_array($posts) && count($posts) > 0 ) {
      return $posts;
    }
    return false;
  }
  function update_paths_for_user( $user_id,$data) {
    $this->db->update_batch(TBL_ENTR, $data, 'id'); 
  }
  
  function get_post_count_for_user( $user_id ) {

    $this->db->from(TBL_ENTR);
    $this->db->where( array('user_id'=>$user_id) );

    return $this->db->count_all_results();
  }
  
    
}
