<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Login extends CI_Controller
{
    
    function __construct()
    {
        parent::__construct();
    }

    function login()
    {
        
        // Create an instance of the user model
        $this->load->model('user_model');
        $this->load->model('file_model');

        // Grab the email or username and password from the form POST
        $username = $this->input->post('username');
        $pass  = $this->input->post('password');
        
        //最近10分钟输错3次 则禁止
        if( $username){
            $times = $this->user_model->get_error_times($username,600);
            if($times >= 3){
                ajax_die(ERR_CODE_LOGIN_MORE,$this->lang->line('ERR_CODE_LOGIN_WRONG'));
            }
        }
        
        if( !$username || !$pass) {
            
                ajax_die(ERR_CODE_PARA,$this->lang->line('ERR_CODE_PARA'));
        }
        
            $result = $this->user_model->validate_user($username,$pass,true);
            // If the user is valid, redirect to the main view
            if($result){
                $this->user_model->login_log($username,1);
                //判断是否第一次登录
//                $r = $this->user_model->get_value($this->session->userdata('id'),'LOGGED');
//                if(empty($r)){
//                    $is_first = true;
//                }else{
//                    $is_first = false;
//                }

                $data = $result;
                //$data['is_first'] = $is_first;
                ajax_die(ERR_CODE_SUCCESS,$this->lang->line('ERR_CODE_SUCCESS'),$data);
            }else{
                $this->user_model->login_log($username,2);
                ajax_die(ERR_CODE_LOGIN_WRONG,$this->lang->line('ERR_CODE_LOGIN_WRONG'));
            }
        }
        
        
    //用手机号登录
    function login_mobile(){
        $mobile = $this->input->post('mobile',true);
        $code = $this->input->post('code',true);
        
        if(!$mobile || !$code){
            ajax_die(ERR_CODE_PARA,$this->lang->line('ERR_CODE_PARA'));
        }
        if(!preg_match('/^1\d{10}$/',$mobile)){
            ajax_die(ERR_CODE_INVALID_MOBILE,$this->lang->line('ERR_CODE_INVALID_MOBILE'));
        }
        
        //check code
        $this->load->model('mobile_model');
        $r = $this->mobile_model->get_code_by_mobile($mobile,1800);
        if(empty($r) || ($r['code'] != $code)){
            ajax_die(ERR_CODE_MOBILE_CODE,$this->lang->line('ERR_CODE_MOBILE_CODE'));
        }
        $this->mobile_model->remove($r['id']);
        
        
        $this->load->model('user_model');
        $result = $this->user_model->validate_user_mobile($mobile);
        if($result){
            $this->user_model->login_log($mobile,1);
            $data = $result;
            ajax_die(ERR_CODE_SUCCESS,$this->lang->line('ERR_CODE_SUCCESS'),$data);
        }else{
            $this->user_model->login_log($mobile,2);
            ajax_die(ERR_CODE_LOGIN_WRONG,$this->lang->line('ERR_CODE_LOGIN_WRONG'));
        }
    }
}