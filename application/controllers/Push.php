<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * User
 *
 * This is a Class for user register and login
 * As before login, no API-Key exists, so should not use REST Controller
 * all done with a hardcoded array.
 *
 * @package	Everlive
 * @subpackage	API
 * @category	Controller
 * @author	Xavi Zhou
*/
class Push extends CI_Controller
{
    function __construct() {
        parent::__construct();
        $language = $this->config->item('rest_language');
        if ($language === NULL)
        {
            $language = 'chinese-simplify';
        }
        $this->lang->load('my', $language);
    }
    
    function index(){
        $app_key = 'dd1066407b044738b6479275';
        $master_secret = 'e8cc9a76d5b7a580859bcfa7';
        $para = array(
            'appKey'=>$app_key,
            'masterSecret'=>$master_secret,
            'logFile'=>FCPATH.'uploads/jpush.log'
        );
        $this->load->library('JPush/JPush',$para);
        $register_id = 1;
        // 简单推送示例
        $result = $this->jpush->push()
            ->setPlatform('all')
            ->addAllAudience()
            ->addRegistrationId($register_id)
            ->setNotificationAlert('Hi, JPush')
            ->setMessage("msg content", 'msg title', 'type', array("key1"=>"value1", "key2"=>"value2"))
            ->setOptions(100000, 3600, null, false)
            ->send();

        echo 'Result=' . json_encode($result);

//        // 完整的推送示例,包含指定Platform,指定Alias,Tag,指定iOS,Android notification,指定Message等
//        $result = $client->push()
//            ->setPlatform(array('ios', 'android'))
//            ->addAlias('alias1')
//            ->addTag(array('tag1', 'tag2'))
//            ->setNotificationAlert('Hi, JPush')
//            ->addAndroidNotification('Hi, android notification', 'notification title', 1, array("key1"=>"value1", "key2"=>"value2"))
//            ->addIosNotification("Hi, iOS notification", 'iOS sound', JPush::DISABLE_BADGE, true, 'iOS category', array("key1"=>"value1", "key2"=>"value2"))
//            ->setMessage("msg content", 'msg title', 'type', array("key1"=>"value1", "key2"=>"value2"))
//            ->setOptions(100000, 3600, null, false)
//            ->send();
//        //sendno time_to_live override_msg_id apns_production(true product or false dev) big_push_duration
//        //又名缓慢推送，把原本尽可能快的推送速度，降低下来，给定的n分钟内，均匀地向这次推送的目标用户推送。最大值为1400.未设置则不是定速推送
//        echo 'Result=' . json_encode($result) . $br;


    }
}