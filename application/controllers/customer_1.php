<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Customer extends REST_Controller
{
	function user_get()
    {
        $id = $this->session->userdata('id');
        if(!$id)
        {
        	$this->response(NULL, 400);
        }

        $this->load->model('customer_model');
        $post = $this->customer_model->Get($id);
        

        $data['post'] = $post;
    
        // $user = $this->some_model->getSomething( $this->get('id') );
    	$users = array(
			1 => array('id' => 1, 'name' => 'Some Guy', 'email' => 'example1@example.com', 'fact' => 'Loves swimming'),
			2 => array('id' => 2, 'name' => 'Person Face', 'email' => 'example2@example.com', 'fact' => 'Has a huge face'),
			3 => array('id' => 3, 'name' => 'Scotty', 'email' => 'example3@example.com', 'fact' => 'Is a Scott!', array('hobbies' => array('fartings', 'bikes'))),
		);
		
    	$user = @$users[$this->get('id')];
    	
        if($user)
        {
            $this->response($user, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'User could not be found'), 404);
        }
    }
    
    function user_post()
    {
        //$this->some_model->updateUser( $this->get('id') );
        $email = $this->input->post('email', true);
        $password = $this->input->post('password', true);
        
        $this->load->model('customer_model');
        $r = $this->customer_model->validate_user($email, md5($password));
        
        $message = array('id' => $this->get('id'), 'name' => $this->post('name'), 'email' => $this->post('email'), 'message' => 'ADDED!');
        
        $this->response($message, 200); // 200 being the HTTP response code
    }
    
    
    
    function user_put()
    {
        // 创建一个新用户
        $username = $this->put('username');
        $email = $this->put('email');
        $mobile = $this->put('mobile');
        $password = $this->put('password');
        
        $this->load->model('customer_model');
        
        if($this->customer_model->existing_user( $username )){
            $this->response(array('code' => USER_USERNAME_EXIST,'message'=>'用户名已存在'), 200);
            exit();
        }
        if($this->customer_model->existing_email( $email )){
            $this->response(array('code' => USER_EMAIL_EXIST,'message'=>'邮箱已存在'), 200);
            exit();
        }
        if($this->customer_model->existing_mobile( $mobile )){
            $this->response(array('code' => USER_MOBILE_EXIST,'message'=>'手机号已存在'), 200);
            exit();
        }
        
        $this->load->model('customer_model');
        $saveData = array(
            'username'=>$username,
            'email'=>$email,
            'mobile'=>$mobile,
            'password'=>md5($password)
        );
        
        $id = $this->customer_model->Save($saveData,0);
        
        if($id){
            $this->response(array('code' => NO_ERROR,'message'=>'创建成功','id'=>$id), 200);
            exit();
        }else{
            $this->response(array('code' => USER_CREATE_ERROR,'message'=>'创建失败'), 200);
            exit();
        }
    }
    
    
    function user_delete()
    {
    	//$this->some_model->deletesomething( $this->get('id') );
        $message = array('id' => $this->get('id'), 'message' => 'DELETED!');
        
        $this->response($message, 200); // 200 being the HTTP response code
    }
    
    function users_get()
    {
        //$users = $this->some_model->getSomething( $this->get('limit') );
        $users = array(
			array('id' => 1, 'name' => 'Some Guy', 'email' => 'example1@example.com'),
			array('id' => 2, 'name' => 'Person Face', 'email' => 'example2@example.com'),
			3 => array('id' => 3, 'name' => 'Scotty', 'email' => 'example3@example.com', 'fact' => array('hobbies' => array('fartings', 'bikes'))),
		);
        
        if($users)
        {
            $this->response($users, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array('error' => 'Couldn\'t find any users!'), 404);
        }
    }


	public function send_post()
	{
		var_dump($this->request->body);
	}


	public function send_put()
	{
		var_dump($this->put('foo'));
	}
}