<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * User
 *
 * This is a Class for user register and login
 * As before login, no API-Key exists, so should not use REST Controller
 * all done with a hardcoded array.
 *
 * @package	Everlive
 * @subpackage	API
 * @category	Controller
 * @author	Xavi Zhou
*/
class User extends CI_Controller
{
    private $user;
    private $union_id;
    function __construct() {
        parent::__construct();
        $language = $this->config->item('rest_language');
        if ($language === NULL)
        {
            $language = 'chinese-simplify';
        }
        $this->lang->load('my', $language);
        $this->load->model('user_model');
        $union_id = 'opc8DuOwGzPYe205_QeAQuUjpgj8';
        $union_id = '20006';
        
        $this->user = $this->user_model->get_user_by_union_id($union_id);
        $this->union_id = $union_id;
    }
    function index(){
        $invite_code = $this->input->get('invite_code');
        if(ENVIRONMENT == 'development'):
            $unoin_id= 'of6p1jlbntj3bBQb7TpcCvg-F9n4';
        endif;
        $id = $this->chk_code($invite_code);
        if(!$id):
            redirect('weixin/user/error');
        endif;
        $data['invite_code'] = $invite_code;
        if($this->user['mobile']):
            $this->load->view('user_invite_has_mobile',$data);
        else:
            $this->load->view('user_invite',$data);
        endif;
    }
    
    function accept(){
        $invite_code = $this->input->post('invite_code');
        if(ENVIRONMENT == 'development'):
            $unoin_id= 'of6p1jlbntj3bBQb7TpcCvg-F9n4';
        endif;
        $this->load->model('user_model');
        $id = $this->chk_code($invite_code);
        if(!$id):
            redirect('weixin/user/error');
        endif;
        $cid = $id;
        
        if($this->user['mobile']):
            $user_id = $this->user['id'];
        else:
            //try to bind mobile
            $mobile = $this->input->post('mobile');
            $r = $this->user_model->get_user_by_mobile($mobile);
            if($r):
                redirect('weixin/user/error2');
            endif;
            
            $userData = array('mobile'=>$mobile);
            if($r['system_account']):
                $userData['username'] = $mobile;
            endif;

            
            if($this->user['system_account']){
                $temp_user = true;
            }elseif((strlen($this->user['username']) > 4) && (substr($this->user['username'], 0, 4) == 'eeev')) {
                $temp_user = true;
            }else{
                $temp_user = false;
            }
            $user = $this->user;
            $userData['birthdate'] = $user['birthdate'];
            $username = $r['username'];
            $path_old = FILE_FOLDER.$username[0].'/'.$username.'/';
            $user_id = $this->user['id'];
            
            if(!$temp_user):
                //formal user update mobile
                $this->user_model->update_mobile($mobile,$user_id);
            else:
                //temp user update mobile & username
                $username = $mobile;
                $userData['username'] = $mobile;
                if ($this->user_model->user_existing($username)) {
                    redirect('weixin/user/error2');
                }
                $userData['id'] = $user_id;
                $userData['password'] = rand(100000,999999);
                $userData['pw_sec'] = 1;

                $path_new = FILE_FOLDER.$username[0].'/'.$username.'/';
                $saved = $this->user_model->update_temp_account($userData);
                $this->user_model->update_mobile($mobile,$user_id);
                if($saved){
                    $path1 = 'uploads/'.$username[0].'/';
                    $path2 = 'uploads/'.$username[0].'/'.$username.'/';
                    if(!file_exists($path1)){
                        mkdir($path1, 0777, true);
                    }
                    if(!file_exists($path2)){
                        mkdir($path2, 0777, true);
                    }
                    rename($path_old,$path_new);
                    //$this->user_model->refresh_session($user_id);
                }else{
                    redirect('weixin/user/error2');
                }
            endif;
        endif;
        
        //这里要增加 是否是 自己的判断
        
        $this->load->model('forest_model');
        if($this->forest_model->is_tree($user_id,$cid)):
            redirect('weixin/user/success');
        endif;
        
        $r = $this->forest_model->get_owner($cid);
        $owner_id = $r['user_id'];
        $owner = $this->user_model->get_user_by_id($owner_id);
        
        $userData = array();
        $userData['user_id'] = $user_id ;
        $userData['cid'] = $cid;
        $userData['label'] = ($owner['display_name'])?$owner['display_name']:'TA'.'的家';
        $userData['is_owner'] = 0;
        $userData['is_main'] = 0;
        $userData['relative'] = REL_OTHE;
        $userData['relative2'] = REL_OTHE;
        
        $this->forest_model->create_tree($userData);
            redirect('weixin/user/success');
    }
    
    function chk_code($invite_code){
        
        $_pre = substr($invite_code, 0,3);
        $_id = substr($invite_code, 3,  strlen($invite_code) - 6);
        $_app = substr($invite_code, strlen($invite_code) - 3);

        if(!$_pre || !$_id || !$_app):
            return false;
        endif;
        if($_id % 3 != 0):
            return false;
        endif;
        
        $this->load->model('user_model');
        $r = $this->user_model->get_user_by_id($_id/3);
        $t = strtotime($r['create_date']);
        $_invite_code = str_pad($t % 666,3,'0',STR_PAD_LEFT).($_id).str_pad($t % 1000,3,'0',STR_PAD_LEFT);
        
        
        if($_invite_code != $invite_code):
            return false;
        endif;
        
        return $_id/3;
    }
    
    function error(){
        echo 'wrong link';
    }
    function error2(){
        echo 'mobile exist';
    }
    function success(){
        echo 'add success';
    }
}