<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Traveller extends REST_Controller
{
	function user_get()
    {
        $cutomer_id = $this->session->userdata('id');
        if(!$cutomer_id)
        {
            $this->response(array('code' => NO_AUTH,'message'=>'请登录'), 200);
        }
        $id = $this->get('id');

        $this->load->model('traveller_model');
        $condition[] = array('where','customer_id',$cutomer_id);
        $condition[] = array('where','id',$id);
        $posts = $this->traveller_model->Search($total,0,0,$condition);
        
        if($posts){
            $this->response(array('code' => NO_ERROR,'data'=>$posts[0],'message'=>'操作成功'), 200);
        }else{
            $this->response(array('code' => NO_RECORD,'data'=>array(),'message'=>'没有记录'), 200);
        }
        
    }
    
    function user_post()
    {
        //$this->some_model->updateUser( $this->get('id') );
        $cutomer_id = $this->session->userdata('id');
        if(!$cutomer_id)
        {
            $this->response(array('code' => NO_AUTH,'message'=>'请登录'), 200);
        }
        $id = $this->get('id');

        $this->load->model('traveller_model');
        $condition[] = array('where','customer_id',$cutomer_id);
        $condition[] = array('where','id',$id);
        $posts = $this->traveller_model->Search($total,0,0,$condition);
        
        if($posts){
            
        }else{
            $this->response(array('code' => NO_RECORD,'data'=>array(),'message'=>'没有记录'), 200);
        }
        
        $saveData = array();        
        $para = array('name','firstname','middlename','surname','email','gender','birthdate','mobile','tel','city','country','id_type','id_no','issue_place','issue_date','expiry_date','child','status');
        foreach($para as $v):
            $saveData[$v] = $this->post($v);
        endforeach;
        $saveData['customer_id'] = $cutomer_id;
        
        $id = $this->traveller_model->Save($saveData,$id);
        $data = $this->traveller_model->Get($id);
        $this->response(array('code' => NO_ERROR,'data'=>$data,'message'=>'操作成功'), 200);
    }
    
    
    
    function user_put()
    {
        $cutomer_id = $this->session->userdata('id');
        // 创建一个新用户
        $this->load->model('traveller_model');
        $saveData = array();        
        $para = array('name','firstname','middlename','surname','email','gender','birthdate','mobile','tel','city','country','id_type','id_no','issue_place','issue_date','expiry_date','child','status');
        foreach($para as $v):
            $saveData[$v] = $this->put($v);
        endforeach;
        $saveData['customer_id'] = $cutomer_id;
        
        $id = $this->traveller_model->Save($saveData,0);
        
        
        if($id){
            $post = $this->traveller_model->Get($id);
            $this->response(array('code' => NO_ERROR,'message'=>'创建成功','data'=>$post), 200);
            exit();
        }else{
            $this->response(array('code' => USER_CREATE_ERROR,'message'=>'创建失败'), 200);
            exit();
        }
    }
    
    
    function user_delete()
    {
        $cutomer_id = $this->session->userdata('id');
        if(!$cutomer_id)
        {
            $this->response(array('code' => NO_AUTH,'message'=>'请登录'), 200);
        }
        $this->load->model('traveller_model');
        
        $id = $this->get('id');

        $this->load->model('traveller_model');
        $condition[] = array('where','customer_id',$cutomer_id);
        $condition[] = array('where','id',$id);
        $posts = $this->traveller_model->Search($total,0,0,$condition);
        if($posts){
            $this->traveller_model->Remove($id);
            $this->response(array('code' => NO_ERROR,'message'=>'删除成功'), 200);
        }else{
            $this->response(array('code' => NO_RECORD,'data'=>array(),'message'=>'没有记录'), 200);
        }
    }
    
    function users_get()
    {
        $cutomer_id = $this->session->userdata('id');
        if(!$cutomer_id)
        {
            $this->response(array('code' => NO_AUTH,'message'=>'请登录'), 200);
        }
        
        $this->load->model('traveller_model');
        $condition[] = array('where','customer_id',$cutomer_id);
        $posts = $this->traveller_model->Search($total,0,0,$condition);
        
        $this->response(array('code' => NO_ERROR,'data'=>$posts,'message'=>'操作成功'), 200);
    }


	public function send_post()
	{
		var_dump($this->request->body);
	}


	public function send_put()
	{
		var_dump($this->put('foo'));
	}
}