<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
//require APPPATH . '/libraries/MY_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package	Everlive
 * @subpackage	API
 * @category	Family
 * @author	Xavi Zhou
 */
class Family extends MY_Controller {

    function __construct()
    {
        parent::__construct();
    }
    
    //初始化一个家庭,创建元节点,没有关系
    public function init_put(){
        $user_id = $this->rest->user->id;
        $name = trim($this->put('name'));
        $tree_id = (int)$this->put('tree_id');
        $gender = (int)$this->put('gender');
        
        if(!$name || !$tree_id):
            $this->response(rest_message('ERR_CODE_PARA'), MY_Controller::HTTP_OK);
        endif;
        //chk if correct tree
        $this->load->model('forest_model');
        $r = $this->forest_model->is_tree($user_id,$tree_id);
        if(!$r):
            $this->response(rest_message('ERR_CODE_FORBIDDEN'), MY_Controller::HTTP_OK);
        endif;
        
        //chk if inited
        $this->load->model('family_model');
        $r = $this->family_model->get_nodes_by_tree_id($tree_id);
        if($r):
            $this->response(rest_message('ERR_CODE_FAMILY_TREE_EXIST'), MY_Controller::HTTP_OK);
        endif;
        $familyData = array(
            'tree_id'=>$tree_id,
            'user_id'=>$user_id,
            'gender'=>$gender,
            'name'=>$name
        );
        
        $r = $this->family_model->create_new($familyData);
        if(!$r):
            $this->response(rest_message('ERR_CODE_DB_FAILURE'), MY_Controller::HTTP_OK);
        endif;
        $data = array('node_id'=>$r);
        $this->response(rest_message('ERR_CODE_SUCCESS',$data), MY_Controller::HTTP_OK);
        
    }
    
    //创建新节点,并绑定关系
    public function index_put()
    {
        $user_id = $this->rest->user->id;
        $name = trim($this->put('name'));
        $tree_id = (int)$this->put('tree_id');
        //$gender = (int)$this->put('gender');
        $node_id = (int)$this->put('node_id');
        $relative = (int)$this->put('relative');
        
        if(!$name || !$tree_id || !$node_id || !$relative):
            $this->response(rest_message('ERR_CODE_PARA'), MY_Controller::HTTP_OK);
        endif;
        
        //chk if correct tree
        $this->load->model('forest_model');
        $r = $this->forest_model->is_tree($user_id,$tree_id);
        if(!$r):
            $this->response(rest_message('ERR_CODE_FORBIDDEN'), MY_Controller::HTTP_OK);
        endif;
        
        $this->load->model('family_model');
        //chk if correct node
        $r = $this->family_model->chk_node($node_id,$tree_id);
        if(!$r):
            $this->response(rest_message('ERR_CODE_FORBIDDEN'), MY_Controller::HTTP_OK);
        endif;
        
        
        
        $unique = false;
        switch ($relative):
            case REL_FATH:
                $gender = GENDER_M;
                $unique = true;
                break;
            case REL_MOTH:
                $gender = GENDER_F;
                $unique = true;
                break;
            case REL_SPOU:
                $gender = ($this->rest->user->gender == GENDER_M)?GENDER_F:GENDER_M;
                $unique = true;
                break;
            case REL_SON:
                $gender = GENDER_M;
                break;
            case REL_DAUG:
                $gender = GENDER_F;
                break;
            case REL_BROT:
                $gender = GENDER_M;
                break;
            case REL_SIST:
                $gender = GENDER_M;
                break;
            default :
                $this->response(rest_message('ERR_CODE_FAMILY_RELATIVE_INVALID'), MY_Controller::HTTP_OK);
                break;
        endswitch;
        
        
        //chk if unique
        if($unique):
            $r = $this->family_model->chk_unique($node_id,$relative);
            if($r):
                $this->response(rest_message('ERR_CODE_UNIQUE'), MY_Controller::HTTP_OK);
            endif;
        endif;
        
        //create note 
        $familyData = array(
            'tree_id'=>$tree_id,
            'gender'=>$gender,
            'name'=>$name
        );
        $id = $this->family_model->create_new($familyData);
        if(!$id):
            $this->response(rest_message('ERR_CODE_DB_FAILURE'), MY_Controller::HTTP_OK);
        endif;
        
        $this->config->load('email', TRUE);
        $relative_mappding = $this->config->item('relative_mappding', 'email');
        
                    
        $relativeData = array(
            'id1'=>$node_id,
            'id2'=>$id,
            'relative1'=>$relative_mappding[$this->rest->user->gender][$relative],
            'relative2'=>$relative
        );
        $this->family_model->add_relative($relativeData);
        
        $this->add_relative_relationship($relative,$node_id,$id);
        
        $this->response(rest_message('ERR_CODE_SUCCESS'), MY_Controller::HTTP_OK);
        //$this->set_response($message, MY_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }
//    function test_get(){
//        $this->add_relative_relationship(REL_FATH,1,15);
//    }
    private function add_relative_relationship($relative,$node_id,$new_id){
        $this->load->model('family_model');
        $nodes = $this->family_model->get_nodes($node_id);
        if(!$nodes) return;
        $nodes_new = array();
        //chk other relationship
        switch ($relative):
            case REL_FATH:
                $nodes_new = $this->deal_relationship_father($nodes);
                break;
            case REL_MOTH:
                $nodes_new = $this->deal_relationship_mother($nodes);
                break;
            case REL_SPOU:
                $nodes_new = $this->deal_relationship_spouse($nodes);
                break;
            case REL_SON:
            case REL_DAUG:
                $nodes_new = $this->deal_relationship_children($nodes,$relative,$new_id);
                break;
            case REL_BROT:
            case REL_SIST:
                $nodes_new = $this->deal_relationship_bro_and_sis($nodes,$relative,$new_id);
                break;
        endswitch;
        if(!$nodes_new) return;
        $this->family_model->add_relative_batch($nodes_new,$new_id);
        
    }
    
    private function deal_relationship_father($nodes){
        $nodes_new = array();
        //chk parents & brothers & sisters
        foreach($nodes as $v):
            switch ($v['relative']):
                case REL_MOTH:
                    $nodes_new[] = array(
                        'id2'=>$v['id'],
                        'relative2'=>REL_SPOU,
                        'relative1'=>REL_SPOU,
                    );
                    break;
                case REL_BROT:
                    $nodes_new[] = array(
                        'id2'=>$v['id'],
                        'relative2'=>REL_SON,
                        'relative1'=>REL_FATH,
                    );
                    break;
                case REL_SIST:
                    $nodes_new[] = array(
                        'id2'=>$v['id'],
                        'relative2'=>REL_DAUG,
                        'relative1'=>REL_FATH,
                    );
                    break;
            endswitch;
        endforeach;
        return $nodes_new;
    }
    
    private function deal_relationship_mother($nodes){
        $nodes_new = array();
        //chk parents & brothers & sisters
        foreach($nodes as $v):
            switch ($v['relative']):
                case REL_FATH:
                    $nodes_new[] = array(
                        'id2'=>$v['id'],
                        'relative2'=>REL_SPOU,
                        'relative1'=>REL_SPOU,
                    );
                    break;
                case REL_BROT:
                    $nodes_new[] = array(
                        'id2'=>$v['id'],
                        'relative2'=>REL_SON,
                        'relative1'=>REL_MOTH,
                    );
                    break;
                case REL_SIST:
                    $nodes_new[] = array(
                        'id2'=>$v['id'],
                        'relative2'=>REL_DAUG,
                        'relative1'=>REL_MOTH,
                    );
                    break;
            endswitch;
        endforeach;
        return $nodes_new;
    }
    private function deal_relationship_spouse($nodes){
        $nodes_new = array();
        //chk children
        $relative = ($this->rest->user->gender == GENDER_M)?REL_MOTH:REL_FATH;
        foreach($nodes as $v):
            switch ($v['relative']):
                case REL_SON:
                case REL_DAUG:
                    $nodes_new[] = array(
                        'id2'=>$v['id'],
                        'relative2'=>$v['relative'],
                        'relative1'=>$relative,
                    );
                    break;
            endswitch;
        endforeach;
        return $nodes_new;
    }
    private function deal_relationship_bro_and_sis($nodes,$relative,$new_id){
        $nodes_new = array();
        //chk parents & brothers & sisters
        foreach($nodes as $v):
            if($v['id'] == $new_id):
                continue;;
            endif;
            switch ($v['relative']):
                case REL_BROT:
                case REL_SIST:
                    $nodes_new[] = array(
                        'id2'=>$v['id'],
                        'relative2'=>$v['relative'],
                        'relative1'=>$relative,
                    );
                    break;
                case REL_FATH:
                case REL_MOTH:
                    $relative1 = ($relative == REL_BROT)?REL_SON:REL_DAUG;
                    $nodes_new[] = array(
                        'id2'=>$v['id'],
                        'relative2'=>$v['relative'],
                        'relative1'=>$relative1,
                    );
                    break;
            endswitch;
        endforeach;
        return $nodes_new;
    }
    
    private function deal_relationship_children($nodes,$relative,$new_id){
        $nodes_new = array();
        //chk spouse & children
        $relative1=($relative==REL_SON)?REL_BROT:REL_SIST;
        foreach($nodes as $v):
            if($v['id'] == $new_id):
                continue;
            endif;
            switch ($v['relative']):
                case REL_SON:
                    $nodes_new[] = array(
                        'id2'=>$v['id'],
                        'relative2'=>REL_BROT,
                        'relative1'=>$relative1,
                    );
                    break;
                case REL_DAUG:
                    $nodes_new[] = array(
                        'id2'=>$v['id'],
                        'relative2'=>REL_SIST,
                        'relative1'=>$relative1,
                    );
                    break;
                case REL_SPOU:
                    $relative2 = ($this->rest->user->gender == GENDER_M)?REL_MOTH:REL_FATH;
                    $nodes_new[] = array(
                        'id2'=>$v['id'],
                        'relative2'=>$relative2,
                        'relative1'=>$relative,
                    );
                    break;
            endswitch;
        endforeach;
        return $nodes_new;
    }
                
    
    
    //修改
    public function index_post()
    {
        $user_id = $this->rest->user->id;
        $name = trim($this->post('name'));
        $avatar = trim($this->post('avatar'));
        $id =  (int) $this->get('id');
        
        if(!$name || !$id):
            $this->response(rest_message('ERR_CODE_PARA'), MY_Controller::HTTP_OK);
        endif;
        //chk if correct tree
        $this->load->model('post_model');
        $r = $this->post_model->is_my_category($user_id,$id);
        if(!$r):
            $this->response(rest_message('ERR_CODE_FORBIDDEN'), MY_Controller::HTTP_OK);
        else:
            $tree_id = $r['tree_id'];
        endif;
        
        $cateData['id'] = $id;
        $cateData['name'] = $name;
        if($date && ($r = strtotime($date))):
            $cateData['date'] = date('Y-m-d',$r);
        endif;
        
        $r = $this->post_model->update_subcategory($cateData);
        if(!$r):
            $this->response(rest_message('ERR_CODE_DB_FAILURE'), MY_Controller::HTTP_OK);
        endif;
        $this->create_file($tree_id);
        $this->response(rest_message('ERR_CODE_SUCCESS'), MY_Controller::HTTP_OK);
    }
    
    
    public function index_get()
    {
        $user_id = $this->rest->user->id;
        $tree_id =  (int) $this->get('tree_id');
        $node_id =  (int) $this->get('node_id');
        
        if(!$tree_id):
            $this->response(rest_message('ERR_CODE_PARA'), MY_Controller::HTTP_OK);
        endif;
        
        //chk if correct tree
        $this->load->model('forest_model');
        $r = $this->forest_model->is_tree($user_id,$tree_id);
        if(!$r):
            $this->response(rest_message('ERR_CODE_FORBIDDEN'), MY_Controller::HTTP_OK);
        endif;

        $this->load->model('family_model');
        if(!$node_id):
            //get my node
            $r = $this->family_model->get_node_by_user_id($tree_id,$user_id);
            if($r):
                $node_id = $r['id'];
                $name = $r['name'];
            endif;
            
            //if my node not exist, try to get root
            if(!$node_id):
                $r = $this->family_model->get_root($tree_id);
                if($r):
                    $node_id = $r['id'];
                    $name = $r['name'];
                endif;
            endif;
            
            //if no node on the tree, the return err
            if(!$node_id):
                $this->response(rest_message('ERR_CODE_FAMILY_TREE_NOT_EXIST'), MY_Controller::HTTP_NOT_FOUND);
            endif;
        else:
            //chk if correct node
            $r = $this->family_model->chk_node($node_id,$tree_id);
            if(!$r):
                $this->response(rest_message('ERR_CODE_FORBIDDEN'), MY_Controller::HTTP_OK);
            else:
                $name = $r['name'];
            endif;
        endif;
        
        $r = $this->family_model->get_nodes_by_node_id($node_id);
        $data['node_id'] = $node_id;
        $data['name'] = $name;
        if($r):
            $data['nodes'] = $r;
        else:
            $data['nodes']= '';
        endif;
        $this->response(rest_message('ERR_CODE_SUCCESS',$data), MY_Controller::HTTP_OK);
    }
    
    
}
