<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * User
 *
 * This is a Class for user register and login
 * As before login, no API-Key exists, so should not use REST Controller
 * all done with a hardcoded array.
 *
 * @package	Everlive
 * @subpackage	API
 * @category	Controller
 * @author	Xavi Zhou
*/
class User extends CI_Controller
{
    function __construct() {
        parent::__construct();
        $language = $this->config->item('rest_language');
        if ($language === NULL)
        {
            $language = 'chinese-simplify';
        }
        $this->lang->load('my', $language);
    }
    
    function register()
    {
        // 创建一个新用户
        $type = (int)$this->input->post('type');
        switch($type):
            case LOGIN_TYPE_WECHAT:
                return $this->register_wechat();
            case LOGIN_TYPE_QQ:
                return $this->register_qq();
            case LOGIN_TYPE_WEIBO:
                return $this->register_weibo();
            case LOGIN_TYPE_MOBILE:
                return $this->register_mobile();
            default:
                $type = LOGIN_TYPE_NORMAL;
                return $this->register_normal();
        endswitch;
    }
    
    private function register_qq(){
        ajax_die(ERR_CODE_SUCCESS,$this->lang->line('ERR_CODE_SUCCESS'));
    }
    private function register_weibo(){
        ajax_die(ERR_CODE_SUCCESS,$this->lang->line('ERR_CODE_SUCCESS'));
    }
    
    
    //使用用户名密码注册
    private function register_normal(){
        $userData = $this->input->post(null,true);
        if( count($userData) ) {
          $this->load->model('user_model');
        } else{
            ajax_die(ERR_CODE_REGISTER,$this->lang->line('ERR_CODE_REGISTER'));
        }         
        $error = '';
        if(!$this->user_model->check_user($userData,$error)){
            ajax_die(ERR_CODE_REGISTER, implode(",'", $error));
        }
        
        $username = $this->input->post('username',true);
        if ($this->user_model->user_existing($username)) {
            ajax_die(ERR_CODE_USERNAME_EXIST,$this->lang->line('ERR_CODE_USERNAME_EXIST'));
        }
        $userData['avatar'] = 'avatar.jpg';
        if(empty($userData['display_name'])){
            $userData['display_name'] = $userData['username'];
        }
        $userData['avatar'] = '';
        //print_r($userData);die();
        
        $user = $this->user_model->register($userData);
        $path1 = FILE_FOLDER.$username[0].'/';
        $path2 = FILE_FOLDER.$username[0].'/'.$username.'/';
        //$avatar = (($user['gender']===GENDER_M)?'M':'F').rand(1, 8).'.png';
        
        
        if ( !$user ) {
            ajax_die(ERR_CODE_REGISTER_ERR,$this->lang->line('ERR_CODE_REGISTER_ERR'));
        }

        if(!file_exists($path1)){
            mkdir($path1, 0777, true);
        }
        if(!file_exists($path2)){
            mkdir($path2, 0777, true);
        }
        
        //是否被邀请种树
        $user_id = $user['id'];
        if(isset($userData['tree_code']) && $userData['tree_code']):
            $r = $this->subuser_create_by_code($user_id,$userData['tree_code']);
        else:
            $r = 0;
        endif;
        
        //如果不是被邀请,生成一棵 家庭树,关联案例文章
        if(!$r):
            $tree_id = $this->subuser_create2($user_id,$username);
        endif;
        $data = array(
            'user_id'=>$user['id'],
            'display_name'=>$user['display_name'],
            'family_id'=>$tree_id,
            'key'=>$user['key']
        );
        ajax_die(ERR_CODE_SUCCESS,$this->lang->line('ERR_CODE_SUCCESS'),$data);
    }
    
    private function subuser_create2($user_id,$username){
        //create family tree
        $userData = array(
            'username'=>$username.'_family',
            'display_name'=>'我的家',
            'label'=>'我的家',
            'relative'=>REL_OTHE,
            'gender'=>GENDER_N,
            'birthdate'=>date('Y-m-d'),
            'is_main'=>1,
        );
        $tree_id = $this->subuser_create($user_id,$userData);

        //add sample article
        $this->load->model('post_model');
        $this->config->load('post', TRUE);
        $post_admin = $this->config->item('post_admin', 'post');                
        $sample_post = $post_admin['sample_post_id'];
        $this->post_model->copy_post($sample_post,$tree_id);
        //add sample article
            
        return $tree_id;
    }
    
    
    private function subuser_create($uid,$user){
        $relative = $user['relative'];
        $username = $user['username'];
        if( !count($user) ) {
            return false;
        }
        
        $this->load->model('user_model');
        $user['agree'] = 1;
        $user['status'] = STATUS_GROW;
        $user['password'] = chr(rand(97,122)).chr(rand(97,122)).chr(rand(97,122)).chr(rand(97,122)).rand(0,9).rand(0,9).rand(0,9).rand(0,9);//only for check
          
        $path1 = 'uploads/'.$username[0].'/';
        $path2 = 'uploads/'.$username[0].'/'.$username.'/';
        $rand = rand(1,10);
        $avatar = $rand.'.jpg';
        $user['avatar'] = $avatar;

        $saved = $this->user_model->register($user);

        if ( isset($saved) && $saved ) {
            if(!file_exists($path1)){
                mkdir($path1, 0777, true);
            }
            if(!file_exists($path2)){
                mkdir($path2, 0777, true);
            }

            //add by xavi
            //sub user only have oss tree avatar
            //can't change in the future
            $avatar = oss_url().'/avatar/'.$rand.'.jpg';
            $user['avatar'] = $avatar;
            //add by xavi

            $cid = $saved['id'];

            /*设定两个用户的关系*/
            $this->load->model('forest_model');
            $userData = array();
            $userData['user_id'] = $uid ;
            $userData['cid'] = $cid;
            $userData['label'] = $user['label'];
            $userData['is_owner'] = 1;
            $userData['is_main'] = $user['is_main'];
            $userData['relative'] = $relative;
//            $gender = GENDER_N;
//            $this->config->load('email', TRUE);
//            $relative_mappding = $this->config->item('relative_mappding', 'email');
//
//            $relative2 = $relative_mappding[$gender][$relative];
//            $userData['relative2'] = $relative2;
            $userData['relative2'] = $relative;

            $this->forest_model->create_tree($userData);
            return $cid;
        }else{
            return false;
        }
        return true;
    }
    
    private function subuser_create_by_code($user_id,$code){
            $this->load->model('forest_model');
            $r = $this->forest_model->code_valid($code);
            if(!$r):
                return false;
            endif;
            $trees = $this->forest_model->get_user_by_cid($r['cid']);
            
            $owner = array();
            foreach($trees as $tree):
                if($tree['is_owner']):
                    $owner = $tree;
                    break;
                endif;
            endforeach;
            
            if(empty($owner)):
                $owner = $trees[0];
            endif;
            
            if($r){
                $userData = array();
                $userData['user_id'] = $user_id ;
                $userData['cid'] = $r['cid'];
                $userData['is_owner'] = 0;
                $userData['is_main'] = 1;
                $userData['label'] = $owner['label'];
                $userData['relative2'] = $r['relative'];
                
                $subuser = $this->user_model->get_user_by_id($r['cid']);
                $this->config->load('email', TRUE);
                $relative_mappding = $this->config->item('relative_mappding', 'email');
                $relative = $relative_mappding[$subuser['gender']][$r['relative']];
                
                $userData['relative'] = $relative;
                //$userData['relative'] = $r['relative'];
                
                $this->forest_model->create_tree($userData);
                $this->forest_model->code_used($code);
                return true;
            }else{
                return false;
            }
    }
    
   
   
    
    
    //要求绑定手机
    function request_mobile_code(){
        $mobile =  $this->input->post('mobile',true);
        if(!$mobile || !preg_match('/^1\d{10}$/',$mobile)){
            ajax_die(ERR_CODE_INVALID_MOBILE,$this->lang->line('ERR_CODE_INVALID_MOBILE'));
        }
        $this->load->model('mobile_model');
        
        //请勿频繁发送
        $r = $this->mobile_model->get_code_by_mobile($mobile,60);
        if(!empty($r)){
            ajax_die(ERR_CODE_FREQUENCY,$this->lang->line('ERR_CODE_FREQUENCY'));
        }
        
        //请勿频繁发送 via ip
        $ip = GetClientIP();
        $r = $this->mobile_model->get_num_by_ip($mobile,60);
        if($r > 3){
            ajax_die(ERR_CODE_FREQUENCY,$this->lang->line('ERR_CODE_FREQUENCY'));
        }
        
        //生成验证码
        $code = rand(1000,9999);
        $data = array(
            'mobile'=>$mobile,
            'ip'=>$ip,
            'code'=>$code
        );
        $this->mobile_model->create_code_by_mobile($data);
        
        $this->load->library('alidayu/alidayu');
        

        //$r = $this->alidayu->send($code,$mobile);
        //$content = sprintf(_("您申请绑定手机，验证码为%s(请勿向任何人提供此短信验证码)，请在30分钟内输入完成绑定"),$code);
        $r = true;
        if($r == false){
            $arr = array (
                'status'=>'failure',
                'message'=>_('短信发送失败，请稍后再试')
            );
        }else{
            $arr = array (
                'status'=>'success',
                'message'=>_('短信发送成功，请查收'),
                'code'=>$code
            );
        }
        echo json_encode($arr);
    }
    
    //用手机号注册
    private function register_mobile(){
        $mobile = $this->input->post('mobile',true);
        $code = $this->input->post('code',true);
        
        if(!$mobile || !$code){
            ajax_die(ERR_CODE_PARA,$this->lang->line('ERR_CODE_PARA'));
        }
        if(!preg_match('/^1\d{10}$/',$mobile)){
            ajax_die(ERR_CODE_INVALID_MOBILE,$this->lang->line('ERR_CODE_INVALID_MOBILE'));
        }
        
        //check code
        $this->load->model('mobile_model');
        $r = $this->mobile_model->get_code_by_mobile($mobile,1800);
        if(empty($r) || ($r['code'] != $code)){
            ajax_die(ERR_CODE_MOBILE_CODE,$this->lang->line('ERR_CODE_MOBILE_CODE'));
        }
        $this->mobile_model->remove($r['id']);
        
        
        //创建用户
        $this->load->model('user_model');
        $username = $mobile;
        if ($this->user_model->user_existing($username)) {
            ajax_die(ERR_CODE_MOBILE_EXIST,$this->lang->line('ERR_CODE_MOBILE_EXIST'));
        }
        $userData['avatar'] = '';
        $userData = array(
            'username'=>$username,
            'mobile'=>$mobile
        );
        $user = $this->user_model->register($userData);
        $path1 = FILE_FOLDER.$username[0].'/';
        $path2 = FILE_FOLDER.$username[0].'/'.$username.'/';
        
        
        if ( !$user ) {
            ajax_die(ERR_CODE_REGISTER_ERR,$this->lang->line('ERR_CODE_REGISTER_ERR'));
        }

        if(!file_exists($path1)){
            mkdir($path1, 0777, true);
        }
        if(!file_exists($path2)){
            mkdir($path2, 0777, true);
        }
        
        //是否被邀请种树
        $user_id = $user['id'];
        if(isset($userData['tree_code']) && $userData['tree_code']):
            $r = $this->subuser_create_by_code($user_id,$userData['tree_code']);
        else:
            $r = 0;
        endif;
        
        //如果不是被邀请,生成一棵 家庭树,关联案例文章
        if(!$r):
            $tree_id = $this->subuser_create2($user_id,$username);
        endif;
        $data = array(
            'user_id'=>$user['id'],
            'display_name'=>$user['display_name'],
            'family_id'=>$tree_id,
            'key'=>$user['key']
        );
        ajax_die(ERR_CODE_SUCCESS,$this->lang->line('ERR_CODE_SUCCESS'),$data);
    }
}