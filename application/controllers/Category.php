<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
//require APPPATH . '/libraries/MY_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package	Everlive
 * @subpackage	API
 * @category	Category
 * @author	Xavi Zhou
 */
class Category extends MY_Controller {

    function __construct()
    {
        parent::__construct();
    }
    
    public function index_put()
    {
        $user_id = $this->rest->user->id;
        $name = trim($this->put('name'));
        $tree_id = (int)$this->put('tree_id');
        $cate_id = (int)$this->put('cate_id');
        $date = $this->put('date');
        $ext = $this->put('ext');
        
        
        if(!$name || !$tree_id || !$cate_id):
            $this->response(rest_message('ERR_CODE_PARA'), MY_Controller::HTTP_OK);
        endif;

        //chk if correct category
        $this->config->load('post', TRUE);
        $category = $this->config->item('category', 'post');       
        if(!isset($category[$cate_id])):
            $this->response(rest_message('ERR_CODE_CATEGORY_INVALID'), MY_Controller::HTTP_OK);
        endif;
        
        
        //chk if correct tree
        $this->load->model('forest_model');
        $r = $this->forest_model->is_tree($user_id,$tree_id);
        if(!$r):
            $this->response(rest_message('ERR_CODE_CATEGORY_INVALID'), MY_Controller::HTTP_OK);
        endif;
        
        if($date && ($r = strtotime($date))):
            $date = date('Y-m-d',$r);
        else:
            $date = GetCurrentDate();
        endif;
        
        $cateData = array(
            'tree_id'=>$tree_id,
            'user_id'=>$user_id,
            'cate_id'=>$cate_id,
            'name'=>$name,
            'date'=>$date,
            'ext'=>$ext
        );
        
        $this->load->model('post_model');
        $r = $this->post_model->add_subcategory($cateData);
        if(!$r):
            $this->response(rest_message('ERR_CODE_DB_FAILURE'), MY_Controller::HTTP_OK);
        endif;
        $this->create_file($tree_id);
        $data = array(
            'subcate_id'=>$r
        );
        $this->response(rest_message('ERR_CODE_SUCCESS',$data), MY_Controller::HTTP_OK);
        //$this->set_response($message, MY_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }
    
    
    //修改
    public function index_post()
    {
        $user_id = $this->rest->user->id;
        $name = trim($this->post('name'));
        $date = $this->post('date');
        $id =  (int) $this->get('id');
        
        if(!$name || !$id):
            $this->response(rest_message('ERR_CODE_PARA'), MY_Controller::HTTP_OK);
        endif;
        //chk if correct tree
        $this->load->model('post_model');
        $r = $this->post_model->is_my_category($user_id,$id);
        if(!$r):
            $this->response(rest_message('ERR_CODE_FORBIDDEN'), MY_Controller::HTTP_OK);
        else:
            $tree_id = $r['tree_id'];
        endif;
        
        $cateData['id'] = $id;
        $cateData['name'] = $name;
        if($date && ($r = strtotime($date))):
            $cateData['date'] = date('Y-m-d',$r);
        endif;
        
        $r = $this->post_model->update_subcategory($cateData);
        if(!$r):
            $this->response(rest_message('ERR_CODE_DB_FAILURE'), MY_Controller::HTTP_OK);
        endif;
        $this->create_file($tree_id);
        $this->response(rest_message('ERR_CODE_SUCCESS'), MY_Controller::HTTP_OK);
    }
    
    
    
    public function index_get()
    {
        $user_id = $this->rest->user->id;
        $id =  (int) $this->get('id');
        
        if(!$id):
            $this->response(rest_message('ERR_CODE_PARA'), MY_Controller::HTTP_NOT_FOUND);
        endif;
        
        //chk if correct tree
        $this->load->model('post_model');
        $r = $this->post_model->is_my_category($user_id,$id);
        if(!$r):
            $this->response(rest_message('ERR_CODE_FORBIDDEN'), MY_Controller::HTTP_NOT_FOUND);
        else:
            $data = array(
                'name'=>$r['name'],
                'date'=>$r['date'],
            );
            $this->response(rest_message('ERR_CODE_SUCCESS',$data), MY_Controller::HTTP_OK);
        endif;
        
    }
    
//    function create_get(){
//        $this->create_file(280);
//    }
    private function create_file($tree_id){
        if(empty($tree_id)):
            return false;
        endif;
        $this->load->model('post_model');
        $this->load->model('user_model');
        $subuser = $this->user_model->get_user_by_id($tree_id);
        if(!$subuser):
            return false;
        endif;
        $username = $subuser['username'];
        $path1='';
        $path2='';
        $path3='';
        chk_upload_path($username, $path1, $path2, $path3);
        $r = $this->post_model->get_category_by_cid($tree_id);
        if($r):
            $content = array(
                'timestamp'=>time(),
                'num'=>count($r),
                'data'=>$r
            );
            write($path2.'category.json',  json_encode($content));
        else:
            return false;
        endif;
        
    }
    
    // category/list?tree_id=280
    public function list_get()
    {
        $user_id = $this->rest->user->id;
        $tree_id =  (int) $this->get('tree_id');
        $timestamp = (int) $this->get('timestamp');
        
        if(!$tree_id):
            $this->response(rest_message('ERR_CODE_PARA'), MY_Controller::HTTP_NOT_FOUND);
        endif;
        
        //chk if correct tree
        $this->load->model('forest_model');
        $r = $this->forest_model->is_tree($user_id,$tree_id);
        if(!$r):
            $this->response(rest_message('ERR_CODE_FORBIDDEN'), MY_Controller::HTTP_NOT_FOUND);
        endif;
        
        $this->load->model('user_model');
        $subuser = $this->user_model->get_user_by_id($tree_id);
        if(!$subuser):
            return false;
        endif;
        $username = $subuser['username'];
        $path1='';
        $path2='';
        $path3='';
        chk_upload_path($username, $path1, $path2, $path3);
        $content = '';
        if(!read($path2.'category.json',$content)):
            $this->response(rest_message('ERR_CODE_EMPTY'), MY_Controller::HTTP_OK);
        endif;
        $json = json_decode($content,true);
        if($json['timestamp'] > $timestamp):
            $this->response(rest_message('ERR_CODE_SUCCESS',$json), MY_Controller::HTTP_OK);
        else:
            $this->response(rest_message('ERR_CODE_LATEST'), MY_Controller::HTTP_OK);
        endif;
        
    }
}
