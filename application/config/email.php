<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$config = Array(
    'protocol' => 'smtp',
    'smtp_host' => 'ssl://smtp.exmail.qq.com',
    'smtp_port' => 465,
    'smtp_user' => 'notice@everlive.me',
    'smtp_pass' => 'ev123456ev',
    'mailtype'  => 'html', 
    'charset'   => 'utf-8',
    'crlf'      =>"\r\n",
    'newline'      =>"\r\n"
);
$config['smtp_timeout'] = '5';

$config['relative_list'] = array(
    REL_FATH=>'父亲',
    REL_MOTH=>'母亲',
    REL_SPOU=>'配偶',
    REL_GRFF=>'爷爷',
    REL_GRMF=>'奶奶',
    REL_GRFM=>'外公',
    REL_GRMM=>'外婆',
    REL_SON=>'儿子',
    REL_DAUG=>'女儿',
    REL_YBRO=>'弟弟',
    REL_EBRO=>'哥哥',
    REL_YSIS=>'妹妹',
    REL_ESIS=>'姐姐',
    REL_BROT=>'兄弟',
    REL_SIST=>'姐妹',
    REL_GRSF=>'孙子',
    REL_GRDF=>'孙女',
    REL_GRSM=>'外孙',
    REL_GRDM=>'外孙女',
    REL_FRIE=>'朋友',
    REL_CMAT=>'同学',
    REL_OTHE=>'其他'//class mate
);
$config['relative_mappding'] = array(
    GENDER_F=>array(
        REL_FATH=>REL_DAUG,
        REL_MOTH=>REL_DAUG,
        REL_SPOU=>REL_SPOU,
        REL_GRFF=>REL_GRDF,
        REL_GRMF=>REL_GRDF,
        REL_GRFM=>REL_GRDM,
        REL_GRMM=>REL_GRDM,
        REL_SON=>REL_MOTH,
        REL_DAUG=>REL_MOTH,
        REL_YBRO=>REL_ESIS,
        REL_EBRO=>REL_YSIS,
        REL_YSIS=>REL_ESIS,
        REL_ESIS=>REL_YSIS,
        REL_BROT=>REL_SIST,
        REL_SIST=>REL_SIST,
        REL_GRSF=>REL_GRMF,
        REL_GRDF=>REL_GRMF,
        REL_GRSM=>REL_GRMM,
        REL_GRDM=>REL_GRMM,
        REL_FRIE=>REL_FRIE,
        REL_OTHE=>REL_OTHE,
        REL_CMAT=>REL_CMAT
    ),
    GENDER_M=>array(
        REL_FATH=>REL_SON,
        REL_MOTH=>REL_SON,
        REL_SPOU=>REL_SPOU,
        REL_GRFF=>REL_GRSF,
        REL_GRMF=>REL_GRSF,
        REL_GRFM=>REL_GRSM,
        REL_GRMM=>REL_GRSM,
        REL_SON=>REL_FATH,
        REL_DAUG=>REL_FATH,
        REL_YBRO=>REL_EBRO,
        REL_EBRO=>REL_YBRO,
        REL_YSIS=>REL_EBRO,
        REL_ESIS=>REL_YBRO,
        REL_BROT=>REL_BROT,
        REL_SIST=>REL_BROT,
        REL_GRSF=>REL_GRFF,
        REL_GRDF=>REL_GRFF,
        REL_GRSM=>REL_GRFM,
        REL_GRDM=>REL_GRFM,
        REL_FRIE=>REL_FRIE,
        REL_OTHE=>REL_OTHE,
        REL_CMAT=>REL_CMAT
    )
);
$config['tag_list'] = array(
    1=>'生活',
    2=>'情感',
    3=>'社会',
    4=>'家乡',
    5=>'学校',
    6=>'其他',
    
    100=>'旅游',
    101=>'亲子',
    102=>'游学',
    103=>'恋人',
    104=>'囧事',
    105=>'自传',
    106=>'收藏'
);
$config['tag_list_active'] = array(    
    100,
    101,
    102,
    103,
    104,
    105,
    106
);