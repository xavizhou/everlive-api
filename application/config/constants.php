<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESCTRUCTIVE') OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code



define('LOGIN_TYPE_NORMAL',     '000');
define('LOGIN_TYPE_WECHAT',     '010');
define('LOGIN_TYPE_QQ',         '020');
define('LOGIN_TYPE_WEIBO',      '030');
define('LOGIN_TYPE_MOBILE',      '040');





//文件上传路径
define('FILE_FOLDER','uploads/');
define('THUMB_FOLDER','thumb/');
define('CACHE_FOLDER', 'cache/');
define('PDF_FOLDER', 'pdf/');
define('ASSETS_FOLDER','assets/');
define('WEATHER_FOLDER','weather/');


define('CACHE_FILE', '.cache');

/*
|--------------------------------------------------------------------------
| File Attribute Value
|--------------------------------------------------------------------------
|
| These modes are used when working with form
|
*/
define('USERNAME_MIN', 4);
define('USERNAME_MAX', 16);
define('PASSWORD_MIN', 4);

define('GENDER_F', 0);//female
define('GENDER_M', 1);//male
define('GENDER_U', 10);//unknow gender
define('GENDER_N', 100);//no gender

define('ACTIVE_F', 0);
define('ACTIVE_T', 1);

define('STATUS_LIVE', 0);
define('STATUS_DEAD', 1);
define('STATUS_CREA', 2);//cremation 火化
define('STATUS_GROW', 10);//小树苗
define('STATUS_FRIE', 11);//朋友树

define('METEOR_SPLIT', '+');


define('USER_BOOK_DELE', 0);
define('USER_BOOK_NORM', 1);
define('USER_BOOK_PUBL', 2);
define('USER_BOOK_CHEC', 3);
define('USER_BOOK_RECO', 4);

define('USER_SOUR_WEB', 1);//来自网页版
define('USER_SOUR_WEIX', 2);//来自微信
define('USER_SOUR_TIPS', 3);//来自情感贴自动创建
define('USER_SOUR_WXLO', 4);//来自微信扫描注册


/*
|--------------------------------------------------------------------------
| Post Type
|--------------------------------------------------------------------------
|
| These modes are used when working with form
|
*/
define('POST_TYPE_BIOG', 1);//自传 biography
define('POST_TYPE_ENTR', 2);//嘱托 entrustment
define('POST_TYPE_SECR', 3);//绝密 secret

define('POST_MODE_TEXT', 1);
define('POST_MODE_IMAG', 2);
define('POST_MODE_VIDE', 3);

define('POST_SOUR_WEB', 1);//来自网页版
define('POST_SOUR_WEIX', 2);//来自微信对话框
define('POST_SOUR_TIPS', 3);//来自情感贴


define('POST_STATUS_DRAF', 0);//草稿
define('POST_STATUS_SAVE', 1);//已经保存
define('POST_STATUS_PULI', 2);//公开的文章

//积分 分值 - type - 每日限额
define('POINT_POST', 100);//发表文章
define('POINT_PUBL', 50);//公开文章
define('POINT_LIKE', 20);//点赞文章

define('POINT_TYPE_POST', 1);//发表文章
define('POINT_TYPE_PUBL', 2);//公开文章
define('POINT_TYPE_LIKE', 3);//点赞文章

define('POINT_LIMIT_POST', 3);//发表文章
define('POINT_LIMIT_PUBL', 3);//公开文章
define('POINT_LIMIT_LIKE', 10);//点赞文章

/*
|--------------------------------------------------------------------------
| Notice
|--------------------------------------------------------------------------
|
*/
define('NOTICE_READ', 1);
define('NOTICE_UNRE', 0);

//通知类型 
define('MSGTEMP_SHARE', 100);//分享给指定用户
define('MSGTEMP_METEO', 200);//流星机制
define('MSGTEMP_SPACE', 300);//空间不足

/*
|--------------------------------------------------------------------------
| Favorite type
|--------------------------------------------------------------------------
|
*/
define('FAVI_MUSI', 1);
define('FAVI_BOOK', 2);
define('FAVI_PHOT', 3);
/*
|--------------------------------------------------------------------------
| Order Status
|--------------------------------------------------------------------------
|
*/
define('ORDER_NOTPAY', 1);
define('ORDER_CANCEL', 2);
define('ORDER_PAYED', 5);
define('ORDER_USED', 10);
/*
|--------------------------------------------------------------------------
| Contribute Status
|--------------------------------------------------------------------------
|
*/
define('CONT_NONE', 0);//未投稿
define('CONT_SEND', 1);//投稿
define('CONT_ACCE', 11);//投稿接受
define('CONT_REFU', 12);//投稿拒绝
/*
|--------------------------------------------------------------------------
| Table Value
|--------------------------------------------------------------------------
|
| These modes are used when working with form
|
*/
define('TBL_USER', 'user');
define('TBL_USES', 'user_social');
define('TBL_UPLA', 'user_place');
define('TBL_UFAV', 'user_favorite');
define('TBL_UFAP', 'user_fav_post');
define('TBL_UFAU', 'user_fav_user');
define('TBL_ULOG', 'user_log');
define('TBL_UMET', 'user_meteor');
define('TBL_UREL', 'user_relative');
define('TBL_USET', 'user_setting');
define('TBL_USEM', 'user_message');
define('TBL_UBOO', 'user_book');
define('TBL_UBOR', 'user_book_remote');
define('TBL_UBOH', 'user_book_history');
define('TBL_UBOF', 'user_book_fav');
define('TBL_ULOC', 'user_location');

define('TBL_CATE', 'category');

define('TBL_CONT', 'contact');
define('TBL_ENTR', 'entrustment');
define('TBL_STAR', 'star');
define('TBL_PAGE', 'page');
define('TBL_POST', 'post');
define('TBL_PTRE', 'post_tree');
define('TBL_PUBL', 'post_public');
define('TBL_PSOC', 'post_social');
define('TBL_PENT', 'post_entrustment');
define('TBL_PENC', 'post_entrustment_contact');//mapping of entrustment and contact
define('TBL_PTAG', 'post_tag');
define('TBL_PCON', 'post_contribute');//mapping of entrustment and contact
define('TBL_POSV', 'post_view');
define('TBL_POSL', 'post_like');
define('TBL_POSC', 'post_comment');

define('TBL_TIPS', 'tips');
define('TBL_TIPC', 'tips_scene');//post of tips
define('TBL_TIPV', 'tips_view');//post of tips history
define('TBL_TIPQ', 'tips_question');//post of tips question

define('TBL_TAGS', 'tags');
define('TBL_BOOK', 'book');
define('TBL_MUSI', 'music');
define('TBL_FILE', 'file');
define('TBL_NOTI', 'notice');
define('TBL_CAPT', 'captcha');
define('TBL_URL', 'url');
define('TBL_ORDE', 'order');
define('TBL_ECOD', 'shopping_ecode');
define('TBL_RCOD', 'register_code');
define('TBL_MCOD', 'mobile_code');
define('TBL_TREE', 'tree');
define('TBL_WECH', 'wechat');

define('TBL_ALBU', 'album');
define('TBL_ALBR', 'album_resource');

define('TBL_MAIL', 'mail_list');
define('TBL_POIN', 'point');

define('TBL_FAMI', 'family');
define('TBL_FREL', 'family_relative');


define('TBL_UFOR', 'user_forest');
define('TBL_UFOC', 'user_forest_code');

define('TBL_USST', 'user_star_status');
define('TBL_MSEN', 'mail_send');
define('TBL_MFEE', 'mail_feedback');


define('TBL_RESC', 'resource');
define('TBL_REST', 'resource_tree');


//relative
define('REL_FATH', 1);//father
define('REL_MOTH', 2);//mother
define('REL_SPOU', 3);//spouse
define('REL_GRFF', 4);//grandfather in father side
define('REL_GRMF', 5);//grandmother in father side
define('REL_GRFM', 6);//grandfather in mother side
define('REL_GRMM', 7);//grandmother in mother side
define('REL_SON', 8);//son
define('REL_DAUG', 9);//daughter
define('REL_YBRO', 10);//elder brother
define('REL_EBRO', 11);//younger brother
define('REL_YSIS', 12);//elder sister
define('REL_ESIS', 13);//younger sister


define('REL_BROT', 21);// brother
define('REL_SIST', 22);// sister


define('REL_GRSF', 14);//grandson in father side
define('REL_GRDF', 15);//granddaughter in father side
define('REL_GRSM', 16);//grandson in monther side
define('REL_GRDM', 17);//granddaughter in monther side

define('REL_OTHE', 50);//other
define('REL_CMAT', 60);//other
define('REL_FRIE', 70);//other

//for ever
define('DATE_FOREVER',  strtotime('2035-01-01'));
define('USER_FREE',  0);//free user
define('USER_ADVA',  1);//advanced user
define('USER_FORE',  2);//forever user

define('FAV_POST',  1);//fav post of dead peaple
define('FAV_TREE',  2);//fav post of tree
define('FAV_SHAR',  3);//fav post of tree

define('EMAIL_VALID','valid.html');
define('EMAIL_ENTRUST','entrustment.html');

define('SYS_MAIL','notice@everlive.me');
define('SYS_MAIL_CONT','notice@everlive.me');
define('SYS_DELI_CONT','postman@everlive.me');
define('SYS_DELI_POST','post@everlive.me');

define('SOCI_WXIN',  1);//forever user

define('TREE_YEAR',  1981);//forever user
define('TREE_BEGI',  1950);//forever user

define('TREE_LIMIT',  20);//most tree
define('TREE_LIMIV',  20);//most tree

define('TREE_POST_YEAR',  PHP_INT_MAX );//most post every year

//空	无内容	有内容	有图片	有视频	有音频	请输入标题	请选择用户	保存中



define('WECHAT_CACHE_EMPTY',  10);//无cache
define('WECHAT_CACHE_START',  20);//无内容
define('WECHAT_CACHE_CONTENT',  30);//有内容

define('WECHAT_CACHE_IMAGE',  31);//有图片
define('WECHAT_CACHE_VIDEO',  32);//有视频
define('WECHAT_CACHE_AUDIO',  33);//有音频

define('WECHAT_CACHE_OVER',  40);//有音频

define('WECHAT_CACHE_TITLE',  50);//请输入标题
define('WECHAT_CACHE_AUTHOR',  60);//请选择用户
define('WECHAT_CACHE_DEALING',  70);//保存中





define('BOOK_STATUS_NORM',  1);//光阴书，正常
define('BOOK_STATUS_SHAR',  2);//光阴书，分享
define('BOOK_STATUS_CONT',  20);//光阴书，投稿
define('BOOK_STATUS_RECO',  21);//光阴书，推荐

define('BOOK_STATUS_CREA',  0);//光阴书，创建，等待输入标题
define('BOOK_STATUS_COMP',  11);//光阴书，全部完成，等待test2处理
define('BOOK_STATUS_DEAL',  12);//光阴书，开始合并
define('BOOK_STATUS_REND',  13);//光阴书，开始渲染
define('BOOK_STATUS_UPLO',  14);//光阴书，已经上传，等待同步


define('BOOK_PURCHASE_FREE',  10);//光阴书，点赞送书
define('BOOK_PURCHASE_PUBL',  20);//光阴书，点赞下载

define('MIN_PDF_FAV',  10);//最少需要的赞
define('MIN_BOOK_FAV',  30);//最少需要的赞免费送

define('GETCONTENT',  TRUE);//最少需要的赞免费送

define('WX_TOKEN','eVer4321');
define('WX_APPID','wx9082eb87ea5323d6');
define('WX_APPSECRET','502d0ec82fe521b0c644f6de05fef1e0');


//define('EVE_ID', 1);
//define('ADM_ID', 2);
//define('SAMPLE_POST_ID', 4324);
//define('OSS_QINDAO', 'oss-cn-qingdao-internal.aliyuncs.com');
define('OSS_QINDAO', 'oss-cn-qingdao.aliyuncs.com');

if (defined('ENVIRONMENT'))
{
	switch (ENVIRONMENT)
	{
		case 'development':
			define('OSS_POST', 'post_dev/');
                        define('OSS_USER', 'user_dev/');
		break;
	
		case 'testing':
			define('OSS_POST', 'post_test/');
                        define('OSS_USER', 'user_test/');
                        break;
	
		case 'production':
			define('OSS_POST', 'post/');
                        define('OSS_USER', 'user/');
		break;

		default:
			define('OSS_POST', 'post_dev/');
                        define('OSS_USER', 'user_dev/');
		break;
	}
}else{
    define('OSS_POST', 'post_dev/');
    define('OSS_USER', 'user_dev/');
}

define('CONTENT_EMPTY',' ');


include 'constants-rest.php';